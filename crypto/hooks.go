package crypto

import (
	"encoding/hex"
	"github.com/libp2p/go-libp2p-core/crypto"
	"github.com/libp2p/go-libp2p-core/peer"
	"github.com/op/go-logging"
	"io/ioutil"
	"os"
	"path/filepath"
)

type PublicK crypto.PubKey
type PrivateK crypto.PrivKey

var log = logging.MustGetLogger("crypto")

type Keypair struct {
	PrivateKey PrivateK
	PublicKey  PublicK
}

type SerializedKey struct {
	Pub    string
	Priv   string
	Pubcid string
}

func (k *Keypair) Sign(b []byte) (string, error) {
	res, err := k.PrivateKey.Sign(b)
	return hex.EncodeToString(res), err
}

func (k *Keypair) Save(filename string) (ok bool) {
	if k.PrivateKey == nil {
		log.Error("No private key ")
		return false
	}
	b, err := crypto.MarshalPrivateKey(k.PrivateKey)
	if err != nil {
		log.Error("problem marshalling key ", k.PubID())
		return false
	}

	dir := filepath.Dir(filename)
	_ = os.MkdirAll(dir, os.ModePerm)

	err = ioutil.WriteFile(filename, b, 0777)
	if err != nil {
		log.Error("problem writing key ", k.PubID())
		return false
	}
	return true
}

func ParsePublicKey(str string) PublicK {

	id, err := peer.Decode(str)
	if err != nil {
		log.Error("problem Decoding key", str, err)
		return nil
	}
	k, err := id.ExtractPublicKey()
	if err != nil {
		log.Error("problem extracting key", str, err)
		return nil
	}
	return k
}

func (k *Keypair) PubID() string {
	id, err := peer.IDFromPublicKey(k.PublicKey)
	if err != nil {
		log.Error("Could not get Pubkey ID")
	}
	return peer.Encode(id)
}

func LoadKey(filename string) Keypair {
	b, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Error("problem reading key ", filename)
		return Keypair{}
	}

	pri, err := crypto.UnmarshalPrivateKey(b)
	if err != nil {
		log.Error("problem unmarshalling key ", filename)
		return Keypair{}
	}

	return Keypair{
		PrivateKey: pri,
		PublicKey:  pri.GetPublic(),
	}

}

func (k *Keypair) Serialize() SerializedKey {
	pub, _ := k.PublicKey.Bytes()
	priv, _ := k.PrivateKey.Bytes()

	return SerializedKey{
		Pub:    string(pub),
		Priv:   string(priv),
		Pubcid: k.PubID(),
	}
}

func GenEd25519() Keypair {
	return Gen(crypto.Ed25519, 0)
}

func Gen(typ int, bits int) Keypair {
	priv, pub, err := crypto.GenerateKeyPair(typ, bits)
	if err != nil {
		log.Error("Could not Generate Keypair ")
	}
	return Keypair{
		PrivateKey: priv,
		PublicKey:  pub,
	}
}
