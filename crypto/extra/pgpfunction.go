package extra

import (
	"bytes"
	c "crypto"
	"encoding/base64"
	p2pc "github.com/libp2p/go-libp2p-core/crypto"
	"github.com/op/go-logging"
	"golang.org/x/crypto/openpgp"
	"golang.org/x/crypto/openpgp/packet"

	"io/ioutil"
)

var log = logging.MustGetLogger("crypto")

// if outEncryptedFile file is not nil, the file will also be stored
// return the string value
func (k *PGPPublicKey) EncryptMessage(secretString string) (encrypted string) {
	c := &packet.Config{
		DefaultHash: c.SHA256,
	}

	entList := openpgp.EntityList{&k.Ent}

	// encrypt string
	buf := new(bytes.Buffer)
	w, err := openpgp.Encrypt(buf, entList, nil, nil, c)
	if err != nil {
		log.Error("error encrypting ", secretString, &k.Ent, err)
		return ""
	}
	_, err = w.Write([]byte(secretString))
	if err != nil {
		log.Error(err)
		return ""
	}
	err = w.Close()
	if err != nil {
		log.Error(err)
		return ""
	}

	encStr := EncodeBytesToBase64(buf)

	// Output encrypted/encoded string
	log.Info("Encrypted Secret:", encStr)

	return encStr
}

func (k *PGPPrivateKey) DecryptMessage(encString string) (decrypted string, err error) {
	decStr := DecodeBase64String(encString)
	log.Info("after  decoding", decStr)
	var entityList openpgp.EntityList
	entityList = append(entityList, &k.k)

	// read the string using the secret pair
	md, err := openpgp.ReadMessage(bytes.NewBuffer(decStr), entityList, nil, nil)
	if err != nil {
		log.Error("pk: ", entityList)
		log.Error("error openpgp.ReadMessage", md, err)
	}

	bytesR, err := ioutil.ReadAll(md.UnverifiedBody)
	if err != nil {
		log.Error("error ioutil.ReadAll", err)
	}

	log.Info("decryption completed : ", decStr)

	return string(bytesR), nil
}

func DecryptPrivateKey(ent *openpgp.Entity, passphrase string) {
	if ent.PrivateKey.Encrypted {
		log.Debug("decrypting private pair ... ")
		err := ent.PrivateKey.Decrypt([]byte(passphrase))
		if err != nil {
			log.Error("could not decrypt ", err)
		}

		for _, subkey := range ent.Subkeys {
			if subkey.PrivateKey.Encrypted {
				err = subkey.PrivateKey.Decrypt([]byte(passphrase))
				if err != nil {
					log.Error("could not decrypt subkey", err)
				}
			}

		}
	}
}

func ReadPubAndPrivateArmorKeyFile(pub, priv, pass string) (p2pc.PrivKey, p2pc.PubKey) {
	private, _ := ReadArmoredPrivateFile(priv, pass)
	public, _ := ReadArmoredPubFile(pub)

	return private, public

}

// Open and parse private or public armored file
func ReadArmoredPubFile(name string) (p2pc.PubKey, error) {
	byts, err := ioutil.ReadFile(name)
	if err != nil {
		return nil, err
	}
	return PGPPubUnmarshaller(byts)
}

// Open and parse private or public armored file
func ReadArmoredPrivateFile(name, passphrase string) (p2pc.PrivKey, error) {
	byts, err := ioutil.ReadFile(name)
	if err != nil {
		return nil, err
	}

	k, err := ArmoredEntityReader(bytes.NewReader(byts))
	if err != nil {
		return nil, err
	}

	DecryptPrivateKey(k, passphrase)

	return &PGPPrivateKey{k: *k}, nil
}

// ================= Utilities =====================

func DecodeBase64String(message string) []byte {

	dec, err := base64.StdEncoding.DecodeString(message)
	if err != nil {
		log.Error("error decoding to base 64 string ", message, err)
	}

	return dec
}

func EncodeBytesToBase64(buf *bytes.Buffer) string {

	bytesR, err := ioutil.ReadAll(buf)
	if err != nil {
		log.Error("error Encode to base64 ", err, buf)
	}
	log.Info("before encoding", bytesR)
	encStr := base64.StdEncoding.EncodeToString(bytesR)
	return encStr
}
