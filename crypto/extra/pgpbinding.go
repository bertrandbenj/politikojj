package extra

import (
	"bytes"
	c "crypto"
	"github.com/libp2p/go-libp2p-core/crypto"
	pb "github.com/libp2p/go-libp2p-core/crypto/pb"
	"golang.org/x/crypto/openpgp"
	"golang.org/x/crypto/openpgp/armor"
	"golang.org/x/crypto/openpgp/packet"
	"io"
)

var PGPDefaultConf = &packet.Config{
	DefaultHash: c.SHA256,
}

const KeyType_OpenPGP = 5

type PGPPrivateKey struct {
	k openpgp.Entity
}

// Ed25519PublicKey is an ed25519 public pair.
type PGPPublicKey struct {
	Ent openpgp.Entity
}

func (k *PGPPrivateKey) Bytes() ([]byte, error) {
	return pgpArmor(&k.k, openpgp.PrivateKeyType)
}

func (k *PGPPrivateKey) Equals(key crypto.Key) bool {
	_, ok := key.(*PGPPrivateKey)
	if !ok {
		return false
	}
	r1, _ := k.Raw()
	r2, _ := key.Raw()
	return bytes.Equal(r1, r2)
}

func (k *PGPPrivateKey) Raw() ([]byte, error) {
	return pgpArmor(&k.k, openpgp.PrivateKeyType)
}

func (k *PGPPrivateKey) Type() pb.KeyType {
	return KeyType_OpenPGP
}

func (k *PGPPrivateKey) Sign(data []byte) ([]byte, error) {

	armoredSignature := new(bytes.Buffer)

	err := openpgp.ArmoredDetachSign(armoredSignature, &k.k, bytes.NewBuffer(data), PGPDefaultConf)

	return armoredSignature.Bytes(), err
}

func (k *PGPPrivateKey) GetPublic() crypto.PubKey {
	return &PGPPublicKey{Ent: k.k}
}

func (k *PGPPublicKey) Bytes() ([]byte, error) {
	return pgpArmor(&k.Ent, openpgp.PublicKeyType)
}

func (k *PGPPublicKey) Equals(key crypto.Key) bool {
	_, ok := key.(*PGPPublicKey)
	if !ok {
		return false
	}
	r1, _ := k.Raw()
	r2, _ := key.Raw()
	return bytes.Equal(r1, r2)
}

func (k *PGPPublicKey) Raw() ([]byte, error) {
	return pgpArmor(&k.Ent, openpgp.PublicKeyType)
}

func (k *PGPPublicKey) Type() pb.KeyType {
	return KeyType_OpenPGP
}

func (k *PGPPublicKey) Verify(data []byte, sig []byte) (bool, error) {
	signer, err := openpgp.CheckArmoredDetachedSignature(
		openpgp.EntityList{&k.Ent},
		bytes.NewBuffer(data),
		bytes.NewBuffer(sig))
	return signer != nil, err
}

func PGPPubUnmarshaller(data []byte) (crypto.PubKey, error) {
	k, err := ArmoredEntityReader(bytes.NewReader(data))
	return &PGPPublicKey{Ent: *k}, err
}

func PGPPrivUnmarshaller(data []byte) (crypto.PrivKey, error) {
	k, err := ArmoredEntityReader(bytes.NewReader(data))
	//decryptPrivateKey(Ent, "") // TODO compatibility issue passphrase isnt a libp2p concept
	return &PGPPrivateKey{k: *k}, err
}

// Utilities function

func pgpArmor(entity *openpgp.Entity, blockType string) ([]byte, error) {
	privArmored := new(bytes.Buffer)

	//privFile, err := os.Create(privKey)
	armoredPrivate, err := armor.Encode(privArmored, blockType, nil)
	if err != nil {
		return []byte{}, err
	}
	err = entity.Serialize(armoredPrivate)
	if err != nil {
		return []byte{}, err
	}
	err = armoredPrivate.Close()
	return privArmored.Bytes(), err
}

func ArmoredEntityReader(armored io.Reader) (*openpgp.Entity, error) {
	block, err := armor.Decode(armored)
	if err != nil {
		return nil, err
	}
	return openpgp.ReadEntity(packet.NewReader(block.Body))
}

func GeneratePGPKeyPair(name string, comment string, email string) (crypto.PrivKey, crypto.PubKey, error) {

	key, err := openpgp.NewEntity(name, comment, email, PGPDefaultConf)
	if err != nil {
		return nil, nil, err
	}

	//  Sign all the identities
	for _, id := range key.Identities {

		id.SelfSignature.PreferredCompression = []uint8{
			uint8(packet.CompressionZLIB),
			uint8(packet.CompressionZIP),
		}

		err := id.SelfSignature.SignUserId(id.UserId.Id, key.PrimaryKey, key.PrivateKey, PGPDefaultConf)
		if err != nil {
			return nil, nil, err
		}
	}

	// Self-sign the Subkeys
	for _, subkey := range key.Subkeys {
		//subkey.Sig.KeyLifetimeSecs = &dur
		err := subkey.Sig.SignKey(subkey.PublicKey, key.PrivateKey, PGPDefaultConf)
		if err != nil {
			return nil, nil, err
		}
	}

	return &PGPPrivateKey{*key}, &PGPPublicKey{*key}, err
}

func RegisterOpenPGPBinding() {
	crypto.PubKeyUnmarshallers[KeyType_OpenPGP] = PGPPubUnmarshaller
	crypto.PrivKeyUnmarshallers[KeyType_OpenPGP] = PGPPrivUnmarshaller
	crypto.KeyTypes = append(crypto.KeyTypes, KeyType_OpenPGP)
}
