package crypto

import (
	"fmt"
	"github.com/btcsuite/btcutil/base58"
	"github.com/libp2p/go-libp2p-core/crypto"
	"github.com/stretchr/testify/assert"
	"gitlab.com/bertrandbenj/politikojj/crypto/extra"
	mrand "math/rand"
	"os"
	"testing"
)

var pgpPair Keypair

const mySecretString = "this is so very secret!"
const passphrase = "test1"

func init() {

	priv, pub := extra.ReadPubAndPrivateArmorKeyFile("../testset/pub.asc", "../testset/priv.asc", "test1")
	pgpPair = Keypair{
		PrivateKey: priv,
		PublicKey:  pub,
	}
}

func BenchmarkSignature(t *testing.B) {

	msg := []byte(mySecretString)

	got, _ := pgpPair.PrivateKey.Sign(msg)
	fmt.Println(got)

	verify, _ := pgpPair.PublicKey.Verify(msg, got)

	assert.True(t, verify, "verify signature")

	t.ReportAllocs()
}

func BenchmarkEncryption(t *testing.B) {
	enc := pgpPair.PublicKey.(*extra.PGPPublicKey).EncryptMessage(mySecretString)
	fmt.Println(enc)
	dec, _ := pgpPair.PrivateKey.(*extra.PGPPrivateKey).DecryptMessage(enc)
	assert.Equal(t, dec, mySecretString, "decrypted string different")

}

func TestReadEntity(t *testing.T) {
	_, err := extra.ReadArmoredPubFile("../testset/pub.asc")
	assert.Nil(t, err, "test armor entity")

}

// fileExists checks if a file exists and is not a directory before we
// try using it to prevent further errors.
func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

func TestKeypair_Save(t *testing.T) {
	key := GenEd25519()
	fname := "/tmp/testcrypto/" + key.PubID()

	assert.True(t, key.Save(fname))

	emptyKey := Keypair{}
	assert.False(t, emptyKey.Save("/tmp/emptyKey"))

	forbidenPath := Keypair{}
	assert.False(t, forbidenPath.Save("/root/emptyKey"))

}

func Test(t *testing.T) {
	pair := GenEd25519()
	genPub := pair.PubID()

	msg, err := pair.PrivateKey.Sign([]byte(mySecretString))
	assert.Nil(t, err)

	parsed := ParsePublicKey(genPub)
	assert.NotNil(t, parsed)

	assert.Nil(t, ParsePublicKey("ABCDEFGH"))

	ok, err := parsed.Verify([]byte(mySecretString), msg)
	assert.Nil(t, err)
	assert.True(t, ok)

	assert.NotNil(t, pair.Serialize())
}

func TestLoadKey(t *testing.T) {
	fname := "../testset/ed25519.priv"
	key := LoadKey(fname)
	assert.NotNil(t, key)

	assert.Empty(t, LoadKey("/no/file"))
	assert.Empty(t, LoadKey("../docs/constitution.md"))
}

func TestReadPrivateArmorFile(t *testing.T) {
	entity, _ := extra.ReadArmoredPrivateFile("../testset/priv.asc", passphrase)
	assert.NotNil(t, entity, "test private armor file")
}

func TestPGPPrivateKey_Sign(t *testing.T) {
	signature, err := pgpPair.PrivateKey.Sign([]byte(mySecretString))
	assert.Nil(t, err, "error should be nil")
	assert.Greater(t, len(signature), 0, "signature should not be empty")
}

func TestEncryptMessage(t *testing.T) {

	enc := pgpPair.PublicKey.(*extra.PGPPublicKey).EncryptMessage(mySecretString)
	assert.Greater(t, len(enc), 0, "encrypted message should not be empty")
}

func TestGeneratePGPKeyPair(t *testing.T) {
	priv, pub, err := extra.GeneratePGPKeyPair("testName", "testComment", "email@email.com")
	assert.Nil(t, err, "Generating key")
	assert.NotNil(t, priv, "Generated private key")
	assert.NotNil(t, pub, "Generated public key")
}

func TestPGPPrivateKey_DecryptMessage(t *testing.T) {
	encoded := "wcBMA6+EN6Xft2vEAQgAVh0wnKRU3vRk5bY8sxD+jGUY92uX2bzekrjY1j6qfTqV6MM+CFEAaXERg9a81rM0odgG0mrXab2n3pmuDzEHPkpOAY8obz8y5SjnHavc5uV8wZb/aXy/bgCh+9ywRYM7SUpcPi8Rh/IVWMpWqSvj9E1v6Aj+mjS3G4deWJilBG/pQnc3yVQX7W4sqNX5Dkvq+b5NKhZvs03tQ4QtKPX03iwH79JVyghKPWhGlCWicjcMRGLzLcvYKEaDK8HaCK8i0qKxoV9nfE4IEQhZdE2LDDKj0IrIEcV+XwS2kQStO9Rps5vxPbEwONeflUFfWbraCbC6ZINAy3FW7Wz0Pr5IwtLmAQ4+flcidDb52qS0HMx5W0r9Vjrinr2uxPS+Y987VoIYAENPUwaQKF28j6U95oFFnp0d/D20YBUVSaaJeSKBvOPnvuTJnAMtEOKcZ/EjAA=="
	dec, err := pgpPair.PrivateKey.(*extra.PGPPrivateKey).DecryptMessage(encoded)
	assert.Nil(t, err, "Decrypting")
	assert.Equal(t, mySecretString, dec, "Decrypted message should match")
}

func TestG1Compatibility(t *testing.T) {
	g1raw := "5jf8kATgxw8QxFugnh8cEPYYf8BVzPQLWcEq1vHhQHyUTLL5CHd1qbhJmwEsWEeBQSfLYGQU7dhbuyLizyHb28Kx"
	g1pub := "3LJRrLQCio4GL7Xd48ydnYuuaeWAgqX4qXYFbXDTJpAa"

	g1Priv, err := crypto.UnmarshalEd25519PrivateKey(base58.Decode(g1raw))
	g1Pub, err := crypto.UnmarshalEd25519PublicKey(base58.Decode(g1pub))
	assert.Nilf(t, err, "Private pgpPair reading %s", g1Priv)

	assert.Equal(t, g1Priv.GetPublic(), g1Pub, "pgpPair not matching ")
	x, _ := g1Priv.GetPublic().Raw()
	log.Info(base58.Encode(x))
}

func Test_libP2PCrypto(t *testing.T) {

	r := mrand.New(mrand.NewSource(1))

	pr, pk, err := crypto.GenerateEd25519Key(r)
	fmt.Println("Raw ", pr, pk)

	bpriv, _ := pr.Raw() //crypto.MarshalPrivateKey(pr)
	bpub, _ := pk.Raw()

	fmt.Println("Marshaled PubKey : ", bpub, "Marshaled PrivateK: ", bpriv, "Type : ", pr.Type().String())

	priv, err := crypto.UnmarshalEd25519PrivateKey(bpriv)
	assert.Nil(t, err, "Private pgpPair reading")

	pub, err := crypto.UnmarshalEd25519PublicKey(bpub)
	assert.Nil(t, err, "Private pgpPair reading")

	log.Infof("UnMarshaled PubKey : %s\nUnMarshaled PrivateK: %s", pub, priv)

}
