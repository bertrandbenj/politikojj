package model

type Wrapper struct {
	CID  string
	Type string
	Data interface{}
}
