package model

import "strings"

// Constitutional Proposition predicates
const (
	ProposeTax               = "ProposeTax"
	ProposeTaxVar            = "ProposeTaxVar"
	ProposeTaxRevenueTarget  = "ProposeTaxRevenueTarget"
	ProposeTaxType           = "ProposeTaxType"
	ProposeNewVariable       = "ProposeNewVariable"
	ProposeCreateAccount     = "ProposeCreateAccount"
	ProposeAccountPermission = "ProposeAccountPermission"
	ProposeNewRole           = "ProposeNewRole"
	ProposeAddPermission     = "ProposeAddPermission"
)

var AvailablePropose = []string{
	ProposeTax,
	ProposeTaxVar,
	ProposeTaxRevenueTarget,
	ProposeTaxType,
	ProposeNewVariable,
	ProposeCreateAccount,
	ProposeAccountPermission,
	ProposeNewRole,
	ProposeAddPermission}

// Tax types
const (
	GlobalTransactionTax = "GlobalTransactionTax"
	BytesTax             = "BytesTax"
)

var AvailableTaxType = []string{GlobalTransactionTax, BytesTax}

// Variable types
const (
	UnitIntervalType = "UnitIntervalValue"
	CategoryType     = "CategoryVariable"
	IntegerType      = "IntegerVariable"
)

var AvailableVariableTax = []string{UnitIntervalType, CategoryType, IntegerType}

// Installation procedures
var InstallAmendment = map[string]func(predicate *Rule, cratos *Cratos){
	ProposeTax:               installTax,
	ProposeTaxVar:            installTaxVar,
	ProposeTaxRevenueTarget:  installTaxRevenueTarget,
	ProposeTaxType:           installTaxType,
	ProposeNewVariable:       installNewVariable,
	ProposeCreateAccount:     installNewAccount,
	ProposeAccountPermission: installAccountPermission,
	ProposeNewRole:           installNewRole,
	ProposeAddPermission:     installAddPermission,
}

func installAddPermission(predicate *Rule, cratos *Cratos) {
	perms := cratos.Roles.Permission[predicate.Subject]
	cratos.Roles.Permission[predicate.Subject] = append(perms, predicate.Object)
}

func installNewRole(predicate *Rule, cratos *Cratos) {
	cratos.Roles.Permission[predicate.Object] = []string{}
	if !strings.HasPrefix(predicate.Subject, ".") {
		cratos.Roles.Inheritance[predicate.Object] = []string{predicate.Subject}
	}
}

func installAccountPermission(predicate *Rule, cratos *Cratos) {
	acc := cratos.PublicAccounts[predicate.Subject]
	acc.Permission = predicate.Object
	cratos.PublicAccounts[predicate.Subject] = acc
}

func installNewAccount(predicate *Rule, cratos *Cratos) {
	cratos.PublicAccounts[predicate.Subject] = PublicAccount{Name: predicate.Subject}
}

func installNewVariable(predicate *Rule, cratos *Cratos) {
	res := Vars{
		Type:      predicate.Object,
		Value:     nil,
		VotesByPk: make(map[string]interface{}),
	}
	if predicate.Object == UnitIntervalType {
		res.Value = 0.5
	}
	cratos.Variables[predicate.Subject] = res
	//cratos.VarVotesByNameByPk[predicate.Subject] = make(map[string]interface{})
}

func installTaxRevenueTarget(predicate *Rule, cratos *Cratos) {
	tax := cratos.Taxes[predicate.Subject]
	tax.Account = predicate.Object
	cratos.Taxes[predicate.Subject] = tax
}

func installTaxType(predicate *Rule, cratos *Cratos) {
	tax := cratos.Taxes[predicate.Subject]
	tax.Type = 0
	cratos.Taxes[predicate.Subject] = tax
}

func installTax(predicate *Rule, cratos *Cratos) {
	cratos.Taxes[predicate.Object] = Tax{
		Name: predicate.Object,
	}
}

func installTaxVar(predicate *Rule, cratos *Cratos) {
	t := cratos.Taxes[predicate.Subject]
	t.Variable = predicate.Object
	cratos.Taxes[predicate.Subject] = t
}

// Basic predicate integrity check
var VerifyPredicate = map[string]func(predicate *Rule, cratos *Cratos) bool{
	ProposeTax:               func(p *Rule, cratos *Cratos) bool { return true },
	ProposeTaxVar:            func(p *Rule, cratos *Cratos) bool { return true },
	ProposeTaxRevenueTarget:  func(p *Rule, cratos *Cratos) bool { return true },
	ProposeTaxType:           checkTaxType,
	ProposeNewVariable:       checkNewVar,
	ProposeCreateAccount:     func(p *Rule, cratos *Cratos) bool { return true },
	ProposeAccountPermission: func(p *Rule, cratos *Cratos) bool { return true },
	ProposeNewRole:           func(p *Rule, cratos *Cratos) bool { return true },
	ProposeAddPermission:     func(p *Rule, cratos *Cratos) bool { return true },
}

func checkNewVar(predicate *Rule, _ *Cratos) bool {
	switch predicate.Object {
	case UnitIntervalType:
		return true
	case CategoryType:
		return true
	case IntegerType:
		return true
	}

	return false
}

func checkTaxType(predicate *Rule, _ *Cratos) bool {

	switch predicate.Object {
	case GlobalTransactionTax:
		return true
	case BytesTax:
		return true
	}
	return false
}
