package model

import (
	"encoding/hex"
	"fmt"
)

// ==== Signature related stuff ====
type SignedStampedDoc struct {
	Timestamp int64
	PublicKey string
	Signature string
}

func (d *SignedStampedDoc) Stamp(s SignedStampedDoc) {
	d.PublicKey = s.PublicKey
	d.Timestamp = s.Timestamp
	d.Signature = s.Signature
}

func (d SignedStampedDoc) PartToSign() []byte {
	return []byte(fmt.Sprintf("%d", d.Timestamp) + d.PublicKey)
}

func (d SignedStampedDoc) Details() (pk string, sign []byte) {
	s, _ := hex.DecodeString(d.Signature)
	return d.PublicKey, s
}

func (d SignedStampedDoc) GetSignature() string {
	return d.Signature
}
func (d SignedStampedDoc) GetPublicKey() string {
	return d.PublicKey
}

type Signable interface {
	PartToSign() []byte
	Details() (pk string, sign []byte)
	GetSignature() string
	GetPublicKey() string
	Stamp(SignedStampedDoc)
}

// Observer pattern
type Observable interface {
	Register(observer Observer)
	Deregister(observer Observer)
	NotifyAll()
}
type Observer interface {
	Update(string)
	GetID() string
}

type ObservableList struct {
	observerList []Observer
}

func (c *ObservableList) Register(o Observer) {
	c.observerList = append(c.observerList, o)
}

func (c *ObservableList) Deregister(o Observer) {
	c.observerList = removeFromslice(c.observerList, o)
}

func (c *ObservableList) NotifyAll() {
	for _, observer := range c.observerList {
		observer.Update("new")
	}
}

func removeFromslice(observerList []Observer, observerToRemove Observer) []Observer {
	observerListLength := len(observerList)
	for i, observer := range observerList {
		if observerToRemove.GetID() == observer.GetID() {
			observerList[observerListLength-1], observerList[i] = observerList[i], observerList[observerListLength-1]
			return observerList[:observerListLength-1]
		}
	}
	return observerList
}

// ==== Other stuff ====
type CidLink struct {
	Link string `json:"/,omitempty"`
}

type Evidence struct {
	CidLink
	Object interface{}
}
