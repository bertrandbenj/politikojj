package model

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/bertrandbenj/politikojj/crypto"
)

var key crypto.Keypair
var ssd SignedStampedDoc

func init() {
	key = crypto.GenEd25519()
	ssd = SignedStampedDoc{
		Timestamp: time.Now().UTC().Unix(),
		PublicKey: key.PubID(),
	}

}

func TestIsSignatureValid(t *testing.T) {
	var x = SignedStampedDoc{
		Timestamp: 0,
		PublicKey: "",
	}
	x.Signature, _ = key.Sign(x.PartToSign())
	_, s := x.Details()
	ok, _ := key.PublicKey.Verify(x.PartToSign(), s)
	assert.True(t, ok)
}

func TestAmendment(t *testing.T) {
	a := Amendment{
		Name:       "AmendName",
		Predicates: []Rule{},
		SignedStampedDoc: SignedStampedDoc{
			Timestamp: time.Now().UTC().Unix(),
			PublicKey: key.PubID(),
		},
	}
	assert.NotNil(t, a.PartToSign())
}

func TestSignedStampedDoc(t *testing.T) {

	pk, sign := ssd.Details()
	assert.NotNil(t, pk)
	assert.Empty(t, sign)

	assert.NotNil(t, ssd.GetPublicKey())
	assert.NotNil(t, ssd.PartToSign())
	assert.Empty(t, ssd.GetSignature())

	ssd2 := SignedStampedDoc{
		Timestamp: time.Now().UTC().Unix(),
		PublicKey: key.PubID(),
		Signature: "xy",
	}
	ssd.Stamp(ssd2)
	assert.NotNil(t, ssd.GetSignature())
}

func TestBlock(t *testing.T) {
	b := Block{
		Index:            0,
		Previous:         CidLink{"ABCD"},
		Network:          "TEST",
		Demos:            CidLink{"DEMOSCID"},
		Cratos:           CidLink{"CRATOSCID"},
		Currency:         CidLink{"CCYCID"},
		Notary:           CidLink{"Notary"},
		Evidences:        nil,
		SignedStampedDoc: ssd,
		Difficulty:       CidLink{"PowDifficulty"},
		Nonce:            "0x321654987",
		PoWHash:          "nil",
	}
	assert.NotNil(t, b.GetSignature())
	assert.NotNil(t, b.PartToSign())
	assert.NotNil(t, b.GetPublicKey())
	assert.NotNil(t, b.PartToHash())
	b.HashPow()
	assert.NotNil(t, b.PoWHash)
	assert.NotNil(t, b.Redacted())
}

func TestBlock_IsHashValid(t *testing.T) {

	for x := 0; x < 16; x++ {
		diff := PersonalizedAdaptivePoW{
			AvgGenTime:         10,
			CommonDifficulty:   64 + uint(x),
			PreviousBlockTimes: []int64{},
			PersonalHandicap:   nil,
		}

		zeros, rem := diff.GetNbZerosAndRemainder()

		block := Block{
			PoWHash: "00000923465789",
		}

		assert.Truef(t, block.IsHashValid(diff), "failed at %d, zeros %d, rem %d", x, zeros, rem)
	}
}

func TestTransaction(t *testing.T) {
	tx := Transaction{
		SignedStampedDoc: ssd,
		To:               "ABC",
		Amount:           0,
	}
	assert.NotNil(t, tx.PartToSign())
}

func TestCategoryVariable(t *testing.T) {
	cv := ParseCategoryVar("ABC")
	assert.NotNil(t, cv)
	assert.NotNil(t, cv.Description())
	assert.Equal(t, 0.0, cv.FloatValue())
	assert.True(t, cv.IsLegal("ABC"))
}

func TestIntegerVariable(t *testing.T) {
	iv := ParseIntegerVar("123")
	assert.NotNil(t, iv)
	assert.NotNil(t, iv.Description())
	assert.Equal(t, 123.0, iv.FloatValue())
	assert.True(t, iv.IsLegal("123"))
	assert.False(t, iv.IsLegal("ABC"))
}

func TestUnitIntervalVariable(t *testing.T) {
	uiv := ParseUnitIntervalVar("0.123")
	assert.NotNil(t, uiv)
	assert.NotNil(t, uiv.Description())
	assert.Equal(t, 0.123, uiv.FloatValue())
	assert.True(t, uiv.IsLegal("0.123"))
	assert.False(t, uiv.IsLegal("1.123"))
}

func TestClaim(t *testing.T) {
	cl := Claim{
		ChosenName:       "ABC",
		Role:             Citizen,
		SignedStampedDoc: ssd,
	}

	assert.NotNil(t, cl.PartToSign())
	assert.False(t, cl.Expired())

	acl := ApprovedClaim{
		Claim:            cl,
		SignedStampedDoc: ssd,
	}
	assert.NotNil(t, acl.PartToSign())
}

func TestVars_Avg(t *testing.T) {
	vs := Vars{
		Type:      "ABC",
		Value:     0,
		VotesByPk: map[string]interface{}{},
	}

	vs.VotesByPk["U1"] = 1.0
	vs.VotesByPk["U2"] = 2.0
	vs.Avg()
	assert.Equal(t, 1.5, vs.Value)
}

func TestObservableList(t *testing.T) {

	ol := ObservableList{}
	obs := testObs{}
	ol.Register(obs)
	assert.Equal(t, 1, len(ol.observerList))
	ol.NotifyAll()

	ol.Deregister(obs)
	assert.Equal(t, 0, len(ol.observerList))

}

type testObs struct {
}

func (o testObs) Update(string) {

}
func (o testObs) GetID() string {
	return "X"
}

func TestInstallationFunctions(t *testing.T) {
	cr := &Cratos{
		Roles: Roles{
			Permission:  map[string][]string{},
			Inheritance: map[string][]string{},
		},
		Claims:           Claims{},
		Variables:        map[string]Vars{},
		ProposePredicate: []string{},
		Taxes:            map[string]Tax{},
		PublicAccounts:   map[string]PublicAccount{},
	}

	installTax(&Rule{Predicate: ProposeTax,
		Subject: ".",
		Object:  "socialSecurityTax"}, cr)
	installTaxVar(&Rule{Predicate: ProposeTaxVar,
		Subject: "socialSecurityTax",
		Object:  "socialSecurityTaxRate"}, cr)
	installAccountPermission(&Rule{Predicate: ProposeAccountPermission,
		Subject: "socialSecurityAccount",
		Object:  "SpendSocialSecurityAccount"}, cr)
	installAddPermission(&Rule{Predicate: ProposeAddPermission,
		Subject: "TreasurerSocialS",
		Object:  "SpendSocialSecurityAccount"}, cr)
	installNewAccount(&Rule{Predicate: ProposeCreateAccount,
		Subject: "socialSecurityAccount",
		Object:  "CurrencyName"}, cr)

	installNewVariable(&Rule{Predicate: ProposeNewVariable,
		Subject: "socialSecurityTaxRate",
		Object:  UnitIntervalType}, cr)
	installTaxRevenueTarget(&Rule{Predicate: ProposeTaxRevenueTarget,
		Subject: "socialSecurityTax",
		Object:  "socialSecurityAccount"}, cr)
	installTaxType(&Rule{Predicate: ProposeTaxType,
		Subject: "socialSecurityTax",
		Object:  GlobalTransactionTax}, cr)
	installNewRole(&Rule{Predicate: ProposeNewRole,
		Subject: ".",
		Object:  "TreasurerSocialS"}, cr)

	checkNewVar(&Rule{Predicate: ProposeNewVariable,
		Subject: "socialSecurityTaxRate",
		Object:  UnitIntervalType}, cr)

	checkTaxType(&Rule{Predicate: ProposeTaxType,
		Subject: "socialSecurityTax",
		Object:  GlobalTransactionTax}, cr)
}

func TestCurrency_SumAccounts(t *testing.T) {
	ccy := Currency{
		Name:          "X",
		MonetaryMass:  30,
		UnitBase:      0,
		CitizenCount:  0,
		Dividend:      0,
		Wallets:       map[string]uint64{},
		JointWallets:  map[string]uint64{},
		PublicWallets: map[string]uint64{},
	}
	ccy.Wallets["X"] = 10
	ccy.JointWallets["Y"] = 10
	ccy.PublicWallets["Z"] = 10
	sum := ccy.SumAccounts()

	assert.Equal(t, uint64(30), sum)
}

func TestOpenBallot(t *testing.T) {
	b := OpenBallot{
		Permission:       VoteGrantClaim,
		VoteObject:       "ABC",
		VoteValue:        "Grant",
		SignedStampedDoc: ssd,
	}

	assert.NotNil(t, b.PartToSign())
}
