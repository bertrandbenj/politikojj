package model

// Associate Public Keys to a crypted Pointer and a signature
type MultiPartyDocument struct {
	Timestamp    int64
	cryptedLinks map[string]string
	signedLinks  map[string]string
}

type Notary struct {
	Documents []MultiPartyDocument
}

func (n *Notary) AddDoc(time int64, users []string, signatures []string, links []CidLink) {
	link := map[string]string{}
	signs := map[string]string{}

	for i := 0; i < len(users); i++ {
		link[users[i]] = links[i].Link
		signs[users[i]] = signatures[i]
	}

	n.Documents = append(n.Documents, MultiPartyDocument{
		Timestamp:    time,
		cryptedLinks: link,
		signedLinks:  signs,
	})
}
