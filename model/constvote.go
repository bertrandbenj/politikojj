package model

const (
	VoteGrantClaim             = "VoteGrantClaim"
	VoteRevokeClaim            = "VoteRevokeClaim"
	VoteGrantPermissionToRole  = "VoteGrantPermissionToRole"
	VoteRevokePermissionToRole = "VoteRevokePermissionToRole"
	VoteVariableValue          = "VoteVariableValue"
	VoteAmendment              = "VoteAmendment"
)

var AvailableVotes = []string{
	VoteGrantClaim,
	VoteRevokeClaim,
	VoteGrantPermissionToRole,
	VoteRevokePermissionToRole,
	VoteVariableValue,
	VoteAmendment,
}

const (
	Citizen            = "Citizen"
	CitizenshipOfficer = "CitizenshipOfficer"
)

var AvailableRole = []string{
	Citizen,
	CitizenshipOfficer,
}
