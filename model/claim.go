package model

import (
	"bytes"
	"time"
)

const ClaimExpiry = 1

type Claim struct {
	ChosenName string
	Role       string
	SignedStampedDoc
}

type ApprovedClaim struct {
	Claim Claim
	SignedStampedDoc
}

type Expirable interface {
	Expired() bool
}

func (c Claim) Expired() bool {
	return time.Unix(c.Timestamp, 0).Add(time.Hour * 24 * ClaimExpiry).Before(time.Now().UTC())
}

func (c Claim) PartToSign() []byte {
	buf := bytes.NewBuffer(c.SignedStampedDoc.PartToSign())
	buf.WriteString(c.ChosenName)
	buf.WriteString(c.Role)
	return buf.Bytes()
}

func (approved ApprovedClaim) PartToSign() []byte {
	buf := bytes.NewBuffer(approved.SignedStampedDoc.PartToSign())
	buf.Write(approved.Claim.PartToSign())

	return buf.Bytes()
}
