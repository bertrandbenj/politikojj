package model

import (
	"bytes"
	"fmt"
	"math"
	"sort"
)

const (
	DtDiffEval = 12 // window size (# of blocks) to compute pow difficulty
	PercentRot = 0.67
)

type PersonalizedAdaptivePoW struct {
	AvgGenTime         float64 // # of seconds between each blocks
	CommonDifficulty   uint
	PreviousBlockTimes []int64
	PersonalHandicap   map[string]uint
	IssuersFrame       []IssuedBlock
}

type IssuedBlock struct {
	Issuer      string
	Time        int64
	IssuerCount int
	Frame       int
	FrameVar    int
}

func (pow PersonalizedAdaptivePoW) PartToSign() []byte {
	buf := bytes.NewBufferString(fmt.Sprintf("%d", pow.CommonDifficulty))
	for _, v := range pow.PreviousBlockTimes {
		buf.WriteString(fmt.Sprintf("%d", v))
	}
	return buf.Bytes()
}

func (pow PersonalizedAdaptivePoW) GetNbZerosAndRemainder() (int, int) {
	nbZeros := int(pow.CommonDifficulty) / 16
	rem := int(pow.CommonDifficulty) - 16*nbZeros
	return nbZeros, rem
}

func (pow PersonalizedAdaptivePoW) GetGlobalDifficulty(oldBlock Block) (globalDiff uint, prevTime []int64, issuerFrame []IssuedBlock) {

	dtSize := math.Min(float64(oldBlock.Index), DtDiffEval)

	elapsed := 0.0
	speed := 100.0
	if oldBlock.Index > 0 {
		elapsed = float64(oldBlock.Timestamp - pow.PreviousBlockTimes[len(pow.PreviousBlockTimes)-1])
	}

	if elapsed > 0 {
		speed = float64(dtSize) / elapsed
	}

	newBasePow := pow.CommonDifficulty

	if speed >= pow.MaxSpeed() {
		if (pow.CommonDifficulty+2)%16 == 0 {
			newBasePow += 2
		} else {
			newBasePow += 1
		}
	}

	if speed <= pow.MinSpeed() {
		if pow.CommonDifficulty%16 == 0 {
			newBasePow -= 2
		} else {
			newBasePow -= 1
		}
	}

	// copy the block time history
	var newPrevious = []int64{oldBlock.Timestamp}
	trimTo := len(pow.PreviousBlockTimes)
	if trimTo >= int(dtSize) && trimTo > 0 {
		trimTo = trimTo - 1
	}
	newPrevious = append(newPrevious, pow.PreviousBlockTimes[:trimTo]...)

	// copy the issuerFrame
	var newIssuerFrame = []IssuedBlock{{
		Issuer:      oldBlock.PublicKey,
		Time:        oldBlock.Timestamp,
		IssuerCount: 0.0,
	}}
	newIssuerFrame = append(newIssuerFrame, pow.IssuersFrame...)

	return uint(newBasePow), newPrevious, newIssuerFrame

}

func (pow PersonalizedAdaptivePoW) GetIssuerFrame(block Block, issuerCount int) int {

	// calc the frame var
	head_1 := pow.IssuersFrame[0]

	frameVar := 0
	if block.Index > 0 {
		delta := issuerCount - head_1.IssuerCount
		if head_1.FrameVar > 0 {
			frameVar = head_1.FrameVar + 5*delta - 1
		} else if head_1.FrameVar < 0 {
			frameVar = head_1.FrameVar + 5*delta + 1
		} else {
			frameVar = head_1.FrameVar + 5*delta
		}
	}

	frame := head_1.Frame

	// return the frame size
	if block.Index == 0 {
		return 1
	} else if frameVar > 0 {
		return frame + 1
	} else if frameVar < 0 {
		return frame - 1
	} else {
		return frame
	}
}

func (pow PersonalizedAdaptivePoW) GetMedianAndCount(user string) (medianOfBlocksCount float64, nbPersonalBlocksInFrame float64, nbBlockSinceLast float64, issuersCount float64, previousIssuersCount float64) {
	nbBlockSinceLast = 0.0
	previousIssuersCount = 0.0

	// count blocks per user in the window
	// find first occurence of the user
	blocksOfIssuer := map[string]uint16{}
	for i := len(pow.IssuersFrame); i >= 0; i-- {

		bl := pow.IssuersFrame[i]

		if val, ok := blocksOfIssuer[bl.Issuer]; ok {
			blocksOfIssuer[bl.Issuer] = val + 1
		} else {
			blocksOfIssuer[bl.Issuer] = 1
		}

		if bl.Issuer == user && nbBlockSinceLast > 0 {
			nbBlockSinceLast = float64(i)
			previousIssuersCount = float64(bl.IssuerCount)
		}
	}

	// extract the median of block counts as welle as the users's block count
	counts := []uint16{}
	for key, val := range blocksOfIssuer {
		counts = append(counts, val)
		if key == user {
			nbPersonalBlocksInFrame = float64(val)
		}
	}
	sort.Slice(counts, func(i, j int) bool {
		return i < j
	})

	medianOfBlocksCount = 1.0
	issuersCount = float64(len(counts))

	if len(counts) > 0 {
		medianOfBlocksCount = float64(counts[len(counts)/2])
	}

	return medianOfBlocksCount, nbPersonalBlocksInFrame, nbBlockSinceLast, issuersCount, previousIssuersCount
}

func (pow PersonalizedAdaptivePoW) DifficultyFor(user string) (uint, float64) {
	var nbPersonalBlocksInFrame = 0.0
	medianOfBlocksInFrame, nbPersonalBlocksInFrame, nbBlockSinceLast, issuersCount, previousIssuersCount := pow.GetMedianAndCount(user)

	var personalExcess = math.Max(0, ((nbPersonalBlocksInFrame+1)/medianOfBlocksInFrame)-1)
	var personalHandicap = math.Floor(math.Log(1+personalExcess) / math.Log(1.189))

	common := float64(pow.CommonDifficulty)

	userDifficulty := personalHandicap + math.Max(common, common*math.Floor(PercentRot*previousIssuersCount)/(1.0+nbBlockSinceLast))

	if int(userDifficulty+1)%16 == 0 {
		userDifficulty += 1
	}

	return uint(userDifficulty), issuersCount
}

func (pow PersonalizedAdaptivePoW) MinGenTime() float64 {
	return math.Floor(pow.AvgGenTime / 1.189)
}
func (pow PersonalizedAdaptivePoW) MaxGenTime() float64 {
	return math.Ceil(pow.AvgGenTime * 1.189)
}
func (pow PersonalizedAdaptivePoW) MaxSpeed() float64 {
	return 1 / pow.MinGenTime()
}
func (pow PersonalizedAdaptivePoW) MinSpeed() float64 {
	return 1 / pow.MaxGenTime()
}
