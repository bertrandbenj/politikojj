package model

import (
	"bytes"
	"fmt"
)

type Currency struct {
	Name         string
	MonetaryMass uint64
	UnitBase     uint8
	//Inflation     float32
	CitizenCount  uint32
	Dividend      uint64
	Wallets       map[string]uint64
	JointWallets  map[string]uint64
	PublicWallets map[string]uint64
}

type Wallet struct {
	PublicKey string
	Balance   uint64
}

type JointWallet struct {
	PublicKeys []string
	Balance    uint64
}

type PublicWallet struct {
	Name    string
	Balance uint64
}
type PublicAccount struct {
	Name       string
	Permission string
}

type Transaction struct {
	SignedStampedDoc
	To     string
	Amount uint64
}

func (t Transaction) PartToSign() []byte {
	buf := bytes.NewBuffer(t.SignedStampedDoc.PartToSign())
	buf.WriteString(t.To)
	buf.WriteString(fmt.Sprintf("%d", t.Amount))
	return buf.Bytes()
}

func (ccy Currency) SumAccounts() uint64 {
	sum := uint64(0)
	for _, v := range ccy.Wallets {
		sum += v
	}
	for _, v := range ccy.JointWallets {
		sum += v
	}
	for _, v := range ccy.PublicWallets {
		sum += v
	}
	return sum
}
