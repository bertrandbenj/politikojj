package model

import (
	"bytes"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"strings"
)

// const (
// 	DtDiffEval = 12 // window size (# of blocks) to compute pow difficulty
// 	AvgGenTime = 10 // # of seconds between each blocks
// )

// var MinGenTime = math.Floor(AvgGenTime / 1.189)
// var MaxGenTime = math.Ceil(AvgGenTime * 1.189)
// var MaxSpeed = 1 / MinGenTime
// var MinSpeed = 1 / MaxGenTime

// the hash is not part of the block itself as it is the ipfs cid
type Block struct {
	// header
	Index    uint32
	Previous CidLink `json:",omitempty"`
	Network  string

	// data
	Demos     CidLink
	Cratos    CidLink
	Currency  CidLink
	Notary    CidLink `json:",omitempty"`
	Evidences []Evidence

	// validation
	SignedStampedDoc
	Difficulty CidLink
	Nonce      string
	PoWHash    string
}

func (block Block) IsHashValid(difficulty PersonalizedAdaptivePoW) bool {
	zeros, rem := difficulty.GetNbZerosAndRemainder()
	prefix := strings.Repeat("0", zeros)
	if !strings.HasPrefix(block.PoWHash, prefix) {
		return false
	}
	remain := strings.TrimPrefix(block.PoWHash, prefix)[0:1]

	switch rem {
	case 0:
		return strings.ContainsAny(remain, "0123456789ABCDEF")
	case 1:
		return strings.ContainsAny(remain, "0123456789ABCDE")
	case 2:
		return strings.ContainsAny(remain, "0123456789ABCD")
	case 3:
		return strings.ContainsAny(remain, "0123456789ABC")
	case 4:
		return strings.ContainsAny(remain, "0123456789AB")
	case 5:
		return strings.ContainsAny(remain, "0123456789A")
	case 6:
		return strings.ContainsAny(remain, "0123456789")
	case 7:
		return strings.ContainsAny(remain, "012345678")
	case 8:
		return strings.ContainsAny(remain, "01234567")
	case 9:
		return strings.ContainsAny(remain, "0123456")
	case 10:
		return strings.ContainsAny(remain, "012345")
	case 11:
		return strings.ContainsAny(remain, "01234")
	case 12:
		return strings.ContainsAny(remain, "0123")
	case 13:
		return strings.ContainsAny(remain, "012")
	case 14:
		return strings.ContainsAny(remain, "01")
	case 15:
		return strings.ContainsAny(remain, "0")
	}
	return false
}

func (block *Block) HashPow() {
	record := block.PartToHash()
	h := sha256.New()
	h.Write([]byte(record))
	hashed := h.Sum(nil)
	(*block).PoWHash = hex.EncodeToString(hashed)
}

func (block Block) Redacted() interface{} {
	res, _ := json.MarshalIndent(block, "", "  ")
	return string(res)
}

func (block Block) PartToSign() []byte {

	evidences := ""
	for _, v := range block.Evidences {
		evidences += v.Link
	}
	buf := bytes.NewBufferString(fmt.Sprint(block.Index) +
		fmt.Sprint(block.Timestamp) +
		block.Previous.Link +
		block.Demos.Link +
		block.Cratos.Link +
		block.Currency.Link +
		block.Notary.Link +
		evidences +
		block.Difficulty.Link +
		block.PublicKey)

	return buf.Bytes()
}

func (block Block) PartToHash() string {
	return block.Signature + block.Nonce
}
