package model

type CitizenOfName struct {
	PublicKey        []string
	CitizenshipClaim CidLink
}

type PubkeysOfRole struct {
}

//func (por *PubkeysOfRole) Add(cid string) {
//	por.PublicKey = append(por.PublicKey, CidLink{cid})
//}

type Demos struct {
	Population       uint64
	CitizensByName   map[string]CitizenOfName
	PublicKeysByRole map[string][]string
}
