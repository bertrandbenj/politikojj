package model

import (
	"bytes"
	"errors"
	"reflect"
	"strconv"

	"github.com/op/go-logging"
)

var log = logging.MustGetLogger("chain")

type OpenBallot struct {
	Permission string
	VoteObject string
	VoteValue  string
	SignedStampedDoc
}

func (b OpenBallot) PartToSign() []byte {
	buf := bytes.NewBuffer(b.SignedStampedDoc.PartToSign())
	buf.WriteString(b.Permission)
	buf.WriteString(b.VoteObject)
	buf.WriteString(b.VoteValue)
	return buf.Bytes()
}

type Rule struct {
	Subject   string
	Predicate string
	Object    string
}

type Amendment struct {
	Name       string
	Predicates []Rule
	SignedStampedDoc
}

func (a Amendment) PartToSign() []byte {
	buf := bytes.NewBuffer(a.SignedStampedDoc.PartToSign())
	buf.WriteString(a.Name)
	return buf.Bytes()
}

//func  (a Amendment) Details() (string, string) {
//	return a.Name, a.Name
//}

//func (a Amendment) VoteType() int {
//	return VoteAmendment
//}

type Roles struct {
	Permission  map[string][]string
	Inheritance map[string][]string
}

type UnitIntervalVariable float64

type CategoryVariable string

type IntegerVariable int64

//type Votable interface {
//	VoteType() int
//}

type VoteableVariable interface {
	IsLegal(s string) bool
	//Parse(s string, res interface{})
	Description() string
	FloatValue() float64
}

type Value interface{}

type Ballots struct {
	Vote []OpenBallot
}

type Claims struct {
	Claim []ApprovedClaim
}

type Tax struct {
	Name     string
	Variable string
	Account  string
	Type     uint8
}

type Vars struct {
	//Name string
	Type      string
	Value     interface{}
	VotesByPk map[string]interface{}
}

func (variable *Vars) Avg() interface{} {
	var avgF = float64(0)
	for _, v := range variable.VotesByPk {
		switch typ := v.(type) {
		case float64:
			avgF += typ
		default:
			log.Info("Unhandled Type", reflect.TypeOf(v))
		}
	}
	avgF /= float64(len(variable.VotesByPk))
	(*variable).Value = avgF
	return avgF
}

type Cratos struct {
	Roles Roles
	//Votes              map[string]OpenBallot
	Claims           Claims
	Variables        map[string]Vars
	ProposePredicate []string
	//VarVotesByNameByPk map[string]map[string]interface{}
	Taxes          map[string]Tax
	PublicAccounts map[string]PublicAccount
}

func (v UnitIntervalVariable) IsLegal(s string) bool {
	if res, err := strconv.ParseFloat(s, 32); err == nil {
		if res >= 0 && res <= 1 {
			return true
		}
	}
	return false
}

func ParseUnitIntervalVar(s string) UnitIntervalVariable {
	if res, err := strconv.ParseFloat(s, 64); err == nil {
		if res >= 0 && res <= 1 {
			return UnitIntervalVariable(res)
		}
	}
	return 0
}

func ParseIntegerVar(s string) IntegerVariable {
	if res, err := strconv.ParseInt(s, 0, 64); err == nil {
		return IntegerVariable(res)
	}
	return IntegerVariable(0)
}

func ParseCategoryVar(s string) CategoryVariable {
	return CategoryVariable(s)
}

func (v UnitIntervalVariable) Description() string {
	return "a floating point number in the interval [0,1]"
}

func (v UnitIntervalVariable) FloatValue() float64 {
	return float64(v)
}

func (v CategoryVariable) IsLegal(s string) bool {
	return true
}

func (v CategoryVariable) Description() string {
	return "A String representation of the Category"
}
func (v CategoryVariable) FloatValue() float64 {
	err := errors.New("CategoryVariable cannot return a float value")

	log.Error(err)
	return 0.0
}

func (v IntegerVariable) IsLegal(s string) bool {
	_, err := strconv.Atoi(s)
	return err == nil
}

func (v IntegerVariable) Description() string {
	return "an integer value point number in the interval [0,1]"
}
func (v IntegerVariable) FloatValue() float64 {

	return float64(v)
}
