module gitlab.com/bertrandbenj/politikojj

go 1.15

require (
	github.com/btcsuite/btcutil v1.0.2
	github.com/davidlazar/go-crypto v0.0.0-20200604182044-b73af7476f6c // indirect
	github.com/golang/groupcache v0.0.0-20210331224755-41bb18bfe9da // indirect
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/mux v1.8.0
	github.com/ipfs/go-ipfs-api v0.2.0
	github.com/joho/godotenv v1.3.0
	github.com/libp2p/go-libp2p v0.14.1
	github.com/libp2p/go-libp2p-core v0.8.5
	github.com/libp2p/go-sockaddr v0.1.1 // indirect
	github.com/mikespook/gorbac v2.1.0+incompatible
	github.com/mitchellh/mapstructure v1.4.1
	github.com/multiformats/go-multiaddr v0.3.2
	github.com/onsi/ginkgo v1.16.4 // indirect
	github.com/op/go-logging v0.0.0-20160315200505-970db520ece7
	github.com/stretchr/testify v1.7.0
	github.com/tevino/abool v1.2.0
	golang.org/x/crypto v0.0.0-20210513164829-c07d793c2f9a
	golang.org/x/lint v0.0.0-20210508222113-6edffad5e616 // indirect
	golang.org/x/mod v0.4.2 // indirect
	golang.org/x/net v0.0.0-20210525063256-abc453219eb5 // indirect
	golang.org/x/sys v0.0.0-20210603125802-9665404d3644 // indirect
	golang.org/x/text v0.3.6 // indirect
	golang.org/x/tools v0.1.2 // indirect
)
