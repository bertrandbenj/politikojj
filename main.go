package main

import (
	"os"

	"github.com/joho/godotenv"
	"github.com/op/go-logging"
	"gitlab.com/bertrandbenj/politikojj/utilities"
)

var log = logging.MustGetLogger("main")

func main() {

	err := godotenv.Load()
	if err != nil {
		log.Error(err)
	}

	utilities.ConfigLogger(os.Getenv("LOGLEVEL"))

	ExecuteSubCommand()

}
