package utilities

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/bertrandbenj/politikojj/model"
	"os"
	"testing"
)

func TestDailyUBI(t *testing.T) {
	var mm = uint64(0)
	pop := uint64(1000)
	c := AnnualRateToDailyRate(0.0982)
	ud := uint64(10000)
	base := uint8(0)

	for x := 0; x < 80; x++ {
		for i := 0; i < 365; i++ {
			ud, base = DailyUBI(ud, c, mm, pop, base)
			mm += uint64(ud) * pop
		}
		log.Info("at the end of year", x, ud, c, mm)
	}
}

func TestMin(t *testing.T) {
	x, y := uint32(10), uint32(11)
	assert.Equal(t, Min(x, y), x)
	assert.Equal(t, Min(y, x), x)
}

func TestDeferCloses(t *testing.T) {
	f, _ := os.Create("/tmp/exFile")
	DeferCloses(f, "huhu")
}

func TestNow(t *testing.T) {
	assert.NotNil(t, Now())
}

func TestContains(t *testing.T) {
	arr := []string{"A", "B"}
	assert.True(t, Contains(arr, "A"))
	assert.False(t, Contains(arr, "C"))
}

func TestConfigLogger(t *testing.T) {
	ConfigLogger("DEBUG")
}

func TestAnnualRateToDailyRate(t *testing.T) {
	assert.Greater(t, AnnualRateToDailyRate(10.0), 0.0)
}

func TestDailyUbi(t *testing.T) {
	ccy := model.Currency{
		Name:          "test",
		MonetaryMass:  100000,
		UnitBase:      0,
		CitizenCount:  0,
		Dividend:      10000,
		Wallets:       map[string]uint64{},
		JointWallets:  map[string]uint64{},
		PublicWallets: map[string]uint64{},
	}

	ccy.Wallets["w1"] = 40000
	ccy.Wallets["w2"] = 60000

	ud, base := DailyUbi(ccy, .1, 2)
	assert.Equal(t, uint8(0), base)
	assert.Greater(t, ud, uint64(0))
}
