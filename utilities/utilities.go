package utilities

import (
	"math"
	"os"
	"time"

	"github.com/op/go-logging"
	"gitlab.com/bertrandbenj/politikojj/model"

	"io"
	"reflect"
)

var log = logging.MustGetLogger("utilities")

func DeferCloses(x io.Closer, prefix string) {
	defer func() {
		if err := x.Close(); err != nil {
			log.Error("Closing "+prefix, reflect.TypeOf(x), err)
		} else {
			log.Info("Closed "+prefix, reflect.TypeOf(x))
		}
	}()
}

func AnnualRateToDailyRate(r float64) float64 {
	return math.Pow(1.0+r, 1/365.25) - 1
}

func DailyUbi(ccy model.Currency, inflation float64, pop uint64) (ud uint64, newBase uint8) {
	mm := ccy.MonetaryMass
	c := AnnualRateToDailyRate(inflation)
	prevUd := ccy.Dividend
	base := ccy.UnitBase

	return DailyUBI(prevUd, c, mm, pop, base)
}

func DailyUBI(lastUD uint64, c float64, mm uint64, pop uint64, base uint8) (ud uint64, newBase uint8) {
	//dt := 86400.
	//dtReeval := 15778800.
	moneyShare := math.Ceil(float64(mm)/math.Pow10(int(base))) / float64(pop)
	reEvalUd := uint64(math.Ceil(float64(lastUD) + c*c*moneyShare))
	if float64(reEvalUd) >= math.Pow10(5) {
		log.Info("Incrementing base", base)
		reEvalUd = uint64(math.Round(math.Ceil(float64(reEvalUd) / 10.)))
		newBase = base + 1
	}
	return reEvalUd, base
}

func Contains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

// Min returns the smaller of x or y.
func Min(x, y uint32) uint32 {
	if x > y {
		return y
	}
	return x
}

func Now() int64 { return time.Now().UTC().Unix() }

func ConfigLogger(lvl string) {
	var format = logging.MustStringFormatter(
		`%{time:15:04:05.000} %{color}%{level:.4s} ▶ %{shortpkg:.3s}.%{shortfunc:-16s}%{color:reset} %{message}`,
	)

	backend2 := logging.NewLogBackend(os.Stderr, "", 0)

	//logging.SetLevel( backend2.Logger.LogLevel("INFO"),"")
	backend2Formatter := logging.NewBackendFormatter(backend2, format)

	// Only errors and more severe messages should be sent to backend2
	backend2Leveled := logging.AddModuleLevel(backend2Formatter)
	LevL, _ := logging.LogLevel(lvl)
	backend2Leveled.SetLevel(LevL, "")

	logging.SetBackend(backend2Leveled)
}
