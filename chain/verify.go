package chain

import (
	"encoding/hex"

	"gitlab.com/bertrandbenj/politikojj/crypto"
	"gitlab.com/bertrandbenj/politikojj/model"
)

func (cs BlockChainService) IsBlockValid(newBlock model.Block) bool {

	if cs.network != newBlock.Network {
		return false
	}

	pubkey := crypto.ParsePublicKey(newBlock.PublicKey)
	sign, _ := hex.DecodeString(newBlock.GetSignature())

	if ok, err := pubkey.Verify(newBlock.PartToSign(), sign); !ok {
		log.Warning("Block Signature invalid", err)
		return false
	}

	var difficulty model.PersonalizedAdaptivePoW
	cs.Store.GetObject(newBlock.Difficulty.Link, &difficulty)

	if !newBlock.IsHashValid(difficulty) {
		log.Warning("Block Hash invalid", newBlock.PoWHash)
		return false
	}

	return true
}

func (cs BlockChainService) IsBlockDeltaValid(new, old model.Block) bool {
	if new.Index != old.Index+1 {
		log.Warning("block invalid... index increment not respected")
		return false
	}
	return true

}
