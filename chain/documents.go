package chain

import (
	"gitlab.com/bertrandbenj/politikojj/crypto"
	"gitlab.com/bertrandbenj/politikojj/model"
	"gitlab.com/bertrandbenj/politikojj/utilities"
)

func MakeAmendment(name string, rules []model.Rule, key crypto.Keypair) model.Amendment {
	var res = model.Amendment{
		Name:       name,
		Predicates: rules,
	}

	StampAndSign(&res, key)
	return res
}

func MakeClaim(name string, role string, key crypto.Keypair) model.Claim {
	var res = model.Claim{
		ChosenName: name,
		Role:       role,
	}
	StampAndSign(&res, key)
	return res
}

// StampAndSign the object using the given private key
func StampAndSign(obj model.Signable, key crypto.Keypair) {
	stamp := model.SignedStampedDoc{
		Timestamp: utilities.Now(),
		PublicKey: key.PubID(),
	}

	obj.Stamp(stamp)

	signature, err := key.Sign(obj.PartToSign())
	if err != nil {
		log.Errorf("signing %s, %s ", obj, err)
	}
	stamp.Signature = string(signature)

	obj.Stamp(stamp)

}

func MakeApprovedClaim(c model.Claim, key crypto.Keypair) model.ApprovedClaim {
	res := model.ApprovedClaim{
		Claim: c,
	}

	StampAndSign(&res, key)
	return res
}

func MakeVote(perm string, object string, value string, key crypto.Keypair) model.OpenBallot {

	var res = model.OpenBallot{
		Permission: perm,
		VoteObject: object,
		VoteValue:  value,
	}

	StampAndSign(&res, key)
	return res
}

func MakeTransaction(to string, amount int, key crypto.Keypair) model.Transaction {
	tx := model.Transaction{
		To:     to,
		Amount: uint64(amount),
	}
	StampAndSign(&tx, key)
	return tx
}
