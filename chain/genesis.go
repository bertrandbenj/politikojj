package chain

import (
	"gitlab.com/bertrandbenj/politikojj/crypto"
	"gitlab.com/bertrandbenj/politikojj/model"
)

func (cs BlockChainService) addCitizen(d *model.Demos, key crypto.Keypair, name string) {
	pid := key.PubID()

	c, _ := cs.claimCitizenship(name, key)
	_, accId := cs.approveClaim(c, key)

	d.CitizensByName[c.ChosenName] = model.CitizenOfName{
		PublicKey:        []string{pid},
		CitizenshipClaim: model.CidLink{Link: accId}}

	d.PublicKeysByRole["Citizen"] = []string{pid}

}

/*
	build and publish the Genesis block using a keypair and a constitution file

	returns its ipfs cid
*/
func (cs BlockChainService) Genesis(key crypto.Keypair, constitutionFile string) model.Block {
	var constCid string

	if len(constCid) == 0 {
		constCid = cs.Store.StoreFile(constitutionFile)
		log.Infof("constitution read local %s, stored as %s", constitutionFile, constCid)
	}

	genesisBlock := model.Block{
		Previous:   model.CidLink{Link: constCid},
		Network:    "TestNet",
		Demos:      cs.buildDemos(key),
		Cratos:     cs.buildCratos(),
		Notary:     cs.buildNotary(),
		Currency:   cs.buildCcy(key),
		Difficulty: cs.buildPow(),
		Evidences:  []model.Evidence{},
		Nonce:      "0x010000",
	}

	StampAndSign(&genesisBlock, key)

	return genesisBlock
}

/*
	build and publish Demos with one user being the node's pubkey

	returns its ipfs cid
*/
func (cs BlockChainService) buildDemos(key crypto.Keypair) model.CidLink {

	demos := model.Demos{
		Population:       uint64(1),
		CitizensByName:   make(map[string]model.CitizenOfName),
		PublicKeysByRole: make(map[string][]string),
	}
	cs.addCitizen(&demos, key, "Cleisthenes")
	demosCid, err := cs.Store.PutObject(demos)
	if err != nil {
		log.Info("error inserting Demos PutObject ", err)
	}

	return model.CidLink{Link: demosCid}

}

/*
	build and publish Currency with one account for the first node's account

	returns its ipfs cid
*/
func (cs BlockChainService) buildCcy(key crypto.Keypair) model.CidLink {

	ccy := model.Currency{
		Name:          "CDT",
		MonetaryMass:  0,
		UnitBase:      0,
		Dividend:      uint64(100000),
		Wallets:       make(map[string]uint64),
		JointWallets:  make(map[string]uint64),
		PublicWallets: make(map[string]uint64),
	}

	ccy.Wallets[key.PubID()] = ccy.Dividend
	ccy.MonetaryMass = ccy.Dividend

	ccyCid, err := cs.Store.PutObject(ccy)
	if err != nil {
		log.Info("error inserting Curency PutObject ", err)
	}
	return model.CidLink{Link: ccyCid}

}

/*
	build and publish Cratos

	returns its ipfs cid
*/
func (cs BlockChainService) buildCratos() model.CidLink {
	cratos := model.Cratos{
		Roles: model.Roles{
			Permission: map[string][]string{
				model.Citizen: {
					model.VoteGrantClaim,
					model.VoteRevokeClaim,
					model.VoteGrantPermissionToRole,
					model.VoteRevokePermissionToRole,
					model.VoteVariableValue,
					model.VoteAmendment,
				},
				model.CitizenshipOfficer: {
					"GrantCitizenship",
					"RevokeCitizenship"}},
			Inheritance: map[string][]string{
				"CitizenshipOfficer": {model.Citizen},
			}},
		//Votes:  make(map[string]model.OpenBallot),
		Claims: model.Claims{},
		ProposePredicate: []string{
			model.ProposeCreateAccount,
			model.ProposeNewVariable,
			model.ProposeTaxRevenueTarget,
			model.ProposeTaxType,
			model.ProposeAccountPermission,
			model.ProposeNewRole,
			model.ProposeAddPermission,
			model.ProposeTax,
			model.ProposeTaxVar,
		},
		Variables: make(map[string]model.Vars),
		//VarVotesByNameByPk: make(map[string]map[string]interface{}),
		Taxes:          make(map[string]model.Tax),
		PublicAccounts: make(map[string]model.PublicAccount)}

	cratos.Variables["Inflation"] = model.Vars{
		Type:      model.UnitIntervalType,
		Value:     0.08,
		VotesByPk: make(map[string]interface{}),
	}

	//node.DeserializeFromCratos(cratos)
	//log.Info("test rbac ", node.IsAuthorized("Citizen", "VoteTransactionTax"))
	cratosCid, err := cs.Store.PutObject(cratos)
	if err != nil {
		log.Error("error inserting Cratos PutObject ", err)
	}
	return model.CidLink{Link: cratosCid}
}

/*
	build and publish empty Notary

	returns its ipfs cid
*/
func (cs BlockChainService) buildNotary() model.CidLink {

	notary := model.Notary{
		Documents: []model.MultiPartyDocument{},
	}
	notaryCid, err := cs.Store.PutObject(notary)
	if err != nil {
		log.Error("error inserting Notary PutObject ", err)
	}

	return model.CidLink{Link: notaryCid}
}

/*
	build and publish PersonalizedAdaptivePoW

	returns its ipfs cid
*/
func (cs BlockChainService) buildPow() model.CidLink {

	diff := model.PersonalizedAdaptivePoW{
		AvgGenTime:         10, // # of seconds between each blocks
		CommonDifficulty:   70,
		PreviousBlockTimes: []int64{},
		PersonalHandicap:   nil,
		IssuersFrame:       []model.IssuedBlock{},
	}

	diffCid, err := cs.Store.PutObject(diff)
	if err != nil {
		log.Info("error inserting Curency PutObject ", err)
	}
	return model.CidLink{Link: diffCid}
}

/*
	helper function to build a claim document and store it


*/
func (cs BlockChainService) claimCitizenship(name string, key crypto.Keypair) (claim model.Claim, cid string) {
	claim = MakeClaim(name, model.Citizen, key)
	cid, err := cs.Store.PutObject(claim)
	if err != nil {
		log.Info("Error Claiming citizenship in PutObject ", err)
	}
	return claim, cid
}

func (cs BlockChainService) approveClaim(c model.Claim, key crypto.Keypair) (approvedClaim model.ApprovedClaim, cid string) {
	//c := Pools.Claims[cCid]
	approved := MakeApprovedClaim(c, key)
	cid, err := cs.Store.PutObject(approved)
	if err != nil {
		log.Error("Error approving claim in PutObject ", err)
		return
	}
	//delete(Pools.Claims, cCid)
	return approved, cid
}
