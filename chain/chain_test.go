package chain

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/bertrandbenj/politikojj/crypto"
	"gitlab.com/bertrandbenj/politikojj/model"
	storage "gitlab.com/bertrandbenj/politikojj/storage/mock"
)

var key crypto.Keypair
var chain *BlockChainService

func init() {
	key = crypto.GenEd25519()
	//key = crypto.ReadPubAndPrivateArmorKeyFile("../testset/pub.asc", "../testset/priv.asc", "test1")
	store := storage.NewFileSystemStore("/tmp/testchain/")
	chain = NewChain(store, "TestNet")

}

func TestIsHashValid(t *testing.T) {

	var diff = model.PersonalizedAdaptivePoW{
		AvgGenTime:         10,
		CommonDifficulty:   90,
		PreviousBlockTimes: []int64{},
		PersonalHandicap:   nil,
		IssuersFrame:       []model.IssuedBlock{},
	}

	var b = model.Block{
		Nonce:   "0x0100000000",
		PoWHash: "00000321654987",
	}

	assert.True(t, b.IsHashValid(diff), "hash should be valid")

}

func TestMakeAmendment(t *testing.T) {
	a := MakeAmendment("abc", []model.Rule{}, key)
	assert.NotNil(t, a, "amendment should not be nil")
}

func TestMakeClaim(t *testing.T) {
	c := MakeClaim("abc", model.Citizen, key)
	assert.NotNil(t, c, "claim should not be nil")
	assert.NotNil(t, c.PublicKey)
}

func TestMakeApprovedClaim(t *testing.T) {
	c := MakeClaim("abc", model.Citizen, key)
	ac := MakeApprovedClaim(c, key)
	assert.NotNil(t, ac, "Approved Claim should not be nil")
}

func TestMakeTransaction(t *testing.T) {
	c := MakeTransaction("abc", 1234, key)
	assert.NotNil(t, c, "Transaction should not be nil")
}

func TestMakeVote(t *testing.T) {
	c := MakeVote(model.VoteGrantClaim, "object", "value", key)
	assert.NotNil(t, c, "Vote should not be nil")
}

func TestBlockChainService_IsBlockValid(t *testing.T) {

	genesis := chain.Genesis(key, "../docs/constitution.md")

	genesis.PoWHash = "FFFF"
	assert.False(t, chain.IsBlockValid(genesis))

	chain.network = "BreakNetwork"
	assert.False(t, chain.IsBlockValid(genesis))
	chain.network = genesis.Network

	genesis.Signature = "breaksignature"
	assert.False(t, chain.IsBlockValid(genesis))

	StampAndSign(&genesis, key)
	genesis.PoWHash = "000000FEDCBA9876543210"
	assert.True(t, chain.IsBlockValid(genesis))

}

func TestBlockChainService(t *testing.T) {

	genesis := chain.Genesis(key, "../docs/constitution.md")

	chain.network = "Break"
	assert.False(t, chain.IsBlockValid(genesis))
	chain.network = genesis.Network

	assert.False(t, chain.IsBlockValid(genesis))

	_, _, _ = chain.ChainBlock(genesis)
	ub := chain.GetHead()
	assert.NotNil(t, ub)
	assert.NotNil(t, chain.HeadBlock())
	assert.NotNil(t, chain.ByIndex(0))
	assert.Equal(t, uint64(1), ub.Majority())

	key2 := crypto.GenEd25519()

	chain.addCitizen(&ub.Demos, key2, "Citizen2")

	key3 := crypto.GenEd25519()

	c, _ := chain.claimCitizenship("Citizen3", key3)
	assert.NotNil(t, c)
	_, accId := chain.approveClaim(c, key)
	assert.NotNil(t, accId)

	chain.BackwardWalk(func(block model.Block, cid string) {})

	assert.False(t, chain.IsBlockDeltaValid(genesis, genesis))

}
