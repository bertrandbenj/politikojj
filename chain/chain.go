package chain

import (
	"sync"

	"github.com/op/go-logging"
	"gitlab.com/bertrandbenj/politikojj/model"
	"gitlab.com/bertrandbenj/politikojj/storage"
)

var log = logging.MustGetLogger("chain")

type UnpackedBlock struct {
	Cid      string
	Block    model.Block
	Cratos   model.Cratos
	Demos    model.Demos
	Currency model.Currency
	model.ObservableList
}

//type Blockchain map[string]model.Block

func (c UnpackedBlock) Majority() uint64 {
	return c.Demos.Population/2 + 1
}

type WalkHandler func(block model.Block, cid string)

type BlockChainService struct {
	Head    UnpackedBlock
	Store   storage.Store
	byIndex map[uint32]string
	network string
	lock    *sync.RWMutex
}

func (cs *BlockChainService) GetHead() UnpackedBlock {
	cs.lock.RLock()
	defer cs.lock.RUnlock()
	return cs.Head
}

func NewChain(store storage.Store, network string) *BlockChainService {
	return &BlockChainService{
		Store:   store,
		Head:    UnpackedBlock{},
		byIndex: make(map[uint32]string),
		network: network,
		lock:    &sync.RWMutex{},
	}
}

//func (cs *BlockChainService) HeadCid() string {
//	return cs.Head.Cid
//}

func (cs *BlockChainService) HeadBlock() model.Block {
	return cs.GetHead().Block
}

//func (cs *BlockChainService) HeadSet() *UnpackedBlock {
//	return &cs.Head
//}
//
//func (cs BlockChainService) SignBlock(block *model.Block, key crypto.PrivateK) {
//	s, _ := key.Sign(block.PartToSign())
//	block.Signature = s
//}

func (cs BlockChainService) BackwardWalk(handler WalkHandler) {
	cid := cs.Head.Cid
	block := model.Block{}

	for count := 0; count < 30; count++ {
		found := cs.Store.GetObject(cid, &block)
		if !found {
			break
		}
		handler(block, cid)
		cid = block.Previous.Link
	}
}

func (cs *BlockChainService) ByIndex(id uint32) model.Block {
	block := model.Block{}
	ok := cs.Store.GetObject(cs.byIndex[id], &block)
	if !ok {
		log.Warning("get by index")
	}

	return block
}

func (cs *BlockChainService) push(cid string, newBlock model.Block) {
	//cs.blockchain[cid] = newBlock
	cs.lock.Lock()
	defer cs.lock.Unlock()

	cs.Head.Cid = cid
	cs.Head.Block = newBlock

	var demos model.Demos
	if cs.Store.GetObject(newBlock.Demos.Link, &demos) {
		cs.Head.Demos = demos
	}
	var cratos model.Cratos
	if cs.Store.GetObject(newBlock.Cratos.Link, &cratos) {
		cs.Head.Cratos = cratos
	}
	var ccy model.Currency
	if cs.Store.GetObject(newBlock.Currency.Link, &ccy) {
		cs.Head.Currency = ccy
	}

	cs.Head.Block = newBlock
	cs.byIndex[newBlock.Index] = cid
	go cs.Store.UpdateHead(cid)
}

func (cs *BlockChainService) ChainBlock(newBlock model.Block) (model.Block, string, error) {

	cid, err := cs.Store.PutObject(newBlock)

	if err != nil {
		log.Error("PutObject new block", err)
	}

	cs.push(cid, newBlock)

	cs.Head.NotifyAll()

	log.Noticef("\033[32m✓ Chained Block n°%d\033[0m %s", newBlock.Index, cid)

	return newBlock, cid, err
}
