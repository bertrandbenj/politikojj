function initBlockDiagram() {
    if (window.goSamples) goSamples();  // init for these samples -- you don't need to call this
    var graph = go.GraphObject.make;  // for conciseness in defining templates in this function

    myDiagram =
        graph(go.Diagram, "blocksDiagramDiv",
            { // enable undo & redo
                "undoManager.isEnabled": true
            });

    // define a simple Node template
    myDiagram.nodeTemplate =
        graph(go.Node, "Auto",  // the Shape will go around the TextBlock
            graph(go.Shape, "RoundedRectangle",
                {strokeWidth: 0, fill: "white"},  // default fill is white
                // Shape.fill is bound to Node.data.color
                new go.Binding("fill", "color")),
            graph(go.TextBlock,
                {margin: 8},  // some room around the text
                // TextBlock.text is bound to Node.data.key
                new go.Binding("text", "label"))
        );

    // but use the default Link template, by not setting Diagram.linkTemplate

    // create the model data that will be represented by Nodes and Links
    nodes = [
        {key: "Constitution", color: "lightblue"}
    ];

    links = [
        {from: "Constitution", to: 0}
    ];

    loadChartData()
}

function loadChartData(){
    // ================== LOAD the data  ==================
    var timedeltas = [];
    var blockIndices = [];
    var blockPoW = [];
    $.get(restapi + '/chain', function (result) {
        $('#headIPFSExplorer').attr('href', ipfsbrowser+ result[0].CID);

        function truncate(str, n){
            return (str.length > n) ? '...' + str.substr(str.length -n ) : str;
        }

        result.forEach(function (item, index) {
            //console.log(item, index)
            let id = item.Data.Index
            nodes.push({key: id, color: "lightblue", label:  id + " " + truncate(item.CID, 5)})
            links.push({from: id -1 , to: id})
        });

        //console.log(nodes, links)
        myDiagram.model = new go.GraphLinksModel(nodes, links);


        //console.log(result[0])
        var lastTime = result[result.length - 1].Data.Timestamp
        for (var i = result.length - 1; i >= 0; i--) {
            var bl = result[i].Data
            blockIndices.push(bl.Index)
            blockPoW.push(bl.Difficulty.CommonDifficulty)
            var t = bl.Timestamp
            timedeltas.push(t - lastTime)
            //console.log("at block ",bl.Index," at time ",t," delta ", t - lastTime)
            lastTime = t
        }

        lineChartData.datasets[0].data = timedeltas
        lineChartData.datasets[1].data = blockPoW
        //lineChartData.datasets[2].data = Array(result.length-1).fill(300)
        lineChartData.labels = blockIndices

        console.log("loaded chart data")
        window.myLine.update();
    }, "json")


}

