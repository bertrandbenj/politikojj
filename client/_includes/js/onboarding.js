var readArmorPk = async () => {

    let rawPK = $('#privatekey').val() || prompt("Enter your private key :")

    let armored = await openpgp.key.readArmored(rawPK);
    let privateKey = armored.keys[0]
    if (!privateKey) {
        alert("Please provide a proper PGP armored private key")
        return
    }

    if (!privateKey.isDecrypted()) {
        try {
            let passphrase = $('#passphrase').val() || prompt("Enter your passphrase :")
            await privateKey.decrypt(passphrase);
            return privateKey;
        } catch {
            alert("Wrong private key or wrong passphrase ")
        }
    }
};


function citizenshipClaim() {

    const claim = {
        ChosenName: $('#name').val(),
        Timestamp: now(),
        Role: "Citizen",
        PublicKey: $('#pubkeycidinput').val(),
        Signature: ""
    }

    const strToSign = claim.ChosenName + claim.Timestamp + claim.Role + claim.PublicKey


    readArmorPk().then(pk => signIt(pk, strToSign).then(sign =>
        postDoc('/claim/role', claim, sign, function (response) {
            ongoingClaims = response
            console.log("loaded ongoingClaims", ongoingClaims)
        })));
}

function keygen() {

    var passphrase = $('#passphrase').val();
    var name = $('#name').val();
    var email = $('#email').val();
    var comm = $('#comment').val();

    var gen = async () => {
        const {privateKeyArmored, publicKeyArmored, revocationCertificate} = await openpgp.generateKey({
            userIds: [{name: name, email: email, comment: comm}], // you can pass multiple user IDs
            //curve: 'ed25519',                                           // ECC curve name
            rsaBits: 2048,
            passphrase: passphrase           // protects the private key
        });

        console.log(privateKeyArmored);     // '-----BEGIN PGP PRIVATE KEY BLOCK ... '
        console.log(publicKeyArmored);      // '-----BEGIN PGP PUBLIC KEY BLOCK ... '
        console.log(revocationCertificate); // '-----BEGIN PGP PUBLIC KEY BLOCK ... '

        $('#privatekey').val(privateKeyArmored);
        $('#pubkey').val(publicKeyArmored);

        window.ipfs.add(publicKeyArmored).then(x => {
            var p = x[0].path
            console.log("res", x, "ipfs cid ", p)
            $('#pubkeycid').attr("href", ipfsservice + p);
            $('#pubkeycidinput').val(p);

            prepareFileSave(privateKeyArmored)
        })
    }

    gen();

}

function prepareFileSave(text) {

    var name = "priv.asc";
    var type = 'text/plain';
    var a = document.getElementById("savePrivateKey");
    var file = new Blob([text], {type: type});
    a.href = URL.createObjectURL(file);
    a.download = name;
}


