function loadPubKeys(){
    $.get(restapi+"/head/currency", function (result){
        console.log(result.Data.Wallets)

        var list = $('#pkList')
        Object.keys(result.Data.Wallets).forEach( function (item, key){
            console.log(key, item);
            list.append(`<option value='${item}' />`)
        })
    }, "json")
}

function txSubmit(){

    const tx = {
        From : $('#txFrom').val(),
        To :  $('#txTo').val(),
        Amount :  parseInt($('#txAmount').val()),
        Timestamp : now()
    }

    const sign =  tx.From+tx.To+tx.Timestamp+tx.Amount

    readArmorPk().then(pk => signIt(pk, sign).then(sign =>
        postDoc('/tx/send', tx,sign, function (res){
            alert(`transaction recorded at ${res.CID}`)
    })));

}