const signIt = async (privateKey, strToSign) => {

    console.log("signing with ", privateKey, " => ", strToSign)
    const signature = await openpgp.sign({
        message: openpgp.cleartext.fromText(strToSign),
        privateKeys: [privateKey],
        detached: true
    });
    //console.log("detachedSignature ", signature);
    return signature.signature;
};

const testIPFS = async () => {
    let x = await ipfsS.dag.resolve("bafyreicoppypdvg433njbif5sfz4avmeyrd4upe36yll27jnsdffb2lwfi")
    console.log("root", x)
    return x;
}

const postDoc = async (path, jsonDoc, signature, callback) => {
    jsonDoc.Signature = signature
    console.log("posting to " + path, jsonDoc)
    postJSON(restapi + path, jsonDoc, callback);
};

const postJSON = function (url, data, callback) {
    return jQuery.ajax({
        'type': 'POST',
        'url': url,
        'headers': {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            // 'Access-Control-Allow-Origin': '*'
        },
        "crossDomain": true,
        'contentType': 'application/json',
        'data': window.JSON.stringify(data),
        'dataType': 'json',
        'success': callback
    });
};