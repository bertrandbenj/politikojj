function saveSettingsToLocalStore() {
    // Store
    if ($('#restapi').val().trim().length > 0) {
        localStorage.setItem("restapi", $('#restapi').val());
    }else{
        localStorage.removeItem("restapi")
    }

    if ($('#ipfsservice').val().trim().length > 0) {
        localStorage.setItem("ipfsservice", $('#ipfsservice').val());
    }else{
        localStorage.removeItem("ipfsservice")
    }

    if ($('#ipfsbrowser').val().trim().length > 0) {
        localStorage.setItem("ipfsbrowser", $('#ipfsbrowser').val());
    }else{
        localStorage.removeItem("ipfsbrowser")
    }

    if ($('#userKey').val().trim().length > 0) {
        localStorage.setItem("userKey", $('#userKey').val());
    }else{
        localStorage.removeItem("userKey")
    }
}

// fill in the settings field if a valu is stored locally, else let placeholder display
function initSettings() {
    if (localStorage.getItem("restapi")) {
        $('#restapi').val(restapi);
    }
    if (localStorage.getItem("ipfsbrowser")) {
        $('#ipfsbrowser').val(ipfsbrowser);
    }
    if (localStorage.getItem("ipfsservice")) {
        $('#ipfsservice').val(ipfsservice);
    }
    if (localStorage.getItem("userKey")) {
        $('#userKey').val(userKey);
    }

    $('#summaryRestAPI').attr('href', restapi+"/chain");


}