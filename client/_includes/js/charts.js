function initChart() {
    lineChartData = {
        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
        datasets: [{
            label: 'Median Time (s)',
            borderColor:  "red",
            backgroundColor:  "red",
            fill: false,
            data: [  ],
            yAxisID: 'y-axis-1',
        }, {
            label: 'PoW Difficulty  ',
            borderColor: "blue",
            backgroundColor: "blue",
            fill: false,
            data: [ ],
            yAxisID: 'y-axis-2'
        } ]
    };

    var ctx = document.getElementById('canvas').getContext('2d');
    window.myLine = Chart.Line(ctx, {
        data: lineChartData,
        options: {
            responsive: true,
            hoverMode: 'index',
            stacked: false,
            title: {
                display: true,
                text: 'PoW common difficulty and Median time'
            },
            scales: {
                yAxes: [{
                    type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                    display: true,
                    position: 'left',
                    id: 'y-axis-1',
                }, {
                    type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                    display: true,
                    position: 'right',
                    id: 'y-axis-2',

                    // grid line settings
                    gridLines: {
                        drawOnChartArea: false, // only want the grid lines for one axis to show up
                    },
                }],
            }
        }
    });


}