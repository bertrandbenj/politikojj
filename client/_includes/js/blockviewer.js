function loadBlockIntoTreeView() {
    $.get(restapi + '/block/' + $('#blockid').val(), function (result) {
        console.log("loaded block ", result);
        $('#treeView').empty();
        ultop = document.getElementById('treeView');
        bli = document.createElement('li');

        span = document.createElement('span');
        span.innerHTML = "Block ";
        span.setAttribute("class", "caret");
        bli.appendChild(span)
        bli.appendChild(recurseTreeObject(result))
        ultop.appendChild(bli)
        redrawCaret()

    }, "json")
}

function recurseTreeObject(obj) {
    console.log("recurseTreeObject ", obj);
    var ul = document.createElement('ul');
    ul.setAttribute("class", "nested")

    for (var key in obj) {
        var li = document.createElement('li');
        var val = obj[key];
        console.log(key, val, " is typo objcect?  ", (typeof val === 'object'));

        if (typeof val === 'object') {
            var span = document.createElement('span');
            span.innerHTML = key;
            span.setAttribute("class", "caret")
            li.appendChild(span)
            li.appendChild(recurseTreeObject(val))

        } else {
            li = document.createElement('li');
            li.innerHTML += key + ': ' + val;
        }
        ul.appendChild(li);
    }

    return ul
}

function redrawCaret() {

    var toggler = document.getElementsByClassName("caret");
    var i;

    for (i = 0; i < toggler.length; i++) {
        toggler[i].addEventListener("click", function () {
            this.parentElement.querySelector(".nested").classList.toggle("activeTree");
            this.classList.toggle("caret-down");
        });
    }

    console.log("redrawCaret", toggler)
}