var ongoingAmendment = {Name: "", Predicates: []};

var ongoingClaims = [];
var cratos;
var pools;

var ipfsS;



function initCratos() {
    ipfsS = window.IpfsHttpClient({
        host: 'localhost',
        port: 5001,
        protocol: 'http',
        timeout:10000})

    testIPFS().then(r => console.log(r))

    $.get(restapi + '/pools', function (result) {
        pools = result
        console.log("loaded pools", pools)
    })

    $.get(restapi + '/claim/role', function (result) {
        ongoingClaims = result
        console.log("loaded ongoingClaims", ongoingClaims)
    })

    $.get(restapi + '/head/cratos', function (result) {
        cratos = result
        console.log("loaded cratos", cratos)

        var e = $('#ruleType')
        e.empty()
        for (var r in cratos.Data.ProposePredicate) {
            e.append(`<option>${cratos.Data.ProposePredicate[r]}</option>`)
        }

        var e2 = $('#role')
        e2.empty()
        for (var r in cratos.Data.Roles.Permission) {
            e2.append(`<option>${r}</option>`)
        }
        setPermissionsFor()
    })

}

function setPermissionsFor() {
    var perms = cratos.Data.Roles.Permission[$('#role').val()]
    var e = $('#permission')
    e.empty()
    for (var p in perms) {
        e.append(`<option>${perms[p]}</option>`)
    }
    setVoteValue()
}

function setVoteValue() {
    var perm = $('#permission').val()
    console.log("setVoteValue to " + perm)
    var e = $('#cratosMultiValue')
    e.empty()
    switch (perm) {
        case 'GrantCitizenship':
            e.append(" <select id='GrantCitizenship'></select> ")
            var vcv = $('#GrantCitizenship')
            for (var x in ongoingClaims) {
                vcv.append(`<option value='${x}'>${ongoingClaims[x].PublicKey}</option>`)
            }
            break;
        case 'VoteGrantClaim':
            e.append(` <select id='VoteGrantClaim'></select> `)
            var grv = $('#VoteGrantClaim')
            for (var x in ongoingClaims) {
                grv.append(` <option value= '${x}' >${ongoingClaims[x].PublicKey}</option>`)
            }
            break;
        case 'VoteVariable':
            e.append(`<input id= 'VoteVariableName'  placeholder='Variable Name' type='text'/> `)
            e.append(`<input id='VoteVariableValue' placeholder='Variable Value' type='text'/> `)
            break;
        case "VoteAmendment":
            e.empty()
            e.append(`<select id='VoteAmendmentValue'></select>`)
            var sel = $('#VoteAmendmentValue')
            for (var a in pools.RecentAmendment) {
                sel.append(`<option value='${a}'>${pools.RecentAmendment[a].Name}</option>`)
            }
        default:
            console.log("not handled")
    }
}

function generalVote() {
    var perm = $('#permission').val()
    switch (perm) {
        case 'GrantCitizenship':
            approveRole()
            break;
        case 'VoteGrantClaim':
            voteRole()
            break;
        case 'VoteVariable':
            voteVariable();
            break;
        case 'VoteAmendment':
            voteAmendment();
            break;
        default:
            console.log("not handled")
    }
}

function now() {
    return Math.floor((new Date()).getTime() / 1000)
}

function approveRole() {

    var val = $('#GrantCitizenship').val()

    var approvedClaim = {
        Claim: ongoingClaims[val],
        Timestamp: now(),
        PublicKey: $('#pubkeycidinput').val()
    }
    console.log("approve", val, ongoingClaims[val])

    var signstr = approvedClaim.Timestamp
        + approvedClaim.Claim.Timestamp
        + approvedClaim.Claim.Role
        + approvedClaim.Claim.PublicKey
        + approvedClaim.Claim.Signature
        + approvedClaim.PublicKey


    readArmorPk().then(pk => signIt(pk, signstr).then(sign =>
        postDoc('/claim/approve', approvedClaim, sign, function (response) {
            console.debug(" got claim/role result  ", response);
        })));

}

function voteAmendment() {
    var n = $('#VoteAmendmentValue').val()
    //var v = $('#VoteVariableValue').val()
    var perm = $('#permission').val()
    var ballot = {
        Permission: perm,
        VoteObject: n,
        VoteValue: "OK",
        Timestamp: now(),
        PublicKey: $('#pubkeycidinput').val()
    }

    var sign = ballot.Permission + ballot.VoteObject + ballot.VoteValue + ballot.Timestamp + ballot.PublicKey

    readArmorPk().then(pk => signIt(pk, sign).then(sign =>
        postDoc("/vote", ballot, sign, function (result) {
            console.debug(" got amendment result  ", result);
    })));

}

function voteVariable() {
    var n = $('#VoteVariableName').val()
    var v = $('#VoteVariableValue').val()
    var perm = $('#permission').val()
    var ballot = {
        Permission: perm,
        VoteObject: n,
        VoteValue: v,
        Timestamp: now(),
        PublicKey: $('#pubkeycidinput').val()
    }

    var sign = ballot.Permission + ballot.VoteObject + ballot.VoteValue + ballot.Timestamp + ballot.PublicKey

    readArmorPk().then(pk => signIt(pk, sign).then(sign =>
        postDoc("/vote", ballot, sign, function (response) {
            console.debug(" got claim/role result  ", response);
    })));

}

function voteRole() {
    var perm = $('#permission').val()

    var ballot = {
        Permission: perm,
        VoteObject: $('#VoteGrantClaim').val(),
        VoteValue: perm === "VoteGrantClaim" ? "Grant" : "Revoke",
        Timestamp: now(),
        PublicKey: $('#pubkeycidinput').val()
    }
    var sign = ballot.VoteObject + ballot.VoteValue + ballot.Timestamp + ballot.PublicKey


    readArmorPk().then(pk => signIt(pk, sign).then(sign =>
        postDoc('/vote', ballot, sign, function (response) {
            console.debug(" got claim/role result  ", response);
    })));
}


function addRule() {
    var sub = $('#amendmentSubjectMV').val()
    var obj = $('#amendmentObjectMV').val()
    var pred = $('#ruleType').val()

    ongoingAmendment.Predicates.push({
        Subject: sub,
        Predicate: pred,
        Object: obj
    })

    $('#amendmentRuleSet').append("<p>" + sub + " => " + pred + " =>" + obj + "</p>")

}


function clearAmendment() {
    ongoingAmendment.Predicates = []
    $('#amendmentRuleSet').html("")
}

function sendAmendment() {
    ongoingAmendment.Name = $('#amendmentName').val()
    ongoingAmendment.Timestamp = now();
    ongoingAmendment.PublicKey = $('#pubkeycidinput').val();

    var sign = ongoingAmendment.Timestamp +
        ongoingAmendment.PublicKey +
        ongoingAmendment.Name

    readArmorPk().then(pk => signIt(pk, sign).then(sign =>
        postDoc("/amend", ongoingAmendment, sign, function (result) {
                console.log(result)
        })));

}

