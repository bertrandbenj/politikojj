---
layout: default
title: In the Blockchain world
nav_order: 2
parent: Specification
---

This document intends to describe politikojj specificities as a blockchain.

# Proof of Authority

# Proof of Identity

# Adaptive Proof of Work


# Head chain
Politikojj blocks are head pointer of the latest state and latest diff. 

Unlike most blockchain out there which build progressively the global state and sometime the entire chain to be known in order to query the current state. 
Politikojj is like git, the head point to the latest state and diff. This allows backward verification of the chain while only the latest block is required for synchronization. 
This is intended to make a clean maintainable code, minimize the services required to be implemented by the nodes and simplify front end developers to access easily blockchain data. 