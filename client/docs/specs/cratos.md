---
layout: default
title: Cratos
nav_order: 1
parent: Specification
---

This document intends to describe the mechanism through which the law is operating on politikojj 

# Technical description
## Use case
![use cases](usecase.png)

## Amendments 
An amendment is a collection of predicate of the form 

subject => predicate => object

the nature of the subjet and object depends on the predicate type 

### example
setup a new tax for a collective social security require the following set of predicate 

```
socialSecurityAccount => ProposeCreateAccount => CurrencyName
socialSecurityAccount => ProposeAccountPermission => SpendSocialSecurityAccount
``` 
Create a public account, ie. associated to a permission not to a public key
create a permission for the account 

```
. => ProposeTax => socialSecurityTax
socialSecurityTaxRate => ProposeNewVariable => UnitIntervalValue
socialSecurityTax => ProposeTaxRevenueTarget => socialSecurityAccount
socialSecurityTax => ProposeTaxType => GlobalTransactionTax
socialSecurityTax => ProposeTaxVar => socialSecurityTaxRate
``` 
- UnitIntervalValue declare a variable to be a ratio on interval ]0,1[ 
- Set the account to which tax revenues flows
- Define how to apply the tax, on every transaction 
 

```
. => ProposeNewRole => TreasurerSocialS 
```
create a role that inherit from non other (.)

```
TreasurerSocialS => ProposeAddPermission => SpendSocialSecurityAccount
``` 
Grant the treasurer role the newly defined permission

 
