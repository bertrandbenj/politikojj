---
layout: default
title: Concepts
nav_order: 1
parent: Choices
---


# A blockchain for commons 
All society have to balance between two occasionally antagonistic concepts : 
- Individual freedom / collective cohesion. 
As collective impact grows, individual spectrum of choices reduces and therefore the feeling of individual freedom.
On the other hand as individual freedom grows, common goods decreases and individuals have to anticipate more of their needs (infrastructure, health care, education,...)
One could argue that each extreme solution end up being a negative feedback loop:
- Full collectivisation decreases individual motivation, group productivity, overall satisfaction.
- Extreme individualism forces people to pay and consider every aspect of their life, that turns into less free time to spend on matter that interest them. Absolute individual economic freedom becomes no freedom at all. 

Now, there is no objective middle ground, nor optimal solution to this question, as its mainly a matter of opinion and conjuncture. 
One historical observation can be made though. Usually as resources lack, power centralizes, group cohesion and coercion rises while more liberal policies tend to follow natural resource's abundance. 

The objective here is not to determine a [pseudo scientific](docs/ml.md#axioms) formula to solve it all but to propose a system with enough flexibility to adapt a range of opinion and needs. 
Propose a stable basis for collective decision-making .

# Suffrage modality 

## Open ballot 
Most suffrage's ballots are anonymous, empirically represented as a closed envelop filled in a private room. 
It is argued that anonymity protects from a potential coercion from the candidates. meaning most of these arguments imply that a vote is an election. nevertheless, anonymity is a consideration to account for.
Anonymity makes it impossible to be sure the ballot box isn't rigged. (changed ballot, additional ones...)

In order to enable democratic practices and "massify voting", we turn toward Electronic voting. however, e-voting require trust over the machinery and the network and the best trust that is Verifiability 
Sadly, Anonymity and Verifiability are antagonist concepts over the principle of Link-ability. Anonymity is the un-link-ability of the ballot and the voter, while verifiability is the link-ability of the two. 
The problem may be partly solved using third party but never entirely. By adding complexity to the system we can imagine Anonymous and partly verifiable or Verifiable and partly anonymous but that would necessarily involve a third party that need to be trusted...

Any e-democracy has to choose which to favor. 
### choice
In order to be consistent with the blockchain choice: a public ledger. we choose Open Ballot, Verifiability over Anonymity. 
- Arguing that Coercion isn't really a question in a direct democracy when you vote for things over people.. 
- Arguing that Verifiability is the strongest argument against voting fraud. 
- Arguing that Transparency induces accountability and honesty 
- Arguing that a ***Public*** decision in a democracy, results from personal opinions (but not private since they affect public)

## Punctual voting 
Most suffrage nowadays are punctual. that means, if we define freedom as a symmetry of rights. current voting system don't respect time symmetry: Who knows if the next generation will agree with decisions made today. why would votes be punctual. If people today decide of a proposition X, perhaps tomorrow, they will change their minds, so they should be able to. 
Again, when seeing democracy through the prism of elections (recurrent mandate for a time period) we are only seeing a subset of suffrage modalities. but as we look at the general question of public decision-making, we see how bias it is. 
Punctual voting like election or petitions induce a rhetoric that is based on news, arguments targeting individual's psychology rather than the group.
Punctual voting frames the choices and coerce individuals into a subset of possibilities (whether it's the time you want to go voting or the choices you get) Individual ability to consider his/her preferred opnions is very limited

### numerical values 
Considering a Demos $$ D $$, a set of individuals $$ i $$, each having a vote $$ V(i) $$ on a numerical value, the common value is a simple average  

$$ mean = \frac{\displaystyle\sum_{i \in D} V(i)}{\vert D \vert} $$

Instead, we propose a system that is neither. People can vote whenever they feel like it, remove their vote whenever
Entering the system means accepting its status quo: equivalent to a vote on every vote-able matters in its present state.
eg: 
- if a public tax A is 10% in a group 10 members. 
- 5 of them voted 5 % and 5 of them voted 15% for an average of (5*0.05 + 5*0.15) / 10
- A new member enters the system, implies a vote of that value 10%. (5*0.05 + 5*0.15 + 1*0.10) / 11
- He can change his vote a minute later to lets say 20%. (5*0.05 + 5*0.15 + 1*0.20) / 11  = 0.109090909
- resulting in an immediate change of the tax value weighted by the group size. 

This form of voting over numerical value is easily applicable and respect a spatial and temporal symmetry for all group participants
#### permission, elections 
Some votes dont result into immediate changes. eg : 
- same group of 10 is undecided about who should be able to spend Tax A incomes 
- 5 members vote for Alice, 5 for Bob, neither have a majority of more than 10/2 + 1
- when the new member joins, no vote is implied
- a moment later the new member vote for Alice
- Alice is granted permission to use the tax incomes for whatever public purpose 
- Alice misuses the account 
Open Question : 
- should revocation be another majority vote or subtract to favourable votes ? 


#### Amendments
Unlike parliamentary democracies, direct democracy does not distinguish voting an executive decision (changing tax rate) from proposing and voting amendments. 
Same question as [Permission & Elections](#permission-elections)

## Delegation 

Vote delegation such as proposed by [liquid democracy](https://en.wikipedia.org/wiki/Liquid_democracy) is considered 
yet individual ballot should overrule the delegation. 
- Considering a delegate of vote "by default" 
- Someone share my opinion but is more involved on political questions 
- On some specific cases I want to  be able to choose without having to cancel the delegation 


# Monetary System 

Largely influenced by [Duniter](../ml.md) but unequivocally political in essence. The definition of freedom as a symmetry is a must-have  
However extending the concept to political rights it's not bounded to economics & money minting.  
- The currency shall be created through a Universal Dividend. 
- Its mass inflation's rate however shall be voted upon and may not respect TRM's [arguable](../ml.md#temporal-symmetry) time symmetry  
- Daily re-evaluation for simplicity 
