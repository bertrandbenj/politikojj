---
layout: default
title: Opinions
nav_order: 2
parent: Choices
---

# Opinions

## Representation

Let's suppose the opinion spectrum for a broad subject 

![governance](../../assets/images/indiv_collectiv.png)

Let's also suppose a distribution of that opinion that may or may not be gaussian 

![distrib](../../assets/images/normal.gif)

It is very likely that one dimension is not enough to describe the range of opinions 