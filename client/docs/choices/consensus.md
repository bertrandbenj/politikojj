---
layout: default
title: Consensus
nav_order: 1
parent: Choices
---

# Consensus

Consensus is a topic that has been thought off across various fields throughout history.

Military, Socially, Political, Philosophical and since the emergence of the information technology area it has been, is and will be formalized, studied and researched. 
Consequently, the topic has been enriched in the past decades with mathematical models proofs and theories. 
It is an essential part to understand how multi agent, multi actor system can coordinate and agree. 
Since the technical literature is a lot more accurate and practical, lets first take a look at the important theories, see how they apply to our blockchain and finally see how we can use it to build a democratic system. 

## Safety and Liveness
As defined by leslie lamport a distributed system can/should fulfill 2 goals, safety and liveness

 - Safety means "(something bad) will not happen"   
 - Liveness means "(a goal) must happen" 

More formally, to satisfy the correctness of these goals we must observe 3 properties :

 - Safety 
     - Validity - Any value agreed on must be proposed by one of the actor 
     - Agreement - All non faulty actors must agree on the same value
 - Liveness
    - Termination - All non faulty actors eventually decide. non-blocking 

## CAP (ou PACAP?)

The CAP theorem states that there is a trilemma between these properties, consistency, availability and partition tolerence.
It has consequences all across distributed systems. from networking, data storage and computation. 

![cap](../../assets/images/cap.jpg)

That means the central point does not exists, this is an **Impossibility theorem** and so a more accurate representation of the problem would be: 

![cap2](../../assets/images/correctCAP.png)
The model is Theoretical. It is important to note that real world systems are complex so its good to see these properties as spectrum not binary properties.
Also, the model only describe the problem using 3 minimal properties, but we could imagine adding complexity to it like anonymity


 - One simple way to understand it: imagine a system where you store and update a single sentence across multiple machines. 

Computer A : Hello
Computer B : Alice

You have a partitioned system! now if we update part of it 

Computer A : Hola 

We intuitively see that machines have to agree somehow when the update is effective. which may quickly become an issue and causes deadlock if we simultaneously update multiple part of the sentence.
Therefore we need to either prioritize what is more important: update time (availability) or the effective value at an instant t (consistency). 

 - we could also decide that consistency is a must have (banking, airplanes, rockets ...)
We would then have to trade off on either availability or partition tolerence. 

On an airplane you would probably have a full copy, redundancy (no partitioning) and guarantee availability since you want the autopilot to respond before the crash 
However for a bank transfer you can wait a few seconds. The bank may prioritize overall system efficiency by partitioning its data. Either way you dont really have a choice since banks need to communicate with one another and are entirely different systems 


## Byzantine fault tolerance 
![byzantine](../../assets/images/byzantine.png)

### super-majority 
the super majority is a concept to keep an eye on when attempting to update blockchain protocol  

## Paxos 
[paper](https://lamport.azurewebsites.net/pubs/lamport-paxos.pdf)

Probably the most famous political system analogy applied to distributed system, paxos is a greek island where (hypothetically) exists an efficient parliamentary system. 
Paxos is an analogy to describe a consensus protocol based on a subset of the population to vote: a parliament made of priests. It has 3 distinguishable actors: 
 - Proposer
 - Acceptor
 - Learner 

### Part time parliament
paxos introduced the concept of part-time parliament, nowadays famous under the name of ledgers
paxos, require agreeable participant meaning a network composed of non byzantine nodes. consequently, it is not applicable to blockchain but is used by large companies like google to synchronize datacenter across continent. 
It is a keystone of the distributed system literature mostly used in big data system. Its importance comes from the fact paxos is kind of unchallenged lower bound for efficiency. 


## Leader based systems

Paxos was followed by a wide variety of leader based ledger system that are not design either to operate on public untrusted network but good or big data. 
As functionalities grows there is a clear cut in literature in favor of leader based system that uses consensus only to elect a leader node and then grant it all power. 
The reasons are simplicity and efficiency.

### The wise tyrant
Wise tyrant seems to be more efficient the more feature we expect of the distributed system. Human analogy to the question of decentralization is that the expertise on every domain and summarizing all information for one individual is limited to the cognitive ability of the leader. 
there is indeed limitation for a leader based system. as the dataset grows, organizing thousands, ten of thousands or even hundreds of thousands of node quickly raises memory limitation questions. 
So there is case to be made regarding a leader based approach, it seems more effective for a few features but is it for a lot of features?

## Blockchain


### Nakamoto consensus 
A nakamoto consensus. is a way to elect a temporary leader on an untrusted public network. The leader can submit a block and a new election starts. Votes are implicit by recording or not the data. instead of having to define a voting mechanism, nakamoto consensus need to define a fork resolution mechanism
A proof of work is a nakamoto consensus based on ressources allocation. 

One analogy we could make is that the difference between nakomoto consensus and previous literature  is a similar difference that exists between agreeing by voting or by participating to a competition. 

Nakamoto consensus is like the olympic games. we all agree on selecting a winner. we mostly need a way to settle equal participant
Previous consensus are political analogies, democracies, parliaments, tyrant systems... we mostly need to count votes

 
### applications
 - What is a blockchain in all that? 
 
A blockchain is a distributed datastore that operate on an untrusted network of machine and that prioritize 
consistency and availability. it does so at the cost of partition tolerence and scalability. 
There is no partitioning, the data is ""centralized"" into each block that represent the latest global state. 

 - What does mean "decentralized" ?
 
Now that you have read the basic principle of distributed systems, you understand that decentralized doesn't mean anything ! 
It may refer to the lack of global authority on a blockchain: there is no admin. 

