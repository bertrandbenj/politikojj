---
layout: default
title: Ğ1 currency 
nav_order: 4
parent: Critics
---

# Libre money yes, however ... 
On my blockchain education path I stumbled upon the Ğ1, unlike most crypto currency this one wasn't design as an asset to capitalise but as a human based trading currency. 
Its monetary mass isn't finite like most token, its ont mined either. It's a [UD](#universal-dividend) minted for each a proof of identity 

# Axioms 
Applying scientific methodology to problem solving seems like a good intention however I'm afraid it confused many people into thinking a political question like a currency is a hard science problem.  
The number of the G1 members unable to differentiate arithmetic axioms from axioms written in a natural language is stunning.
The so called axioms concerning a political matter is a soft science, defining concept as vague and [ambiguous](https://en.wikipedia.org/wiki/Ambiguity) as freedom is way out of the realm of hard science, no matter how much arithmetic is used. why even call them axioms if they have no arithmetic translation and therefore are not building block of the theory. they are interesting considerations in the thought process and most certainly concepts cherished by the author but axioms ... really though? 
Was  this intentional from the author of the [TRM](http://en.trm.creationmonetaire.info/). Perhaps he believes it ... either way this turned some members into hardcore believers that it is some sort of ultimate solution to all economic issues. "You're asleep", "you need awaken", "its scientifically proven, you fool", clearly a pseudo scientific grounding has its flaws 
I sincerely hope not to make the same mistake and emphasize enough on the fact that my proposition is a political construct to help find consensus on political questions and never pretend to solve a hard science problem based on un-mentioned underlying market ideology. don't take me wrong here, I don't deny the importance of market I just deny it being a hard science. 


# Universal Dividend
Universal dividend is a great denomination as it merges the idea of collective property and capital dividend. 
Defining a currency as such restore sovereignty over the matter and explain why money minting is a right of all. 
Now the term Universal does not refer to all mind kind but to the specific political construct that define the group. 
A decentralized identity network called [Web Of Trust](#web-of-trust)

# Web Of Trust
The second great innovation brought by the G1 is the group construct. totally decentralized with a complete absence of authority.
It does demonstrate perfectly the possibility and thus experimentally disproves arguments against flat and symmetrical group building. 
The possible absence of authority is being proven, the question remains: is it a good thing in every situation? 
Duniter's WOT as one purpose to determine who is a member, and that simple question is solved quite expensively computationally wise. 
This can become an issue when scaling but more importantly when adding features. 
Indeed, if [Freedom of democratic modification](http://en.trm.creationmonetaire.info/formalisaton.html) isn't a pious wish but one not implemented yet. question 
Then we must analyze the group construct underneath the currency and its entire political geometry. 

# Democracy really 
Its entirely apolitical, no choices to be made and no modification to suggest... of course there is a forum where you are invited to do suggestions, but who reads it ? how are things decided? 
My experience on the matter is I think enlightening about the "democratic" selling point. I must have done 5 to 10 suggestions from bug notification to actual feature suggestion...  
even those where all qualified people agreed was objective technical improvement, some self declared member arbitrarily told me to format my suggestion into a format called RFC so as to archive it.
meanwhile the forum itself administrated by arbitrary chosen admins decide archive conversations of their chosing at a pace of their choosing.
Once a disagreement get tense, immediately entire conversation get archived, occasionally people get banned, using argument that a private forum result in private ruling.  
so where exactly is the Democracy? 
Similarly, while constantly mentioning freedom and democratic empowerment, having a political disagreement on a matter as simple as should tax mechanism be considered, I lost my right to speak at a meeting or to be exact the recording of my talk. because an argument that doesn't suit someone holding a bit of power gets you sanctioned. 
In fact the capacity for democratic modification is neither thought of, nor wished for as most of the core people are anti politic ideologues. they apparently believe that money creation is the sole rule that should be defined fairly to define democracy . 
Which at this point should freak out anybody most users that are genuinely "I want a better world for everyone" people. Little do they understand that together with decentralizing power the G1 guarantees anonymity and forbids any form of tax. SO if you are a G1 user you better believe in charity because social justice isn't an options.
No matter what their selling points are, I now know for granted that tax isn't an option on the G1 since the core people are harcore individualists. I dont quite understand the consistency of this view so I probably shouldn't. But I hardly see how a world made of private everything, no public anything is any better. but trust me on that there will never be taxes in the G1 and consequently no benefits either. but their can be black market that is okay, that is guaranteed 

Point here is: Is democracy more important to you than liberalism, G1 is an individualist liberal turning point, if you think that it's going to be something else you are fooled, stupid. There is simply no way you are going to change the mind of somebody who has consciously chosen economic liberalism over public education. ask the developers to make a G1 tax for education they will refuse in the name of ... freedom, believe it or  not, denying others public instruction is an act of freedom for hardcore liberals. education !!! the very mean to emancipation. 
Is it really outweighting individual's absolute economic freedom. really?
should this message outlive me, if you ever wonder what is this discrepancy between the sweet dream you were sold and the reality it turned out to be, well start questioning yourself, why when the economy mostly liberal is going rogue, rise inequality, are you steering the wheel further down that same direction? But worse... how can you believe that is a solution to anything. 

Anyway my personnal opinion doesnt matter much as the point of building a democratic system is to enable people opinions not give them one. 



As beautiful proof, decentralized solution the WoT can be, I will not implement it yet for that reason of feature limitation. 
I employ mankind history, trust intuitively my ancestors to not have it all wrong (despite the improvements I'm here to suggest) and choose a socio-mimetism for building the group. 
Thus have voting and institution granting membership. voting can solve small groups memberships. as for the bigger one a role permission approach grounds computation complexity, increase process speed while somewhat breaking membership symmetry. 
Now membership symmetry is a longterm guarantee and so can we really trade it off ? Why go all the way to decentralize a system of asymmetrical permissions ?

So let's question whether we are on a [CAP](https://en.wikipedia.org/wiki/CAP_theorem) situation where out of three objectives 
- symmetry of rights     
- democratic modification   
- decentralised protocol

but we could have other prerequisite guarantees like 
- process efficiency  
- human comprehensible (as features gets added )

Simply put, the more features and guarantee we want, the more trade off we have to consider 

## Temporal Symmetry 