package node

import (
	"fmt"
	"runtime"
	"sync"

	"github.com/mikespook/gorbac"
	"github.com/op/go-logging"
	"github.com/tevino/abool"
	"gitlab.com/bertrandbenj/politikojj/chain"
	"gitlab.com/bertrandbenj/politikojj/crypto"
	"gitlab.com/bertrandbenj/politikojj/model"
	"gitlab.com/bertrandbenj/politikojj/utilities"
)

var log = logging.MustGetLogger("Key")
var craftLock = &sync.Mutex{}

type BlockDataProcessor interface {
	ApplyDelta(t int64, oldBlock model.Block, updatedCid chan string, evidences chan model.Evidence, waitG *sync.WaitGroup)
}

type BlockSmith struct {
	Key       crypto.Keypair
	powCores  int
	CratosP   CratosProcessingService
	demosP    DemosProcessingService
	currencyP CurrencyProcessorService
	notaryP   NotaryProcessorService
	powP      PowProcessorService
	Pools     *TemporaryPool
	Chain     *chain.BlockChainService
	lock      *sync.Mutex
}

func NewBlockSmith(ch *chain.BlockChainService, pair crypto.Keypair, powCores int) *BlockSmith {
	// ==== Find out how many cores are configured to be used ====
	if powCores < 1 || powCores > runtime.NumCPU() {
		powCores = runtime.NumCPU()
	}

	p := NewPool()

	BlockSmithRes := BlockSmith{
		powCores: powCores,
		lock:     &sync.Mutex{},
		Key:      pair,
		CratosP: CratosProcessingService{
			store:       ch.Store,
			chain:       *ch,
			Pools:       p,
			rbac:        gorbac.New(),
			permissions: gorbac.Permissions{},
		},
		demosP: DemosProcessingService{
			store: ch.Store,
			Pools: p,
		},
		currencyP: CurrencyProcessorService{
			store: ch.Store,
			chain: *ch,
			Pools: p,
		},
		notaryP: NotaryProcessorService{
			store: ch.Store,
			Pools: p,
		},
		powP: PowProcessorService{
			store: ch.Store,
		},
		Chain: ch,
		Pools: p,
	}
	return &BlockSmithRes

}

func (smith BlockSmith) RunBlockSmith() {
	log.Info("Running BlockSmith ...")
	for {
		newBlock, err := smith.GenerateBlock(smith.Chain.HeadBlock())
		if err != nil {
			log.Error("Generating block", err)
		}
		err = smith.CheckForkOrChain(newBlock)
		if err != nil {
			log.Error("Chaining block", err)
		}
	}
}

func (smith BlockSmith) CheckForkOrChain(block model.Block) error {
	if smith.Chain.IsBlockValid(block) {

		head := smith.Chain.HeadBlock()

		if smith.Chain.IsBlockDeltaValid(block, head) { // standard case block follows our head
			_, _, err := smith.Chain.ChainBlock(block)
			if err != nil {
				log.Error("Chaining block ", err)
				return err
			}
		} else {
			if head.Index < block.Index { // new block is ahead of current head
				_, _, err := smith.Chain.ChainBlock(block) // naively accept any block // TODO handle forks
				if err != nil {
					log.Error("Chaining block ", err)
					return err
				}
			} else {
				if head.Index == block.Index && head.PoWHash == block.PoWHash {
					return PolitikojjError(fmt.Sprintf("block %d already known ", block.Index))
				} else {
					return PolitikojjError(fmt.Sprintf("block index too old %d compared to local current %d", block.Index, head.Index))
				}
			}
		}
	} else {
		return PolitikojjError("local integrity of the block")
	}
	return nil
}

func (smith BlockSmith) GenerateBlock(oldBlock model.Block) (model.Block, error) {
	smith.lock.Lock()
	var wg sync.WaitGroup
	wg.Add(5)

	// ======= Build the block =======
	updatedDemosChan := make(chan string, 1)
	updatedCratosChan := make(chan string, 1)
	updatedCurrencyChan := make(chan string, 1)
	updatedNotaryChan := make(chan string, 1)
	updatedDifficultyChan := make(chan string, 1)
	evidences := make(chan model.Evidence, 100)
	t := utilities.Now()

	go smith.CratosP.ApplyDelta(t, oldBlock, updatedCratosChan, evidences, &wg)
	go smith.demosP.ApplyDelta(t, oldBlock, updatedDemosChan, evidences, &wg)
	go smith.currencyP.ApplyDelta(t, oldBlock, updatedCurrencyChan, evidences, &wg)
	go smith.notaryP.ApplyDelta(t, oldBlock, updatedNotaryChan, evidences, &wg)
	go smith.powP.ApplyDelta(t, oldBlock, updatedDifficultyChan, evidences, &wg)

	newBlock := model.Block{
		Index:     oldBlock.Index + 1,
		Previous:  model.CidLink{Link: smith.Chain.GetHead().Cid},
		Network:   oldBlock.Network,
		Demos:     model.CidLink{Link: <-updatedDemosChan},
		Cratos:    model.CidLink{Link: <-updatedCratosChan},
		Currency:  model.CidLink{Link: <-updatedCurrencyChan},
		Notary:    model.CidLink{Link: <-updatedNotaryChan},
		Evidences: []model.Evidence{},
		SignedStampedDoc: model.SignedStampedDoc{
			Timestamp: t,
			PublicKey: smith.Key.PubID()},
		Difficulty: model.CidLink{Link: <-updatedDifficultyChan},
		Nonce:      "",
		PoWHash:    "",
	}

	wg.Wait()
	close(evidences)

	log.Warningf("block %d crafted, adding %d evidence ... ", newBlock.Index, len(evidences))

	for i := range evidences {
		newBlock.Evidences = append(newBlock.Evidences, i)
		smith.Pools.Remove(i.CidLink.Link)
	}

	chain.StampAndSign(&newBlock, smith.Key)
	//smith.Chain.SignBlock(&newBlock, smith.Key.PrivateKey)

	newBlock = smith.CalculatePow(newBlock)

	smith.lock.Unlock()
	return newBlock, nil
}

func (smith BlockSmith) CalculatePow(block model.Block) model.Block {
	resChan := make(chan model.Block, 1)

	var difficulty model.PersonalizedAdaptivePoW
	smith.currencyP.store.GetObject(block.Difficulty.Link, &difficulty)

	// ==== Run inside goroutines until a result is found ====
	var interrupt = abool.New()
	for cpu := 1; cpu <= smith.powCores; cpu++ {
		go func(b model.Block, cpu int, c chan model.Block) {
			for i := 0; interrupt.IsNotSet(); i++ {
				b.Nonce = fmt.Sprintf("0x%.2x%.8x", cpu, i)
				chain.StampAndSign(&b, smith.Key)

				b.HashPow()
				if b.IsHashValid(difficulty) {
					//log.Info("found PoW", cpu, i, b.Nonce, b.PoWHash)
					interrupt.Set()
					c <- b
				}
			}
		}(block, cpu, resChan)
	}
	return <-resChan
}
