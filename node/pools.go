package node

import (
	"reflect"
	"sync"

	"gitlab.com/bertrandbenj/politikojj/model"
)

// ==================== Pools ====================
type TemporaryPool struct {
	Claims         map[string]model.Claim
	ApprovedClaims map[string]model.ApprovedClaim
	Votes          map[string]model.OpenBallot
	Transactions   map[string]model.Transaction
	Amendments     map[string]model.Amendment
	sync.RWMutex
	updateChannel chan model.Wrapper
}

func NewPool() *TemporaryPool {
	return &TemporaryPool{
		Claims:         make(map[string]model.Claim),
		ApprovedClaims: make(map[string]model.ApprovedClaim),
		Votes:          make(map[string]model.OpenBallot),
		Transactions:   make(map[string]model.Transaction),
		Amendments:     make(map[string]model.Amendment),
	}
}

func (pool *TemporaryPool) GetVotes() map[string]model.OpenBallot {
	pool.RLock()
	defer pool.RUnlock()
	return pool.Votes
}

func (pool *TemporaryPool) GetTxs() map[string]model.Transaction {
	pool.RLock()
	defer pool.RUnlock()
	return pool.Transactions
}

func (pool *TemporaryPool) CountVotes() map[string]map[string]uint64 {
	pool.RLock()
	defer pool.RUnlock()
	counts := make(map[string]map[string]uint64)
	for _, v := range pool.Votes {
		if v.Permission != model.VoteVariableValue {
			if counts[v.Permission] == nil {
				counts[v.Permission] = make(map[string]uint64)
			}
			counts[v.Permission][v.VoteObject]++
		}
	}

	return counts
}

func (pool *TemporaryPool) Remove(cid string) {
	pool.Lock()
	defer pool.Unlock()
	delete(pool.Claims, cid)
	delete(pool.ApprovedClaims, cid)
	delete(pool.Votes, cid)
	delete(pool.Amendments, cid)
	delete(pool.Transactions, cid)

}

func (pool *TemporaryPool) Exists(cid string) bool {
	pool.RLock()
	defer pool.RUnlock()

	if _, ok := pool.Claims[cid]; ok {
		return true
	}

	if _, ok := pool.Votes[cid]; ok {
		return true
	}

	if _, ok := pool.Transactions[cid]; ok {
		return true
	}

	if _, ok := pool.ApprovedClaims[cid]; ok {
		return true
	}

	if _, ok := pool.Amendments[cid]; ok {
		return true
	}

	return false
}

func (pool *TemporaryPool) Add(cid string, data interface{}) {
	if pool.Exists(cid) {
		log.Warningf("Document is already pooled %s", cid)
		return
	}

	pool.Lock()
	defer pool.Unlock()

	strType := ""

	switch doc := data.(type) {
	case model.Claim:
		pool.Claims[cid] = doc
		strType = "Claim"
	case model.ApprovedClaim:
		pool.ApprovedClaims[cid] = doc
		strType = "ApprovedClaim"
	case model.OpenBallot:
		pool.Votes[cid] = doc
		strType = "OpenBallot"
	case model.Amendment:
		pool.Amendments[cid] = doc
		strType = "Amendment"
	case model.Transaction:
		pool.Transactions[cid] = doc
		strType = "Transaction"
	default:
		log.Error("unhandled data type", reflect.TypeOf(data))
		return
	}

	if pool.updateChannel != nil {
		wrap := model.Wrapper{
			CID:  cid,
			Type: strType,
			Data: data,
		}
		//log.Info("pushing Doc for broadcast ", wrap)
		pool.updateChannel <- wrap
	}
}

func (pool *TemporaryPool) Attach(docsChan chan model.Wrapper) {
	pool.updateChannel = docsChan
}
