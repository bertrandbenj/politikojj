package node

import (
	"sync"

	"gitlab.com/bertrandbenj/politikojj/model"
	"gitlab.com/bertrandbenj/politikojj/storage"
	"gitlab.com/bertrandbenj/politikojj/utilities"
)

type DemosProcessingService struct {
	store storage.Store
	Pools *TemporaryPool
}

func (proc DemosProcessingService) ApplyDelta(t int64, oldBlock model.Block, updatedCid chan string, evidences chan model.Evidence, waitG *sync.WaitGroup) {
	defer waitG.Done()
	// === load from CID
	var oldDemos model.Demos
	if proc.store.GetObject(oldBlock.Demos.Link, &oldDemos) {
		// === Apply delta
		newDemos := oldDemos

		proc.FindNewCitizens(&newDemos, evidences)

		// === Persist new one
		newCid, err := proc.store.PutObject(newDemos)
		if err != nil {
			log.Error("Demos dag put error ", newCid)
		}
		updatedCid <- newCid
	} else {
		log.Warning("Issue previous demos ", oldDemos)
	}

}

func (proc DemosProcessingService) FindNewCitizens(demos *model.Demos, evidences chan model.Evidence) {
	majority := demos.Population/2 + 1

	ballotCount := proc.Pools.CountVotes()

	var voteObjectToRemove []string

	for k, v := range ballotCount[model.VoteGrantClaim] {
		if v >= majority {
			var claim model.Claim
			if proc.store.GetObject(k, &claim) {

				demos.CitizensByName[claim.ChosenName] = model.CitizenOfName{
					PublicKey:        []string{claim.PublicKey},
					CitizenshipClaim: model.CidLink{Link: k},
				}
				citizenList := demos.PublicKeysByRole["Citizen"]
				citizenList = append(citizenList, claim.PublicKey)
				demos.PublicKeysByRole[model.Citizen] = citizenList
				demos.Population++
				voteObjectToRemove = append(voteObjectToRemove, k)
			}
		}

	}
	proc.Pools.RLock()
	defer proc.Pools.RUnlock()
	for k, v := range proc.Pools.GetVotes() {
		if utilities.Contains(voteObjectToRemove, v.VoteObject) {
			evidences <- model.Evidence{
				CidLink: model.CidLink{Link: k},
				Object:  v}
		}
	}

}
