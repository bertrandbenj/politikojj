package node

import (
	"sync"

	"gitlab.com/bertrandbenj/politikojj/model"
	"gitlab.com/bertrandbenj/politikojj/storage"
)

type PowProcessorService struct {
	store storage.Store
}

func (proc PowProcessorService) ApplyDelta(t int64, oldBlock model.Block, updatedCid chan string, evidences chan model.Evidence, waitG *sync.WaitGroup) {
	defer waitG.Done()

	oldCid := oldBlock.Difficulty.Link
	var lastPow model.PersonalizedAdaptivePoW

	if !proc.store.GetObject(oldCid, &lastPow) {
		log.Error("couldn't load PersonalizedAdaptivePoW")
		return
	}

	newBasePow, newPrevious, newIssuerFrame := lastPow.GetGlobalDifficulty(oldBlock)

	newPAPOW := model.PersonalizedAdaptivePoW{
		AvgGenTime:         lastPow.AvgGenTime,
		CommonDifficulty:   newBasePow,
		PreviousBlockTimes: newPrevious,
		PersonalHandicap:   nil,
		IssuersFrame:       newIssuerFrame,
	}

	// === Persist new one
	newCid, err := proc.store.PutObject(newPAPOW)
	if err != nil {
		log.Error("Currency dag put error ", oldCid, err)
	}

	updatedCid <- newCid
}
