package node

type PolitikojjError string

func (e PolitikojjError) Error() string {
	return "Politikojj error: " + string(e)
}
