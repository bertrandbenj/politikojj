package node

import (
	"sync"

	"gitlab.com/bertrandbenj/politikojj/model"
	"gitlab.com/bertrandbenj/politikojj/storage"
)

type NotaryProcessorService struct {
	store storage.Store

	Pools *TemporaryPool
}

func (proc NotaryProcessorService) ApplyDelta(t int64, oldBlock model.Block, updatedCid chan string, evidences chan model.Evidence, waitG *sync.WaitGroup) {
	defer waitG.Done()

	updatedCid <- oldBlock.Notary.Link
}
