package node

import (
	"strconv"
	"sync"

	"github.com/mikespook/gorbac"
	"gitlab.com/bertrandbenj/politikojj/chain"
	"gitlab.com/bertrandbenj/politikojj/model"
	"gitlab.com/bertrandbenj/politikojj/storage"
)

func (proc CratosProcessingService) IsAuthorized(role string, perm string) bool {
	if proc.permissions[perm] != nil {
		return proc.rbac.IsGranted(role, proc.permissions[perm], nil)
	}
	return false
}

type CratosProcessingService struct {
	store       storage.Store
	chain       chain.BlockChainService
	Pools       *TemporaryPool
	rbac        *gorbac.RBAC
	permissions gorbac.Permissions
}

func (proc CratosProcessingService) ApplyDelta(t int64, oldBlock model.Block, updatedCratosCid chan string, evidences chan model.Evidence, waitG *sync.WaitGroup) {
	defer waitG.Done()

	// === load from CID
	var oldDemos model.Demos
	proc.store.GetObject(oldBlock.Demos.Link, &oldDemos)

	var oldCratos model.Cratos
	if proc.store.GetObject(oldBlock.Cratos.Link, &oldCratos) {

		// === Apply delta
		newCratos := oldCratos

		ballotCount := proc.Pools.CountVotes()
		proc.ApplyAmendment(ballotCount[model.VoteAmendment], &newCratos, evidences)
		proc.ApplyVariableValueChange(&newCratos, oldDemos, evidences)

		// === Persist new one
		newCid, err := proc.store.PutObject(newCratos)
		if err != nil {
			log.Error("cratos dag put error ", newCid)
		}
		updatedCratosCid <- newCid
	}
}

func (proc CratosProcessingService) ApplyAmendment(countAmendmentVote map[string]uint64, newCratos *model.Cratos, evidences chan model.Evidence) {

	for amendmentCid, v := range countAmendmentVote {
		if v >= proc.chain.GetHead().Majority() {
			var amendment model.Amendment
			if proc.store.GetObject(amendmentCid, &amendment) {

				for _, vr := range amendment.Predicates {

					var installFunction = model.InstallAmendment[vr.Predicate]
					if installFunction == nil {
						log.Error("Unknown Installation method for", vr.Predicate)
					}
					installFunction(&vr, newCratos)

				}

			}
			//Pools.Remove(amendmentCid)
			evidences <- model.Evidence{
				CidLink: model.CidLink{Link: amendmentCid},
				Object:  amendment,
			}
			for voteK, vote := range proc.Pools.GetVotes() {
				if vote.VoteObject == amendmentCid {
					evidences <- model.Evidence{
						CidLink: model.CidLink{Link: voteK},
						Object:  vote,
					}
					//Pools.Remove(voteK)
				}
			}

		}
	}
}

func (proc CratosProcessingService) ApplyVariableValueChange(newCratos *model.Cratos, demos model.Demos, evidences chan model.Evidence) {
	for voteCid, ballot := range proc.Pools.GetVotes() {
		switch ballot.Permission {
		case model.VoteVariableValue:
			variable := newCratos.Variables[ballot.VoteObject]

			// Set unvoted value of new members to the value applicable they entered
			for _, v := range demos.PublicKeysByRole["Citizen"] {
				if (&variable).VotesByPk[v] == nil {
					(&variable).VotesByPk[v] = variable.Value
				}
			}

			// Apply the new vote
			vValue, _ := strconv.ParseFloat(ballot.VoteValue, 64)
			(&variable).VotesByPk[ballot.PublicKey] = vValue

			// calculate the new variable value
			//vars := newCratos.Variables[ballot.VoteObject]
			(&variable).Avg()
			(*newCratos).Variables[ballot.VoteObject] = variable
			evidences <- model.Evidence{
				CidLink: model.CidLink{Link: voteCid},
				Object:  ballot,
			}
		}
	}
}

func (proc CratosProcessingService) DeserializeFromCratos(block model.Cratos) {
	// map[RoleId]PermissionIds
	var jsonRoles = block.Roles.Permission
	// map[RoleId]ParentIds
	var rolesInheritance = block.Roles.Inheritance

	// Build roles and add them to goRBAC instance
	for rid, pids := range jsonRoles {
		role := gorbac.NewStdRole(rid)
		for _, pid := range pids {
			_, ok := proc.permissions[pid]
			if !ok {
				proc.permissions[pid] = gorbac.NewStdPermission(pid)
			}
			_ = role.Assign(proc.permissions[pid])
		}
		_ = proc.rbac.Add(role)
	}

	// Assign the inheritance relationship
	for rid, parents := range rolesInheritance {
		if err := proc.rbac.SetParents(rid, parents); err != nil {
			log.Error(err)
		}
	}
}

func (proc CratosProcessingService) SerializeForCratos() model.Roles {
	roles := model.Roles{
		Permission:  make(map[string][]string),
		Inheritance: make(map[string][]string)}

	WalkHandler := func(r gorbac.Role, parents []string) error {
		// WARNING: Don't use goRbac RBAC instance in the handler,
		// otherwise it causes deadlock.
		permissions := make([]string, 0)
		for _, p := range r.(*gorbac.StdRole).Permissions() {
			permissions = append(permissions, p.ID())
		}

		roles.Permission[r.ID()] = permissions
		roles.Inheritance[r.ID()] = parents
		return nil
	}

	if err := gorbac.Walk(proc.rbac, WalkHandler); err != nil {
		log.Error(err)
	}

	return roles
}
