package node

import (
	"sync"
	"time"

	"gitlab.com/bertrandbenj/politikojj/chain"
	"gitlab.com/bertrandbenj/politikojj/model"
	"gitlab.com/bertrandbenj/politikojj/storage"
	"gitlab.com/bertrandbenj/politikojj/utilities"
)

type CurrencyProcessorService struct {
	store storage.Store
	chain chain.BlockChainService
	Pools *TemporaryPool
}

func IsNewDay(oldTime int64, nowT int64) bool {
	y2, m2, d2 := time.Unix(nowT, 0).Date()
	y1, m1, d1 := time.Unix(oldTime, 0).Date()
	return y2 > y1 || m2 > m1 || d2 > d1
}

func (ccy CurrencyProcessorService) addUniversalDividend(oldBlock model.Block, inflation float64, newCurrency model.Currency, oldCurrency model.Currency) {

	var oldDemos model.Demos
	if ccy.store.GetObject(oldBlock.Demos.Link, &oldDemos) {
		mm := oldCurrency.MonetaryMass
		pop := oldDemos.Population
		ud, _ := utilities.DailyUbi(oldCurrency, inflation, pop)

		for _, v := range oldDemos.CitizensByName {
			pk := v.PublicKey[0]
			newCurrency.Wallets[pk] += uint64(ud)
		}

		newCurrency.MonetaryMass = mm + (uint64(ud) * pop)
	}

}

func (ccy CurrencyProcessorService) ApplyDelta(t int64, oldBlock model.Block, updatedCid chan string, evidences chan model.Evidence, waitG *sync.WaitGroup) {

	defer waitG.Done()
	// === load from CID
	oldCid := oldBlock.Currency.Link
	var oldCcy model.Currency
	if !ccy.store.GetObject(oldCid, &oldCcy) {
		log.Error("couldn't load Currency")
		return
	}

	var oldCratos model.Cratos
	if !ccy.store.GetObject(oldBlock.Cratos.Link, &oldCratos) {
		log.Error("couldn't load Cratos")
		return
	}
	taxes := oldCratos.Taxes
	inflation := oldCratos.Variables["Inflation"].Value.(float64)

	// === Apply delta
	newCurrency := oldCcy
	if IsNewDay(oldBlock.Timestamp, t) {
		ccy.addUniversalDividend(oldBlock, inflation, newCurrency, oldCcy)
	}

	var e []model.Evidence
	ccy.Pools.RLock()
	for cid, tx := range ccy.Pools.GetTxs() {

		if newCurrency.Wallets[tx.PublicKey] >= tx.Amount {
			newCurrency.Wallets[tx.PublicKey] -= tx.Amount
			totalTaxed := uint64(0)
			for _, tax := range taxes {
				taxRate := oldCratos.Variables[tax.Variable].Value.(float64)
				taxAmount := uint64(taxRate * float64(tx.Amount))
				totalTaxed += taxAmount
				newCurrency.PublicWallets[tax.Account] += taxAmount
			}
			newCurrency.Wallets[tx.To] += tx.Amount - totalTaxed
			e = append(e,
				model.Evidence{
					CidLink: model.CidLink{Link: cid},
					Object:  tx})
		}
	}
	ccy.Pools.RUnlock()

	for _, v := range e {
		evidences <- v
	}

	// === Persist new one
	newCid, err := ccy.store.PutObject(newCurrency)
	if err != nil {
		log.Error("Currency dag put error ", oldCid, err)
	}
	updatedCid <- newCid

}
