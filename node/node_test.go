package node

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/bertrandbenj/politikojj/chain"
	"gitlab.com/bertrandbenj/politikojj/crypto"
	"gitlab.com/bertrandbenj/politikojj/model"
	storage "gitlab.com/bertrandbenj/politikojj/storage/mock"
)

var smith *BlockSmith
var ch *chain.BlockChainService
var store *storage.IPFSStoreMock

func init() {
	key := crypto.LoadKey("../testset/ed25519.priv")

	store = storage.NewFileSystemStore("/tmp/testnode/")
	ch = chain.NewChain(store, "TestNet")
	smith = NewBlockSmith(ch, key, 1)
}

func TestIsNewDay(t *testing.T) {
	day1 := time.Date(2020, 01, 01, 13, 52, 45, 0, time.UTC)
	day2 := time.Date(2020, 01, 02, 13, 52, 45, 0, time.UTC)

	assert.True(t, IsNewDay(day1.Unix(), day2.Unix()), "day2 isn't day1 +1")

}

func TestBlockSmith_CalculatePow(t *testing.T) {
	genesis := smith.Chain.Genesis(smith.Key, "../docs/constitution.md")
	res := smith.CalculatePow(genesis)
	assert.True(t, smith.Chain.IsBlockValid(res))
}

func TestSome(t *testing.T) {
	u := crypto.GenEd25519()
	cht := chain.NewChain(store, "Test2")

	sm := NewBlockSmith(cht, u, 1)
	sm.CratosP.DeserializeFromCratos(cht.GetHead().Cratos)
	sm.CratosP.SerializeForCratos()

}

func TestPolitikojjError_Error(t *testing.T) {
	err := PolitikojjError("An Error")
	assert.NotNil(t, err)
	assert.Contains(t, err.Error(), "An Error")
}

func TestAll(t *testing.T) {
	t.Parallel()

	assert := assert.New(t)
	// Create a new genesis block
	genesis := ch.Genesis(smith.Key, "../docs/constitution.md")
	genesis = smith.CalculatePow(genesis)
	_, _, err := ch.ChainBlock(genesis)
	assert.Nil(err, "chaining block #0")

	// Create a new user who request Citizenship and get it by vote
	u2 := crypto.GenEd25519()
	claim := chain.MakeClaim("citizen1", model.Citizen, u2)
	claimCId, _ := store.PutObject(claim)
	smith.Pools.Add(claimCId, claim)
	voteGrant := chain.MakeVote(model.VoteGrantClaim, claimCId, "Grant", smith.Key)
	grantCid, _ := store.PutObject(voteGrant)
	smith.Pools.Add(grantCid, voteGrant)

	// Build the block #1
	b1, err := smith.GenerateBlock(genesis)
	assert.Nil(err, "Generating block #1")
	assert.Nil(smith.CheckForkOrChain(b1), "chaining block #1")

	assert.Equal(ch.Head.Demos.Population, uint64(2), "Demos size should be 2")

	// propose an amendment and have the 2 users vote for it  for it
	amendSS := socialSecurityAmendment()
	amendCid, _ := store.PutObject(amendSS)
	smith.Pools.Add(amendCid, amendSS)
	voteAmendment1 := chain.MakeVote(model.VoteAmendment, amendCid, "Grant", smith.Key)
	amendVote1, _ := store.PutObject(voteAmendment1)
	smith.Pools.Add(amendVote1, voteAmendment1)
	voteAmendment2 := chain.MakeVote(model.VoteAmendment, amendCid, "Grant", u2)
	amendVote2, _ := store.PutObject(voteAmendment2)
	smith.Pools.Add(amendVote2, voteAmendment2)

	// Build the block #2
	b2, err := smith.GenerateBlock(b1)
	assert.Nil(err, "Generating block #2")
	assert.Nil(smith.CheckForkOrChain(b2), "chaining block #1")

	assert.Equal(len(ch.Head.Cratos.Taxes), 1, "There should be one tax")

	// Make a transaction
	tx := chain.MakeTransaction(u2.PubID(), 12345, smith.Key)
	txCid, _ := store.PutObject(tx)
	smith.Pools.Add(txCid, tx)

	// Build the block #3
	b3, err := smith.GenerateBlock(b2)
	assert.Nil(err, "Generating block #3")
	assert.Nil(smith.CheckForkOrChain(b3), "chaining block #1")

	ccy := ch.Head.Currency
	assert.Equal(ccy.SumAccounts(), ccy.MonetaryMass, "Monetary Mass be equal to the sum of all accounts")
	assert.Greater(ccy.PublicWallets["socialSecurityAccount"], uint64(1), "test tax account shouldnt be empty")

	//Vote a variable change
	voteTax := chain.MakeVote(model.VoteVariableValue, "socialSecurityTaxRate", "0.4", u2)
	vtCid, _ := store.PutObject(voteTax)
	smith.Pools.Add(vtCid, voteTax)

	// Build the block #4
	b4, err := smith.GenerateBlock(b3)
	assert.Nil(err, "Generating block #4")
	assert.Nil(smith.CheckForkOrChain(b4), "chaining block #1")

	assert.Equal(.45, ch.Head.Cratos.Variables["socialSecurityTaxRate"].Value, "tax rate should be the average")

}

func socialSecurityAmendment() model.Amendment {
	return chain.MakeAmendment("SocialSecurity", []model.Rule{
		{Predicate: model.ProposeCreateAccount,
			Subject: "socialSecurityAccount",
			Object:  "CurrencyName"},
		{Predicate: model.ProposeAccountPermission,
			Subject: "socialSecurityAccount",
			Object:  "SpendSocialSecurityAccount"},
		{Predicate: model.ProposeTax,
			Subject: ".",
			Object:  "socialSecurityTax"},
		{Predicate: model.ProposeNewVariable,
			Subject: "socialSecurityTaxRate",
			Object:  model.UnitIntervalType},
		{Predicate: model.ProposeTaxRevenueTarget,
			Subject: "socialSecurityTax",
			Object:  "socialSecurityAccount"},
		{Predicate: model.ProposeTaxType,
			Subject: "socialSecurityTax",
			Object:  model.GlobalTransactionTax},
		{Predicate: model.ProposeTaxVar,
			Subject: "socialSecurityTax",
			Object:  "socialSecurityTaxRate"},
		{Predicate: model.ProposeNewRole,
			Subject: ".",
			Object:  "TreasurerSocialS"},
		{Predicate: model.ProposeAddPermission,
			Subject: "TreasurerSocialS",
			Object:  "SpendSocialSecurityAccount"}}, smith.Key)
}
