# Preambule
Acknowledging the failure of nation states to fulfill the democratic mission they were empowered to, we hereby constitute ourself as a cyber-state.

Acknowledging the failure of central banks to improve economic freedom, we hereby declare our own fairly minted currency.

Acknowledging the failure of un-representative democracies to execute the will of the people, we constitute ourself with direct voting ability to propose, decide and revoke. 

Acknowledging the failure of anti social tax policies to drive society toward a better common good, we take all tax matters under collective direct voting. 

Acknowledging the failure corruption inside un-elected bodies and institutions we constitute ourself with the right to revoke all public role.

# Constitution
## Article 1
The right of peoples to self-determination is an inalienable right.

## Article 2
We have no elected government, nor are we likely to have one.

## Article 3 
Suffrage is mostly direct and can be exercised by every Citizen.

Citizens can propose amendments to the law.

Suffrage may grant roles and permissions over executive actions.

Suffrage may overrule any executive action, amendments or revoke any granted permission. 

## Article 4
This Constitution is the sole foundation for the exercise of executive, legislative and judicial power of this community

## Article 5
A Cyber Citizen can change his mind on any public matter and vote accordingly as often as technically possible 

## Article 6
The Constitution's currency may be ruled only by its citizens 

The Currency is a non-exclusive means of exchange 

The Currency is an accepted tax payment 


## Article 7 
...