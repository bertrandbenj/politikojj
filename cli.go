package main

import (
	"bufio"
	"bytes"
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"net/http"
	"os"
	"strconv"
	"strings"

	"gitlab.com/bertrandbenj/politikojj/chain"
	"gitlab.com/bertrandbenj/politikojj/crypto"
	"gitlab.com/bertrandbenj/politikojj/model"
	"gitlab.com/bertrandbenj/politikojj/network"
	"gitlab.com/bertrandbenj/politikojj/node"
	"gitlab.com/bertrandbenj/politikojj/storage"
)

var flags struct {
	// global
	server     *string
	restPort   *int
	tcpPort    *int
	private    *string
	passphrase *string
	cert       *string
	pem        *string

	// p2p
	p2pPort *int
	target  network.AddrList
	secio   *bool
	seed    *int64

	// tx
	//from   *string
	to     *string
	amount *int

	// Claim
	publicKey *string
	name      *string
	role      *string

	// vote
	permission *string
	voteObject *string
	voteValue  *string

	// daemon
	headCid *string
	tcp     *bool
	p2p     *bool
	rest    *bool
	genesis *bool
	sync    *string
	cpu     *int
}

func InteractiveAmendment() model.Amendment {
	scanCredentials()

	if len(*flags.name) <= 0 {
		*flags.name = ScanX("Enter a name for your amendment : ")
	}

	var rules []model.Rule

	for {
		discontinue := ScanX("Add a rule ? Y/n")
		if discontinue == "n" {
			break
		}
		subj := ScanX("Enter Subject : ")
		pred := ScanX("Enter Predicate : " + fmt.Sprint(model.AvailablePropose))
		obj := ScanX("Enter Object : ")

		rules = append(rules, model.Rule{
			Subject:   subj,
			Predicate: pred,
			Object:    obj,
		})
	}

	return chain.MakeAmendment(*flags.name, rules, keypair())
}

func keypair() crypto.Keypair {
	//	crypto.ReadPubAndPrivateArmorKeyFile(*flags.publicKey, *flags.private, *flags.passphrase)

	key := crypto.LoadKey(*flags.private)
	return key
}

func InteractiveVote() model.OpenBallot {
	scanCredentials()
	if len(*flags.permission) <= 0 {
		*flags.permission = ScanX("Enter the permission you want to use " + fmt.Sprint(model.AvailableVotes))
	}
	if len(*flags.voteObject) <= 0 {
		*flags.voteObject = ScanX("Enter the cid of the object you want to vote upon ")
	}
	if len(*flags.voteValue) <= 0 {
		*flags.voteValue = ScanX("Enter the value of your vote ")
	}

	res := chain.MakeVote(*flags.permission, *flags.voteObject, *flags.voteValue, keypair())

	return res
}

func InteractiveClaim() model.Claim {
	scanCredentials()
	if len(*flags.name) <= 0 {
		*flags.name = ScanX("Enter a username eg: bob ")
	}

	if len(*flags.role) <= 0 {
		*flags.role = ScanX("Enter a role name eg: Citizen ")
	}

	res := chain.MakeClaim(*flags.name, *flags.role, keypair())

	return res
}

func scanCredentials() {
	if len(*flags.private) <= 0 {
		*flags.private = ScanX("Enter the private key file (must match the public key from) ")
	}

	if len(*flags.passphrase) <= 0 {
		*flags.passphrase = ScanX("Enter the private key file decryption passphrase ")
	}

	if len(*flags.publicKey) <= 0 {
		*flags.publicKey = ScanX("Enter Public key from ")
	}

}

func InteractiveTransaction() model.Transaction {
	scanCredentials()

	if len(*flags.to) <= 0 {
		*flags.to = ScanX("Enter Public key To ")
	}

	if *flags.amount < 0 {
		*flags.amount, _ = strconv.Atoi(ScanX("Enter the amount you wish to transfer "))
	}

	return chain.MakeTransaction(*flags.to, *flags.amount, keypair())
}

func SendDoc(url string, doc interface{}, returnObject interface{}) {

	jsonDoc, _ := json.Marshal(doc)
	fullURL := "http://" + *flags.server + ":" + strconv.Itoa(*flags.restPort) + url
	log.Info("Posting to ", fullURL, string(jsonDoc))
	res, err := http.Post(fullURL, "application/json", bytes.NewBuffer(jsonDoc))
	if err != nil {
		log.Error(err)
	} else {
		decoder := json.NewDecoder(res.Body)
		if err := decoder.Decode(&returnObject); err != nil {
			log.Error("Decoding", err)
		}
		log.Info("Got response ", returnObject)
	}
}

func ExecuteSubCommand() {

	if len(os.Args) < 2 {
		os.Args = append(os.Args, "help")
	}

	switch os.Args[1] {
	case "help":
		//case "--help":
		//case "-h":
		fmt.Println("Politikojj : Direct Democracy inside a blockchain ")
		fmt.Println("  ")

		fmt.Println("politikojj SUBCOMMAND -flag1 value1 -flag2=value2 ")
		fmt.Println("")
		fmt.Println("Use -h flag to have detail information on subcommand")
		fmt.Println()
		fmt.Println("SUBCOMMAND includes : ")
		fmt.Println(" SERVER ")
		fmt.Println("  daemon  : Start a long running process keeping the blockchain up to date")
		fmt.Println(" CLI ")
		fmt.Println("  claim   : build, sign and send a Claim request to the blockchain ")
		fmt.Println("  vote    : build, sign and send a Vote to the blockchain ")
		fmt.Println("  propose : build, sign and send a Proposition for Amendment to the blockchain ")
		fmt.Println("  tx      : build, sign and send a Transaction to the blockchain ")

		return
	case "daemon":
		RunAsDaemon()
		return
	case "tx":
		parseOrPrintUsage(CliTX())
		trx := InteractiveTransaction()
		res := model.Wrapper{}
		SendDoc("/tx/send", trx, &res)
		return
	case "claim":
		parseOrPrintUsage(CliClaim())
		cl := InteractiveClaim()
		res := map[string]model.Claim{}
		SendDoc("/claim/role", cl, &res)

		return
	case "vote":
		parseOrPrintUsage(CliVote())
		v := InteractiveVote()
		res := model.OpenBallot{}
		SendDoc("/vote", v, &res)

		return
	case "propose":
		parseOrPrintUsage(CliPropose())
		a := InteractiveAmendment()
		res := model.Amendment{}
		SendDoc("/amend", a, &res)
		return
	default:
		fmt.Println("default ", os.Args[1])
		flag.Parse()
	}
}

func parseOrPrintUsage(f *flag.FlagSet) {
	flags.private = f.String("private", os.Getenv("PRIVATEKEY"), "/path/to/private")
	flags.passphrase = f.String("passphrase", os.Getenv("PASSPHRASE"), "secret to decrypt private key's file")
	flags.server = f.String("server", "127.0.0.1", "IP or domain")
	flags.cert = f.String("cert", os.Getenv("SSL_CERT"), "cert.pem file for TLS")
	flags.pem = f.String("pem", os.Getenv("SSL_KEY"), "key.pem file for TLS")

	p, _ := strconv.Atoi(os.Getenv("REST_PORT"))
	flags.restPort = f.Int("rest_port", p, "REST API Port number")

	err := f.Parse(os.Args[2:])
	if err != nil {
		f.Usage()
	}
}

func RunAsDaemon() {
	ctx := context.Background()
	parseOrPrintUsage(CliDaemon())
	log.Notice(" === Welcome to Politikojj === ")

	// Init some data structure and check connectivity to tier services
	key := keypair()

	var store = storage.NewIPFSStore(os.Getenv("IPFS"), os.Getenv("IPFS_PROJECT_FOLDER"))
	var chainS = chain.NewChain(store, "TestNet")
	var smith = node.NewBlockSmith(chainS, key, *flags.cpu)
	var rest = network.NewRest(smith)
	var tcp = network.NewTCP(smith, *flags.tcpPort)
	var p2p = network.NewP2P(ctx, smith, *flags.p2pPort, *flags.seed, *flags.secio, flags.target)

	if len(*flags.headCid) > 0 {
		var head model.Block
		if store.GetObject(*flags.headCid, &head) {
			_, _, _ = chainS.ChainBlock(head)
		} else {
			log.Warning("No headCID ", *flags.headCid)
		}
	}

	if *flags.genesis {
		//go func() {
		genesisBlock := chainS.Genesis(smith.Key, os.Getenv("CONSTITUTION"))
		smith.CalculatePow(genesisBlock)
		_, cid, err := chainS.ChainBlock(genesisBlock)
		if err != nil {
			log.Error(" chaining block ", cid, err)
		} else {
			log.Info("generated genesis block : ", cid)
		}

	}

	if len(*flags.sync) > 0 {
		urls := strings.Split(*flags.sync, ",")
		for _, url := range urls {
			log.Info("connecting to", url)
		}
	}

	if *flags.rest {
		restCloser, err := rest.StartupRest(*flags.restPort, *flags.pem, *flags.cert)
		if err != nil {
			log.Error("Error startup the Rest Server ")
		} else {
			defer restCloser.Close()
		}
	}

	if *flags.p2p {
		//go p2p.startupP2P(flags.target)
		go p2p.RunMDNS(ctx, "MeetMeAtTheEntranceOfTheCyberSpace")
		//go p2p.runKademliaDHT(context.Background(), "MeetMeAtTheEntranceOfTheCyberSpace",flags.target)
	}

	go smith.RunBlockSmith()

	if *flags.tcp {
		tcp.StartupTCP()
	}

	select {}
}

func CliTX() *flag.FlagSet {
	cmd := flag.NewFlagSet("tx", flag.ExitOnError)

	flags.publicKey = cmd.String("from", os.Getenv("PUBLICKEY_CID"), "Public key's CID")
	flags.to = cmd.String("to", "", "Public key's CID")
	flags.amount = cmd.Int("amount", -1, "amount to transfer")
	//flags.private = cmd.String("private", os.Getenv("PRIVATEKEY"), "/path/to/private")
	//flags.passphrase = cmd.String("passphrase", os.Getenv("PASSPHRASE"), "secret to decrypt private key's file")
	//flags.server = cmd.String("server", "127.0.0.1", "IP or domain")
	//flags.restPort = cmd.String("restPort", os.Getenv("PORT"), "restPort number")
	return cmd
}

func CliClaim() *flag.FlagSet {
	cmd := flag.NewFlagSet("claim", flag.ExitOnError)
	flags.publicKey = cmd.String("pubkey", os.Getenv("PUBLICKEY_CID"), "Public key's CID")
	flags.role = cmd.String("role", "", "The role you claim. eg. Citizen")
	flags.name = cmd.String("name", "", "Your Nickname")

	return cmd
}
func CliVote() *flag.FlagSet {
	cmd := flag.NewFlagSet("vote", flag.ExitOnError)
	flags.publicKey = cmd.String("pubkey", os.Getenv("PUBLICKEY_CID"), "Public key's CID")
	flags.voteObject = cmd.String("object", "", "The CID of the object you wish to vote upon")
	flags.voteValue = cmd.String("value", "", "the value of your vote")
	flags.permission = cmd.String("permission", "", "The permission you use ")
	return cmd
}
func CliPropose() *flag.FlagSet {
	cmd := flag.NewFlagSet("propose", flag.ExitOnError)
	flags.publicKey = cmd.String("pubkey", os.Getenv("PUBLICKEY_CID"), "Public key's CID")
	flags.name = cmd.String("name", "", "Name of your Amendment")

	return cmd
}
func CliDaemon() *flag.FlagSet {
	cmd := flag.NewFlagSet("daemon", flag.ExitOnError)

	// options to startup a node server : genesis XOR head XOR sync
	flags.headCid = cmd.String("head", "", "An IPFS cid pointing to the head of the blockchain")
	flags.genesis = cmd.Bool("genesis", true, "Start with a new genesis block ")
	flags.sync = cmd.String("sync", "", "[url1, url2] Use comma separated list of url to connect to at startup")

	// services to setup
	flags.tcp = cmd.Bool("tcp", true, "Use TCP")
	flags.p2p = cmd.Bool("p2p", true, "Use P2P")
	flags.rest = cmd.Bool("rest", true, "use REST")

	envCPU, err := strconv.Atoi(os.Getenv("POW_CORES"))
	if err != nil || envCPU < 1 {
		envCPU = 1
	}
	flags.cpu = cmd.Int("cpu", envCPU, "number of CPU cores to be used for PoW calculations. Any value out of interval [1;maxCPU] result in maxCPU")

	envTcp, _ := strconv.Atoi(os.Getenv("TCP_PORT"))
	flags.tcpPort = cmd.Int("tcp_port", envTcp, "TCP port on which to wait direct input")

	// p2p params
	p, _ := strconv.Atoi(os.Getenv("P2P_PORT"))
	flags.p2pPort = cmd.Int("p2p_port", p, "P2P port on which to wait for incoming connections")
	cmd.Var(&flags.target, "bootstrap", "P2P peer multi address to dial. separated by comas  eg: /ip4/1.2.3.4/tcp/8443/p2p/Qm....")
	flags.secio = cmd.Bool("secio", false, "Enable Secure I/O of libP2P")
	flags.seed = cmd.Int64("seed", 0, "set random seed for id generation")

	return cmd
}

func ScanX(display string) string {
	fmt.Println(display)
	reader := bufio.NewReader(os.Stdin)
	text, _ := reader.ReadString('\n')
	text = strings.Replace(text, "\n", "", -1)
	return text
}
