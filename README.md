# Politikojj

![banner](https://gitlab.com/bertrandbenj/politikojj/-/raw/master/static/favicon.ico ) 

## What is politikojj ?

Politikojj is a censorship-resistant, self-sovereign governance and currency blockchain.
It is designed for direct democratic usage, propose and amend rules, manage public accounts 
It is written in Go and makes extensive use of IPFS 

The project includes :
- a go binary to run a blockhain's node
- a command line interface to interact with the blockchain
- a static html/js [client interface](https://bertrandbenj.gitlab.io/politikojj)

## Table of Contents
[Choices Justification](docs/choices/concepts.md)
- [A Blockchain for commons](docs/choices/concepts.md#a-blockchain-for-commons)
- [suffrage modality](docs/choices/concepts.md#suffrage-modality)
- [Monetary system](docs/choices/concepts.md#monetary-system)

[Technical Specifications](docs/specs.md)
- [Use case](docs/specs.md#use-case)    
- [Amendments](docs/specs.md#amendments)


## Install 

### Prerequisite 
```
# some of wich may be overkill 
sudo apt-get install build-essential g++ libffi-dev make gcc ruby-dev ruby-full zlib1g-dev nodejs 

# install ruby jekyll 
gem install jekyll bundler

# DL and install IPFS 
wget https://dist.ipfs.io/go-ipfs/v0.8.0/go-ipfs_v0.8.0_linux-amd64.tar.gz
tar -xvzf go-ipfs_v0.8.0_linux-amd64.tar.gz
cd go-ipfs
sudo bash install.sh 
ipfs --version 
ipfs init 

```

### Download latest binary [here](https://gitlab.com/bertrandbenj/politikojj/-/releases)



## Usage 

### Start IPFS 
```
ipfs daemon 
```

### Start the client
You can use a static version [here](https://bertrandbenj.gitlab.io/politikojj)

### Start the client
```
cd client
bundle exec jekyll serve
```

### Start the server 
```
# packaged binary 
./politikojj daemon

# from source 
go run . daemon 

```


Use one of the following subcommand 
```
./politikojj daemon
./politikojj tx
./politikojj claim
./politikojj vote
./politikojj propose
```
a -h flag is available for each 

