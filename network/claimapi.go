package network

import (
	"gitlab.com/bertrandbenj/politikojj/model"
	"net/http"
)

func (rest RestService) GetClaimRole(w http.ResponseWriter, _ *http.Request) {
	rest.respondObjectAsJson(w, rest.smith.Pools.Claims)
}

func (rest RestService) GetApprovedClaimRole(w http.ResponseWriter, _ *http.Request) {
	rest.respondObjectAsJson(w, rest.smith.Pools.ApprovedClaims)
}

func (rest RestService) PostClaimRole(w http.ResponseWriter, r *http.Request) {
	var cl model.Claim
	log.Info("entering ... ")

	if rest.DecodeReceivedJSON(w, r, &cl) {

		if rest.VerifySignable(w, &cl) {

			if cid, ok := rest.StoreToIPFS(w, cl); ok {
				rest.smith.Pools.Add(cid, cl)
				log.Warning(rest.smith.Pools.Claims)
				rest.respondObjectAsJson(w, rest.smith.Pools.Claims)
			}

		}

	}
}

func (rest RestService) PostApprovedClaim(w http.ResponseWriter, r *http.Request) {
	var acl model.ApprovedClaim
	if rest.DecodeReceivedJSON(w, r, &acl) {

		if rest.VerifySignable(w, &acl) {

			if cid, ok := rest.StoreToIPFS(w, acl); ok {
				rest.smith.Pools.Add(cid, acl)
				rest.respondObjectAsJson(w, rest.smith.Pools.ApprovedClaims)
			}

		}

	}

}
