package network

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"github.com/multiformats/go-multiaddr"
	"github.com/stretchr/testify/assert"
	"gitlab.com/bertrandbenj/politikojj/chain"
	"gitlab.com/bertrandbenj/politikojj/crypto"
	"gitlab.com/bertrandbenj/politikojj/model"
	"gitlab.com/bertrandbenj/politikojj/node"
	storage "gitlab.com/bertrandbenj/politikojj/storage/mock"
	"gitlab.com/bertrandbenj/politikojj/utilities"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"time"
)

var rest *RestService
var store *storage.IPFSStoreMock
var chainS *chain.BlockChainService
var smith *node.BlockSmith
var wait time.Duration

//check https://semaphoreci.com/community/tutorials/building-and-testing-a-rest-api-in-go-with-gorilla-mux-and-postgresql
func init() {
	key := crypto.LoadKey("../testset/ed25519.priv")

	store = storage.NewFileSystemStore("/tmp/testnetwork/")
	chainS = chain.NewChain(store, "TestNet")
	smith = node.NewBlockSmith(chainS, key, 2)
	rest = NewRest(smith)

	_, err := rest.StartupRest(8083, "", "")

	if err != nil {
		log.Error(err)
	}
	utilities.ConfigLogger("DEBUG")

	wait, err = time.ParseDuration(os.Getenv("SEC"))
	if err != nil || wait == time.Duration(0) {
		wait = 1 * time.Second
	}

	log.Infof("using WaitTime %s, %d ", os.Getenv("SEC"), wait)

}

type testObject struct {
	field1 string
	field2 int
	field3 interface{}
}

var X = testObject{
	field1: "HelloWorld",
	field2: 123456,
	field3: testObject{
		field1: "Recursive?",
		field2: 321654,
		field3: nil,
	},
}

func TestRest(t *testing.T) {
	data, _ := json.MarshalIndent(X, "", "\t")

	r, _ := http.NewRequest("GET", "/block/0", bytes.NewBuffer(data))
	w := httptest.NewRecorder()

	rest.GetBlock(w, r)

	var result testObject
	rest.DecodeReceivedJSON(w, r, &result)
	log.Info(result)

	assert.Equal(t, http.StatusInternalServerError, w.Code, "Response code")

	rest.GetCCY(w, r)
	rest.GetCratos(w, r)
	rest.GetDemos(w, r)
	rest.GetApprovedClaimRole(w, r)
	rest.GetAvailables(w, r)
	rest.GetBlockchain(w, r)
	rest.GetHeadCid(w, r)
	rest.GetPools(w, r)
	rest.GetClaimRole(w, r)
	rest.GetRBAC(w, r)
	rest.GetNetwork(w, r)
	rest.HandleKeyGen(w, r)
	rest.FileHandler(w, r)

	ssd := model.SignedStampedDoc{
		Timestamp: 123456789,
		PublicKey: smith.Key.PubID(),
	}

	testCl := model.Claim{
		ChosenName:       "NAME",
		Role:             model.Citizen,
		SignedStampedDoc: ssd,
	}
	testCl.Signature, _ = smith.Key.Sign(testCl.PartToSign())
	rest.VerifyRuleDataType(model.Amendment{
		Name:             "huhu",
		Predicates:       []model.Rule{},
		SignedStampedDoc: ssd,
	})
	rest.VerifySignable(w, &testCl)
	rest.StoreToIPFS(w, testCl)

	rest.PostBlock(w, r)

}

func TestRestService_PostClaimRole(t *testing.T) {

	u1 := crypto.GenEd25519()
	cl := chain.MakeClaim("userx", model.Citizen, u1)

	rest.PostClaimRole(mockHTTP(cl))

	acl := chain.MakeApprovedClaim(cl, smith.Key)
	rest.PostApprovedClaim(mockHTTP(acl))
}

func TestRestService_PostTX(t *testing.T) {
	tx := chain.MakeTransaction("BOB", 1000, smith.Key)
	rest.PostTX(mockHTTP(tx))
}

func mockHTTP(payload interface{}) (http.ResponseWriter, *http.Request) {
	data, _ := json.MarshalIndent(payload, "", "\t")
	r, _ := http.NewRequest("POST", "...", bytes.NewBuffer(data))
	w := httptest.NewRecorder()
	return w, r
}

func TestRestService_PostAmendment(t *testing.T) {

	u1 := crypto.GenEd25519()
	amendment := chain.MakeAmendment("SocialSecurity", []model.Rule{
		{Predicate: model.ProposeCreateAccount,
			Subject: "socialSecurityAccount",
			Object:  "CurrencyName"},
		{Predicate: model.ProposeAccountPermission,
			Subject: "socialSecurityAccount",
			Object:  "SpendSocialSecurityAccount"},
		{Predicate: model.ProposeTax,
			Subject: ".",
			Object:  "socialSecurityTax"}}, u1)

	rest.PostAmendment(mockHTTP(amendment))

	vote := chain.MakeVote(model.VoteAmendment, "xxx", "Grant", smith.Key)
	rest.PostVote(mockHTTP(vote))
}

func TestStringsToAddrs(t *testing.T) {
	ma, err := StringsToAddrs([]string{"/ip4/127.0.0.1/tcp/80/"})
	assert.Nil(t, err)
	assert.NotNil(t, ma)

	adl := AddrList{}
	assert.Nil(t, adl.Set("/ip4/127.0.0.1/tcp/80/"))
	assert.NotNil(t, adl.String())
}

func TestNewTCP(t *testing.T) {
	key := crypto.LoadKey("../testset/ed25519.priv")
	st1 := storage.NewFileSystemStore("/tmp/politikojj/nettcp/")
	ch1 := chain.NewChain(st1, "TestNet")
	sm1 := node.NewBlockSmith(ch1, key, 1)

	tcp := NewTCP(sm1, 11111)
	go tcp.StartupTCP()

	time.Sleep(3 * time.Second)

	assert.True(t, true)

	tcp.handleConn(nil)
}

func TestServerP2P_RunMDNS(t *testing.T) {
	log.Infof("using WaitTime %s, %d nano", os.Getenv("SEC"), wait)

	assert := assert.New(t)

	RendezVous := "MeetMeAtTheEntranceOfTheCyberSpac3"

	// start A new blockchain
	key := crypto.LoadKey("../testset/ed25519.priv")

	//key := crypto.ReadPubAndPrivateArmorKeyFile("../testset/pub.asc", "../testset/priv.asc", "test1")
	sm1, s1, ctx1 := setupNode(t, 1, key, nil)
	assert.NotNil(s1.GetID())
	go s1.RunMDNS(ctx1, RendezVous)

	// Create a new genesis block
	genesis := sm1.Chain.Genesis(key, "./docs/constitution.md")
	genesis = smith.CalculatePow(genesis)
	_, _, err := sm1.Chain.ChainBlock(genesis)
	assert.Nil(err, "chaining block #0")

	// Now add a user, register him as citizen before he can startup hi node
	u2 := crypto.GenEd25519()
	addUser(u2, "citizen2", sm1)

	// Add another user, register it and start a third node
	u3 := crypto.GenEd25519()
	addUser(u3, "citizen3", sm1)

	b1, err := s1.smith.GenerateBlock(genesis)
	assert.Nil(err, "Generating block #1")
	assert.Nil(s1.smith.CheckForkOrChain(b1), "chaining block #1")
	time.Sleep(wait)

	_, s2, ctx2 := setupNode(t, 2, u2, nil)
	_, _, err = s2.chain.ChainBlock(genesis)
	go s2.RunMDNS(ctx2, RendezVous)

	// Add a new block
	b2, err := s1.smith.GenerateBlock(b1)
	assert.Nil(err, "Generating block #2")
	assert.Nil(s1.smith.CheckForkOrChain(b2), "chaining block #2")
	time.Sleep(wait)

	// start a third node
	_, s3, ctx3 := setupNode(t, 3, u3, nil)
	_, _, err = s3.chain.ChainBlock(genesis)
	go s3.RunMDNS(ctx3, RendezVous)

	// Add a new empty block in the second node
	b3, err := s2.smith.GenerateBlock(s2.chain.GetHead().Block)
	assert.Nil(err, "Generating block #3")
	assert.Nil(s2.smith.CheckForkOrChain(b3), "chaining block #3")
	time.Sleep(5 * wait)

	// Verify block is propagated
	assert.Equal(s1.chain.GetHead().Block, s2.chain.GetHead().Block, "Heads should match between nodes 1 and 2 ")
	assert.Equal(s1.chain.GetHead().Block, s3.chain.GetHead().Block, "Heads should match between nodes 1 and 3 ")

	// check document propagation
	ballot := chain.MakeVote(model.VoteVariableValue, "Inflation", "0.15", u2)
	voteCid, _ := store.PutObject(ballot)
	s2.smith.Pools.Add(voteCid, ballot)
	time.Sleep(5 * wait)

	s1.smith.Pools.RLock()
	s2.smith.Pools.RLock()
	s3.smith.Pools.RLock()
	assert.EqualValues(s1.smith.Pools.Votes, s2.smith.Pools.Votes)
	assert.EqualValues(s3.smith.Pools.Votes, s2.smith.Pools.Votes)
	s1.smith.Pools.RUnlock()
	s2.smith.Pools.RUnlock()
	s3.smith.Pools.RUnlock()
}

func addUser(key crypto.Keypair, name string, smith *node.BlockSmith) {
	claim := chain.MakeClaim(name, model.Citizen, key)
	claimCId, _ := store.PutObject(claim)
	smith.Pools.Add(claimCId, claim)
	voteGrant := chain.MakeVote(model.VoteGrantClaim, claimCId, "Grant", smith.Key)
	grantCid, _ := store.PutObject(voteGrant)
	smith.Pools.Add(grantCid, voteGrant)
}

func setupNode(t *testing.T, id int, key crypto.Keypair, bootstrap []multiaddr.Multiaddr) (*node.BlockSmith, *ServerP2P, context.Context) {
	if bootstrap == nil {
		bootstrap = []multiaddr.Multiaddr{}
	}

	ids := fmt.Sprintf("%d", id)
	p := 10000 + id

	ctx := context.Background()

	st1 := storage.NewFileSystemStore("/tmp/politikojj/net" + ids + "/")
	ch1 := chain.NewChain(st1, "TestNet")
	sm1 := node.NewBlockSmith(ch1, key, 1)
	server := NewP2P(ctx, sm1, p, int64(id), true, bootstrap)
	server.Sleep = 1
	return sm1, server, ctx
}
