package network

import (
	"gitlab.com/bertrandbenj/politikojj/chain"
	"gitlab.com/bertrandbenj/politikojj/model"
	"gitlab.com/bertrandbenj/politikojj/node"
	"gitlab.com/bertrandbenj/politikojj/storage"

	"net"
	"strconv"
)

type TCPService struct {
	smith    *node.BlockSmith
	chain    *chain.BlockChainService
	store    storage.Store
	port     int
	bcServer chan model.Block
}

func NewTCP(smith *node.BlockSmith, port int) *TCPService {

	return &TCPService{
		smith:    smith,
		chain:    smith.Chain,
		store:    smith.Chain.Store,
		port:     port,
		bcServer: make(chan model.Block),
	}
}

func (tcp TCPService) StartupTCP() {
	// find the next available port
	listener, p := nextAvailablePort(tcp.port)
	_ = listener.Close()
	tcp.port = p

	// start TCP and serve TCP server
	server, err := net.Listen("tcp", ":"+strconv.Itoa(tcp.port))
	if err != nil {
		log.Error(err)
	} else {
		log.Infof("TCP server listening on %s", server.Addr())
	}
	defer server.Close()

	for {
		conn, err := server.Accept()
		if err != nil {
			log.Error(err)
		}
		go tcp.handleConn(conn)
	}

}

func (tcp TCPService) handleConn(conn net.Conn) {

	if conn == nil {
		return
	}

	defer conn.Close()
	//utilities.DeferClose(conn, "TCP connection to "+conn.RemoteAddr().String())
}
