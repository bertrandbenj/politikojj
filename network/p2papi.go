package network

import (
	"bufio"
	"encoding/json"
	"github.com/libp2p/go-libp2p-core/network"
	"github.com/mitchellh/mapstructure"
	"gitlab.com/bertrandbenj/politikojj/model"
	"sync"
)

func (p2p ServerP2P) readBlock(rw *bufio.ReadWriter, stream network.Stream) {
	remote := stream.Conn().RemotePeer().Pretty()
	for {
		log.Noticef("%d Read Block from %s", p2p.port, remote)
		str, err := rw.ReadString('\n')
		if err != nil {
			log.Error("ReadString", err)
		}

		if str == "" {
			log.Error("Returning", err)
			return
		}
		if str != "\n" {

			newBlock := model.Block{}
			if err := json.Unmarshal([]byte(str), &newBlock); err != nil {
				log.Error("json.Unmarshal", err)
			}
			err = p2p.smith.CheckForkOrChain(newBlock)
			if err != nil {
				log.Errorf("%d %s", p2p.port, err)
			} else {
				log.Noticef("%d Chained block received from %s", p2p.port, remote)
			}

		}
	}
}

func (p2p *ServerP2P) readPoolDoc(rw *bufio.ReadWriter, stream network.Stream) {
	remote := stream.Conn().RemotePeer().Pretty()

	for {
		log.Noticef("%d Read Doc from %s", p2p.port, remote)
		str, err := rw.ReadString('\n')
		if err != nil {
			log.Error("Reading delimiter", err)
		}

		if str == "" {
			log.Error("Returning", err)
			return
		}
		if str != "\n" {

			var doc model.Wrapper
			if err := json.Unmarshal([]byte(str), &doc); err != nil {
				log.Error("json.Unmarshal", err)
			} else {
				log.Notice("Received new document", doc.CID, doc)
				var ssd model.SignedStampedDoc
				_ = mapstructure.Decode(doc.Data, &ssd)

				switch doc.Type {
				case "OpenBallot":
					var result model.OpenBallot
					_ = mapstructure.Decode(doc.Data, &result)
					result.Stamp(ssd)
					p2p.smith.Pools.Add(doc.CID, result)
				case "Claim":
					var result model.Claim
					_ = mapstructure.Decode(doc.Data, &result)
					result.Stamp(ssd)
					p2p.smith.Pools.Add(doc.CID, result)
				case "ApprovedClaim":
					var result model.ApprovedClaim
					_ = mapstructure.Decode(doc.Data, &result)
					result.Stamp(ssd)
					p2p.smith.Pools.Add(doc.CID, result)
				case "Amendment":
					var result model.Amendment
					_ = mapstructure.Decode(doc.Data, &result)
					result.Stamp(ssd)
					p2p.smith.Pools.Add(doc.CID, result)
				case "Transaction":
					var result model.Transaction
					_ = mapstructure.Decode(doc.Data, &result)
					result.Stamp(ssd)
					p2p.smith.Pools.Add(doc.CID, result)
				}

			}

		}
	}
}

func (p2p ServerP2P) broadcastBlock(remoteHead model.Block) {
	var writeMutex = &sync.Mutex{}
	p2p.streamLock.RLock()

	for remote, rw := range p2p.BlockStreams {
		writeMutex.Lock()

		log.Infof("%d sending head %d to %s", p2p.port, remoteHead.Index, remote)

		if rw == nil {
			log.Errorf("%d broadcasting head %d to %s", p2p.port, remoteHead.Index, remote)
			return
		}

		bytes, err := json.Marshal(remoteHead)
		if err != nil {
			log.Error("Marshal", err)
		}

		_, err = rw.Write(bytes)
		if err != nil {
			log.Error("Write block data", err)
		}
		_, err = rw.WriteString("\n")
		if err != nil {
			log.Error("Writing delimiter", err)
		}
		_ = rw.Flush()
		writeMutex.Unlock()
	}
	p2p.streamLock.RUnlock()
}

func (p2p *ServerP2P) broadcastDoc(doc model.Wrapper) {
	var writeMutex = &sync.Mutex{}

	p2p.streamLock.RLock()
	for remote, rw := range p2p.DocsStreams {
		go func(remote string, writer *bufio.ReadWriter) {
			log.Infof("%d sending Doc %s to %s", p2p.port, doc.Type, remote)

			bytes, err := json.Marshal(doc)
			if err != nil {
				log.Error("Unmarshalling", err)
				return
			}
			writeMutex.Lock()

			n, err := writer.Write(bytes)
			if err != nil {
				log.Error("Write doc data", err, n, len(bytes))
				return
			}
			_, err = writer.WriteString("\n")
			if err != nil {
				log.Error("Writing delimiter", err)
				return
			}
			err = writer.Flush()
			if err != nil {
				log.Error("Flushing rw", err)
				return
			}
			writeMutex.Unlock()

		}(remote, rw)
	}
	p2p.streamLock.RUnlock()
}
