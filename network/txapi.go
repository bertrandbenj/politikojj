package network

import (
	"gitlab.com/bertrandbenj/politikojj/model"
	"net/http"
)

func (rest RestService) PostTX(w http.ResponseWriter, r *http.Request) {
	var tx model.Transaction
	if rest.DecodeReceivedJSON(w, r, &tx) {
		if rest.VerifySignable(w, &tx) {
			if cid, ok := rest.StoreToIPFS(w, tx); ok {
				rest.smith.Pools.Add(cid, tx)
				wrappedTX := model.Wrapper{
					CID:  cid,
					Data: tx,
				}
				rest.respondObjectAsJson(w, wrappedTX)
			}
		}
	}
}
