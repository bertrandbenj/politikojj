package network

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"gitlab.com/bertrandbenj/politikojj/model"
	"io"
	"net/http"
	"strconv"
)

func (rest RestService) PostBlock(w http.ResponseWriter, r *http.Request) {
	var newBlock model.Block

	if rest.DecodeReceivedJSON(w, r, &newBlock) {

		rest.smith.CheckForkOrChain(newBlock)

		rest.respondObjectAsJson(w, rest.chain.HeadBlock())

	}
}

func (rest RestService) GetCratos(w http.ResponseWriter, _ *http.Request) {
	x := model.Wrapper{
		CID:  rest.chain.GetHead().Block.Cratos.Link,
		Data: rest.chain.GetHead().Cratos,
	}
	rest.respondObjectAsJson(w, x)
}

func (rest RestService) GetCCY(w http.ResponseWriter, _ *http.Request) {
	x := model.Wrapper{
		CID:  rest.chain.GetHead().Block.Currency.Link,
		Data: rest.chain.GetHead().Currency,
	}
	rest.respondObjectAsJson(w, x)
}

func (rest RestService) GetDemos(w http.ResponseWriter, _ *http.Request) {
	x := model.Wrapper{
		CID:  rest.chain.GetHead().Block.Demos.Link,
		Data: rest.chain.GetHead().Demos,
	}
	rest.respondObjectAsJson(w, x)
}

func (rest RestService) GetHeadCid(w http.ResponseWriter, _ *http.Request) {
	_, _ = io.WriteString(w, rest.chain.GetHead().Cid)
}

func (rest RestService) GetBlock(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	bid, err := strconv.Atoi(vars["id"])
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	rest.respondObjectAsJson(w, rest.chain.ByIndex(uint32(bid)))
}

func (rest RestService) GetBlockchain(w http.ResponseWriter, _ *http.Request) {

	var prettyChain []model.Wrapper

	rest.chain.BackwardWalk(func(block model.Block, cid string) {
		prettyChain = append(prettyChain, model.Wrapper{
			CID:  cid,
			Data: block})
	})

	bytes, err := json.MarshalIndent(prettyChain, "", "  ")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	_, _ = w.Write(bytes)
}
