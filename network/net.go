package network

import (
	"encoding/json"
	"net/http"
)

var Endpoints endpoints

type endpoints struct {
	Rest []string
}

func (rest RestService) GetNetwork(w http.ResponseWriter, _ *http.Request) {
	log.Info("GetNetwork", Endpoints)
	bytes, _ := json.MarshalIndent(Endpoints, "", "\t")

	w.Header().Set("Content-Type", "application/json")
	n, err := w.Write(bytes)

	if err != nil {
		log.Error("writing ", n, "bytes of data ", err)
	}
}
