package network

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/op/go-logging"
	"gitlab.com/bertrandbenj/politikojj/chain"
	"gitlab.com/bertrandbenj/politikojj/crypto"
	"gitlab.com/bertrandbenj/politikojj/model"
	"gitlab.com/bertrandbenj/politikojj/node"
	"gitlab.com/bertrandbenj/politikojj/storage"
	"gitlab.com/bertrandbenj/politikojj/utilities"
	"io"
	"net"
	"net/http"
	"strconv"
	"time"
)

var log = logging.MustGetLogger("network")

type RestService struct {
	chain *chain.BlockChainService
	store storage.Store
	smith *node.BlockSmith
}

func NewRest(smith *node.BlockSmith) *RestService {
	RestAPI := RestService{
		chain: smith.Chain,
		store: smith.Chain.Store,
		smith: smith}
	return &RestAPI
}

func (rest RestService) makeMuxRouter() http.Handler {
	muxRouter := mux.NewRouter()
	muxRouter.HandleFunc("/favicon.ico", rest.FileHandler).Methods("GET")
	//muxRouter.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir("static/"))))

	// Chain API
	muxRouter.HandleFunc("/chain", rest.GetBlockchain).Methods("GET")
	muxRouter.HandleFunc("/block", rest.PostBlock).Methods("POST")
	muxRouter.HandleFunc("/block/{id:[0-9]+}", rest.GetBlock).Methods("GET")
	muxRouter.HandleFunc("/head", rest.GetHeadCid).Methods("GET")
	muxRouter.HandleFunc("/head/demos", rest.GetDemos).Methods("GET")
	muxRouter.HandleFunc("/head/cratos", rest.GetCratos).Methods("GET")
	muxRouter.HandleFunc("/head/currency", rest.GetCCY).Methods("GET")

	// Claim API
	muxRouter.HandleFunc("/claim/role", rest.GetClaimRole).Methods("GET")
	muxRouter.HandleFunc("/claim/approvedRole", rest.GetApprovedClaimRole).Methods("GET")
	muxRouter.HandleFunc("/claim/approve", rest.PostApprovedClaim).Methods("POST")
	muxRouter.HandleFunc("/claim/role", rest.PostClaimRole).Methods("POST")

	// Cratos API
	muxRouter.HandleFunc("/rbac", rest.GetRBAC).Methods("GET")
	muxRouter.HandleFunc("/vote", rest.PostVote).Methods("POST")
	muxRouter.HandleFunc("/amend", rest.PostAmendment).Methods("POST")

	// Tx API
	muxRouter.HandleFunc("/tx/send", rest.PostTX).Methods("POST")
	//muxRouter.HandleFunc("/tx/of/{pubkey:[0-9a-Z]+}", rest.GetTxOf).Methods("GET")

	// Network API
	muxRouter.HandleFunc("/network", rest.GetNetwork).Methods("GET")

	// Extra
	muxRouter.HandleFunc("/keygen", rest.HandleKeyGen).Methods("POST")
	muxRouter.HandleFunc("/pools", rest.GetPools).Methods("GET")
	muxRouter.HandleFunc("/available", rest.GetAvailables).Methods("GET")

	return muxRouter
}
func (rest RestService) GetPools(w http.ResponseWriter, r *http.Request) {
	rest.respondObjectAsJson(w, rest.smith.Pools)
}

func (rest RestService) FileHandler(w http.ResponseWriter, r *http.Request) {
	log.Info("FileHandler")
	//TODO make this generic
	http.ServeFile(w, r, "static/favicon.ico")
}

func (rest RestService) StartupRest(port int, pem string, crt string) (io.Closer, error) {
	//InitEndpoint()
	//InitPool()

	muxRoutes := rest.makeMuxRouter()
	// Where ORIGIN_ALLOWED is like `scheme://dns[:port]`, or `*` (insecure)
	headersOk := handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type", "Accept"})
	originsOk := handlers.AllowedOrigins([]string{"*"})
	methodsOk := handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "OPTIONS"})

	var listener net.Listener

	srv := &http.Server{
		Addr:           ":" + strconv.Itoa(port),
		Handler:        handlers.CORS(originsOk, headersOk, methodsOk)(muxRoutes),
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	listener, p := nextAvailablePort(port)
	log.Noticef("Rest server listening on %d", p)

	go func() {
		if pem != "" && crt != "" {
			err := srv.ServeTLS(tcpKeepAliveListener{TCPListener: listener.(*net.TCPListener)}, crt, pem)
			if err != nil {
				log.Info("HTTPS Server Error - ", err)
			}
		} else {
			err := srv.Serve(tcpKeepAliveListener{TCPListener: listener.(*net.TCPListener)})
			if err != nil {
				log.Info("HTTP Server Error - ", err)
			}
		}
	}()

	return listener, nil
}

func nextAvailablePort(port int) (net.Listener, int) {
	for i := port; i < port+10; i++ {
		listener, err := net.Listen("tcp", fmt.Sprintf(":%d", i))
		if err != nil {
			continue
		} else {
			if port != i {
				log.Warningf("using port %d since port %d already in use, please check your environment variable and command line parameters ", i, port)
			}
			return listener, i
		}
	}
	return nil, 0
}

type tcpKeepAliveListener struct {
	*net.TCPListener
}

func (rest RestService) HandleKeyGen(w http.ResponseWriter, r *http.Request) {

	name := r.FormValue("name")
	comment := r.FormValue("comment")
	email := r.FormValue("email")

	log.Info("genKey : ", name, comment, email)

	key := crypto.GenEd25519()

	bytes, err := json.MarshalIndent(key.Serialize(), "", "  ")
	if err != nil {
		log.Error("issue responding ")
	}

	_, _ = w.Write(bytes)
}

//  =================  Some generic functions for the rest Server  =================

func (rest RestService) DecodeReceivedJSON(w http.ResponseWriter, r *http.Request, dataObject interface{}) (ok bool) {
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&dataObject); err != nil {
		log.Error("Decoding", err)

		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte("HTTP 500: Internal Server Error decoding : " + err.Error()))
		return false
	}
	utilities.DeferCloses(r.Body, "Received JSON")
	return true
}

func (rest RestService) respondObjectAsJson(w http.ResponseWriter, data interface{}) {
	cl, err := json.MarshalIndent(data, "", "\t")
	if err != nil {
		log.Error("marshalling claim pool ", err)
		w.WriteHeader(http.StatusInternalServerError)
	}

	w.Header().Set("Content-Type", "application/json")
	_, _ = w.Write(cl)
}

func (rest RestService) VerifySignable(w http.ResponseWriter, signable model.Signable) (ok bool) {

	pk, signature := signable.Details()
	pubK := crypto.ParsePublicKey(pk)

	if ok, err := pubK.Verify(signable.PartToSign(), signature); !ok {
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte("HTTP 500: Signature invalid"))
		log.Error("Verifying Signature for string ", err)
		return false
	}
	return true
}

func (rest RestService) StoreToIPFS(w http.ResponseWriter, payload interface{}) (cid string, ok bool) {
	cid, err := rest.store.PutObject(payload)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte("HTTP 500: Saving to IPFS"))
		return "", false
	}
	return cid, true
}

func (rest RestService) GetAvailables(w http.ResponseWriter, r *http.Request) {
	var Available struct {
		Votes        []string
		VariableType []string
		TaxType      []string
		Propose      []string
	}
	Available.Votes = model.AvailableVotes
	Available.TaxType = model.AvailableTaxType
	Available.VariableType = model.AvailableVariableTax
	Available.Propose = model.AvailablePropose
	rest.respondObjectAsJson(w, &Available)
}
