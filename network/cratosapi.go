package network

import (
	"gitlab.com/bertrandbenj/politikojj/model"
	"net/http"
)

func (rest RestService) PostVote(w http.ResponseWriter, r *http.Request) {
	var ballot = model.OpenBallot{}

	if rest.DecodeReceivedJSON(w, r, &ballot) {
		log.Info("Decoded ballot ", ballot)
		if cid, ok := rest.StoreToIPFS(w, ballot); ok {
			rest.smith.Pools.Add(cid, ballot)
		}
	}
}

func (rest RestService) PostAmendment(w http.ResponseWriter, r *http.Request) {
	var amendment model.Amendment

	if rest.DecodeReceivedJSON(w, r, &amendment) {
		if rest.VerifySignable(w, &amendment) {
			log.Warning("amendment", amendment)

			if rest.VerifyRuleDataType(amendment) {
				if cid, ok := rest.StoreToIPFS(w, amendment); ok {
					rest.smith.Pools.Add(cid, amendment)
					rest.respondObjectAsJson(w, model.Wrapper{
						CID:  cid,
						Data: amendment,
					})
				}
			}
		}
	}
}

// check some of the data type
func (rest RestService) VerifyRuleDataType(a model.Amendment) bool {
	for _, v := range a.Predicates {
		cratos := rest.chain.GetHead().Cratos
		if !model.VerifyPredicate[v.Predicate](&v, &cratos) {
			return false
		}
	}
	return true
}

func (rest RestService) GetRBAC(w http.ResponseWriter, _ *http.Request) {
	rest.respondObjectAsJson(w, rest.smith.CratosP.SerializeForCratos())
}
