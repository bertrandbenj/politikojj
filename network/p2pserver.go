package network

import (
	"bufio"
	"context"
	"fmt"
	mrand "math/rand"
	"strings"
	"time"

	"github.com/libp2p/go-libp2p-core/crypto"
	"gitlab.com/bertrandbenj/politikojj/model"

	"sync"

	"gitlab.com/bertrandbenj/politikojj/chain"
	"gitlab.com/bertrandbenj/politikojj/node"
	"gitlab.com/bertrandbenj/politikojj/storage"

	"github.com/libp2p/go-libp2p"
	"github.com/libp2p/go-libp2p-core/host"
	"github.com/libp2p/go-libp2p-core/network"
	"github.com/libp2p/go-libp2p-core/peer"
	"github.com/multiformats/go-multiaddr"
)

type ServerP2P struct {
	chain        *chain.BlockChainService
	store        storage.Store
	smith        *node.BlockSmith
	port         int
	multiAddress multiaddr.Multiaddr
	secio        bool
	seed         int64
	host         host.Host
	Sleep        time.Duration
	peerChan     chan peer.AddrInfo
	BlockStreams map[string]*bufio.ReadWriter
	blockChan    chan model.Block
	DocsStreams  map[string]*bufio.ReadWriter
	docsChan     chan model.Wrapper
	streamLock   *sync.RWMutex
}

func (p2p *ServerP2P) Update(s string) {
	log.Infof("%d Promoting head update... broadcasting %s", p2p.port, s)
	//p2p.updatedHead.Set()
	p2p.blockChan <- p2p.chain.GetHead().Block
}

func (p2p *ServerP2P) GetID() string {
	return p2p.multiAddress.String()
}

func NewP2P(ctx context.Context, smith *node.BlockSmith, listenF int, seed int64, secio bool, bootstrap []multiaddr.Multiaddr) *ServerP2P {
	P2P := ServerP2P{
		chain:        smith.Chain,
		store:        smith.Chain.Store,
		smith:        smith,
		port:         listenF,
		secio:        secio,
		seed:         seed,
		Sleep:        5 * time.Second,
		blockChan:    make(chan model.Block, 1),
		BlockStreams: make(map[string]*bufio.ReadWriter),
		streamLock:   &sync.RWMutex{},
		DocsStreams:  make(map[string]*bufio.ReadWriter),
		docsChan:     make(chan model.Wrapper, 1),
	}

	smith.Chain.Head.Register(&P2P)
	smith.Pools.Attach(P2P.docsChan)

	log.Noticef("Init P2P Server using seed %d, secure I/O ? %t ", seed, secio)

	if listenF == 0 {
		log.Fatal("Please provide a port to bind on with -p2p_port")
	}

	// Make a host that listens on the given multi-address
	P2P.makeBasicHost(ctx, bootstrap)

	return &P2P
}

func (p2p ServerP2P) handlePoolStream(s network.Stream) {
	remote := s.Conn().RemotePeer().Pretty()
	log.Infof("%d handle a pool stream! %s %s", p2p.port, s.Protocol(), remote)

	rw := bufio.NewReadWriter(bufio.NewReader(s), bufio.NewWriter(s))

	p2p.streamLock.Lock()
	p2p.DocsStreams[remote] = rw
	p2p.streamLock.Unlock()

	go p2p.readPoolDoc(rw, s)
}

func (p2p ServerP2P) handleStream(s network.Stream) {
	p2p.handleBlockStream(s, "router")
}

func (p2p ServerP2P) handleBlockStream(s network.Stream, origin string) {
	remote := s.Conn().RemotePeer().Pretty()
	log.Infof("%d handle a new stream! %s %s %s", p2p.port, s.Protocol(), remote, origin)

	rw := bufio.NewReadWriter(bufio.NewReader(s), bufio.NewWriter(s))
	p2p.streamLock.Lock()
	p2p.BlockStreams[remote] = rw
	p2p.streamLock.Unlock()
	go p2p.readBlock(rw, s)
	//go p2p.writeHead(rw, s)s
}

// Listen to new head block and broadcast to connected peers
func (p2p ServerP2P) runBroadcaster() {

	for {
		select {
		case newDoc := <-p2p.docsChan:
			log.Infof("%d about to broadcast Doc %s ", p2p.port, newDoc.CID)
			go p2p.broadcastDoc(newDoc)
			break

		case newHead := <-p2p.blockChan:
			log.Infof("%d about to broadcast Block n°%d ", p2p.port, newHead.Index)
			go p2p.broadcastBlock(newHead)
			break

		}
	}
}

// Creates a LibP2P host with a random peer ID listening on the
// given multi-address. It will use secio if secio is true.
func (p2p *ServerP2P) makeBasicHost(ctx context.Context, _ AddrList) {

	// find the next available port
	listener, p := nextAvailablePort(p2p.port)
	_ = listener.Close()
	p2p.port = p

	opts := []libp2p.Option{
		libp2p.ListenAddrStrings(
			fmt.Sprintf("/ip4/127.0.0.1/tcp/%d", p2p.port),
			fmt.Sprintf("/ip4/0.0.0.0/tcp/%d", p2p.port)),
		//libp2p.ListenAddrs(bootstrap...),
	}

	if p2p.smith.Key.PrivateKey != nil {
		opts = append(opts, libp2p.Identity(p2p.smith.Key.PrivateKey))
	} else {
		// If the seed is zero, let libp2p create a keypair. Otherwise, use a
		// deterministic randomness source to make generated keys stay the same
		// across multiple runs
		if p2p.seed != 0 {
			r := mrand.New(mrand.NewSource(p2p.seed))
			priv, _, err := crypto.GenerateKeyPairWithReader(crypto.RSA, 2048, r)
			if err != nil {
				log.Error("generating KeyPair using ", r)
			}
			opts = append(opts, libp2p.Identity(priv))
		}
	}

	// Use Secure I/O if requested
	if !p2p.secio {
		opts = append(opts, libp2p.NoSecurity)
	}

	var err error
	p2p.host, err = libp2p.New(ctx, opts...)
	if err != nil {
		log.Error("Creating new libp2p host", err)
	} else {
		log.Noticef("P2P server listening on %d, using pubkey %s", p2p.port, p2p.host.ID())
		log.Notice(p2p.host.Addrs())
	}

	// Build host multi-address
	hostAddr, _ := multiaddr.NewMultiaddr(fmt.Sprintf("/ipfs/%s", p2p.host.ID().Pretty()))

	// Now we can build a full multi-address to reach this host
	// by encapsulating both addresses:
	p2p.multiAddress = p2p.host.Addrs()[0].Encapsulate(hostAddr)

	p2p.host.SetStreamHandler("/pkj/1.0.0", p2p.handleStream)
	p2p.host.SetStreamHandler("/pkj/tx/1.0.0", p2p.handlePoolStream)

	log.Debugf("If you wish to bootstrap on that node, run on a different terminal:\n./politikojj daemon -bootstrap %s", p2p.multiAddress)

}

func (p2p *ServerP2P) isCitizen(peer peer.AddrInfo) bool {
	pubkey := fmt.Sprintf("%v", peer.ID)
	for _, v := range p2p.chain.GetHead().Demos.PublicKeysByRole["Citizen"] {
		if pubkey == v {
			return true
		}
	}
	return false
}

func (p2p *ServerP2P) RunMDNS(ctx context.Context, RendezVousPoint string) {

	h := p2p.host
	p2p.peerChan = initMDNS(ctx, h, RendezVousPoint)

	go p2p.runBroadcaster()

	for {
		log.Infof("%d waiting for peerInfo ", p2p.port)

		peerInfo := <-p2p.peerChan
		// will block until we discover a peerInfo
		log.Infof("%d ✓ Found peer %s", p2p.port, peerInfo)
		if !p2p.isCitizen(peerInfo) {
			log.Warningf("%d Refusing connection from %s, the peer is not a citizen", p2p.port, peerInfo)
			continue
		}

		if err := h.Connect(ctx, peerInfo); err != nil {
			log.Infof("%d Connection failed: %s", p2p.port, err)
			continue
		}

		// open a stream, this stream will be handled by the router on the other end
		if stream, err := h.NewStream(ctx, peerInfo.ID, "/pkj/1.0.0"); err != nil {
			log.Info("Stream open failed", err)
			_ = stream.Close()
		} else {
			log.Infof("\u001B[32m%d ✓ Connected to:\u001B[0m %s", p2p.port, peerInfo)
			p2p.handleBlockStream(stream, "RunMDNS")
		}

		// Open A document stream
		if streamDoc, err := h.NewStream(ctx, peerInfo.ID, "/pkj/tx/1.0.0"); err != nil {
			log.Info("Stream open failed", err)
			_ = streamDoc.Close()
		} else {
			p2p.handlePoolStream(streamDoc)
		}
	}
}

// A new type we need for writing a custom flag parser
type AddrList []multiaddr.Multiaddr

func (al *AddrList) String() string {
	strs := make([]string, len(*al))
	for i, addr := range *al {
		strs[i] = addr.String()
	}
	return strings.Join(strs, ",")
}

func (al *AddrList) Set(value string) error {
	addr, err := multiaddr.NewMultiaddr(value)
	if err != nil {
		return err
	}
	*al = append(*al, addr)
	return nil
}

func StringsToAddrs(addrStrings []string) (maddrs []multiaddr.Multiaddr, err error) {
	for _, addrString := range addrStrings {
		addr, err := multiaddr.NewMultiaddr(addrString)
		if err != nil {
			return maddrs, err
		}
		maddrs = append(maddrs, addr)
	}
	return
}
