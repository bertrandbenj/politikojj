package storage

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/bertrandbenj/politikojj/model"
	"gitlab.com/bertrandbenj/politikojj/storage/mock"
	"testing"
)

func TestFileToString(t *testing.T) {
	str := mock.FileToString("../testset/pub.asc")
	assert.NotNil(t, str, "File should not be empty")
}

func TestStorage(t *testing.T) {
	store := mock.NewFileSystemStore("/tmp/teststorage")

	ssd := model.SignedStampedDoc{
		Timestamp: 123465,
		PublicKey: "ABC",
		Signature: "huhuhhu",
	}
	cid, err := store.PutObject(ssd)
	assert.Nil(t, err)
	assert.NotNil(t, cid)

	var get model.SignedStampedDoc
	store.GetObject(cid, &get)

	assert.EqualValues(t, ssd, get)

	fCid := store.StoreFile("../docs/constitution.md")
	assert.NotNil(t, fCid)

	store.UpdateHead(cid)
}
