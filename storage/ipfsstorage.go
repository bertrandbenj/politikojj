package storage

import (
	"bytes"
	"context"
	"encoding/json"
	"io"
	"io/ioutil"
	"math/rand"
	"net/http"
	"os"
	"reflect"
	"runtime"
	"time"

	shell "github.com/ipfs/go-ipfs-api"
	"github.com/op/go-logging"
)

type IPFSStorage struct {
	url        string
	sh         *shell.Shell
	rootFolder string
}

var log = logging.MustGetLogger("storage")

/**
return the cid, data and eventual error
*/
func (s IPFSStorage) PutObject(payload interface{}) (st string, err error) {
	jsonObj, err := json.MarshalIndent(payload, "", "")
	if err != nil {
		log.Errorf("MarshalIndent JSON for payload %s", reflect.TypeOf(payload), err)
	}
	//log.Infof("dag put %s", &jsonObj )
	st, err = s.sh.DagPut(jsonObj, "json", "cbor")
	if err != nil {
		_, file, no, _ := runtime.Caller(1)
		log.Errorf("from %s#%d err: %s for payload\n", file, no, err, payload)
	}
	return
}

func (s IPFSStorage) UpdateHead(cid string) {
	opt := shell.FilesWrite.Create(true)
	err := s.sh.FilesWrite(context.Background(), s.rootFolder+"head", bytes.NewBufferString(cid), opt)
	if err != nil {
		log.Error("Error Updating IPFS head file to ", s.rootFolder+"head", cid, err)
	}
}

/*
	Store a file from the file system into IPFS

	return its IPFS CID
*/
func (s IPFSStorage) StoreFile(filename string) (cid string) {
	fReader, err := os.Open(filename)
	if err != nil {
		log.Error("Error opening", filename, err)
	}

	err = s.sh.FilesMkdir(context.Background(), s.rootFolder)
	if err != nil {
		log.Warning("Error creating directory ", s.rootFolder, err)
	}
	pubString, _ := ioutil.ReadAll(fReader)

	opt := shell.FilesWrite.Create(true)
	cid, err = s.sh.Add(bytes.NewBuffer(pubString), opt)
	if err != nil {
		log.Error("Error ipfs files Add ", filename, err)
	}

	return cid
}

func (s IPFSStorage) GetObject(cid string, payload interface{}) (ok bool) {

	err := s.sh.DagGet(cid, payload)
	if err != nil {
		_, file, no, _ := runtime.Caller(1)

		log.Errorf("In %s#%d while fectiong %s IPFS err %s", file, no, cid, err)
		return false
	}
	return true
}

// ==================

func NewIPFSStore(url string, folder string) *IPFSStorage {
	return &IPFSStorage{
		url:        url,
		sh:         shell.NewShell(url),
		rootFolder: folder,
	}
}

func (s IPFSStorage) TestIPFS() {
	rd, err := s.makeRandomObject()
	if err != nil {
		log.Error("IPFS error, please make sure IPFS is running: ", err)
		return
	} else {
		log.Info("saved random object :", rd)
	}

	type X struct{ HUHU string }
	in := X{rd}

	cid, err := s.PutObject(in)
	if err != nil {
		log.Error("Testing DagPut", err)
	} else {
		log.Info("Testing put okay", cid)
	}

	var x X

	res := s.ManualDagGet(cid, x)

	if err != nil {
		log.Error("Testing DagGet ", err, x)
	}

	log.Info("IPFS test done ", cid, x, res)
}

func (s IPFSStorage) ManualDagGet(cid string, v interface{}) interface{} {
	resp, err := http.Get("http://" + s.url + "/api/v0/dag/get?arg=" + cid)
	if err != nil {
		log.Error("http post", err)
	}

	//body, err := ioutil.ReadAll(resp.Body)
	//if err != nil {
	//	log.Error("decoding", err)
	//}
	decoder := json.NewDecoder(resp.Body)
	err = decoder.Decode(&v)
	//if err != nil {
	//	log.Error("unmarshaling", err, body, string(body))
	//}
	//
	//err = json.Unmarshal( body , v)
	if err != nil {
		log.Error("unmarshaling", err)
	}
	defer resp.Body.Close()
	return v
}

func (s IPFSStorage) makeRandomObject() (string, error) {
	// do some math to make a size
	x := rand.Intn(120) + 1
	y := rand.Intn(120) + 1
	z := rand.Intn(120) + 1
	size := x * y * z

	r := io.LimitReader(bytes.NewBufferString("Hello World"), int64(size))
	time.Sleep(time.Millisecond * 5)
	res, err := s.sh.Add(r)
	if err != nil {
		log.Error(err)
	} else {
		log.Info(res)
	}
	return res, err
}
