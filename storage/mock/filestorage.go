package mock

import (
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"github.com/op/go-logging"

	"io/ioutil"
	"os"
	"reflect"
	"strings"
)

var log = logging.MustGetLogger("storemock")

type IPFSStoreMock struct {
	directory string
}

func (fs IPFSStoreMock) GetObject(cid string, payload interface{}) (ok bool) {
	//str, _ := os.Open(fs.directory + cid)
	//b, err := ioutil.ReadAll(str)
	err := json.NewDecoder(strings.NewReader(FileToString(cid))).Decode(payload)
	if err != nil {
		log.Error("error decoding", cid)
		return false
	}

	return true
}
func (fs IPFSStoreMock) PutObject(payload interface{}) (string, error) {
	b, _ := json.MarshalIndent(payload, "", "\t")
	h := sha256.New()
	h.Write(b)
	hashed := reflect.TypeOf(payload).Name() + "_" + hex.EncodeToString(h.Sum(nil))

	f, _ := os.Create(fs.directory + hashed)
	_, err := f.Write(b)
	if err != nil {
		return "", err
	}
	return f.Name(), nil
}
func NewFileSystemStore(dir string) (fs *IPFSStoreMock) {
	err := os.MkdirAll(dir, os.ModePerm)
	if err != nil {
		log.Info("couldnt mkdir", err)
	}
	FSStore := IPFSStoreMock{
		directory: dir,
	}
	return &FSStore
}

func (fs IPFSStoreMock) StoreFile(filename string) string {
	content, _ := os.Open(filename)
	name := strings.Split(filename, "/")
	n := name[len(name)-1]
	f, _ := os.Create(fs.directory + n)
	b, _ := ioutil.ReadAll(content)
	_, _ = f.Write(b)
	return f.Name()
}

func (fs IPFSStoreMock) UpdateHead(cid string) {
	head, _ := os.Open(fs.directory + "head")
	_, _ = head.WriteString(cid)
}

func FileToString(filename string) string {
	content, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Error(err, filename)
	}

	return string(content)
}

func StringToFile(data string, filename string) {
	dst, err := os.Create(filename)
	if err != nil {
		log.Error("Creating file", filename)
	}
	_, err = dst.WriteString(data)
	if err != nil {
		log.Error("Writing String to ", filename)
	}
	defer dst.Close()
}
