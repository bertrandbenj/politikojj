package storage

type Store interface {
	GetObject(cid string, payload interface{}) (ok bool)
	PutObject(payload interface{}) (string, error)
	//GetPubKey(cid string) crypto.PublicK
	StoreFile(filename string) (cid string)
	UpdateHead(cid string)
	//StoreArmoredPubKey(pubkey crypto.PGPPublicKey) (string, *bytes.Buffer)
}
