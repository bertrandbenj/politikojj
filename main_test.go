package main

import (
	"os"
	"testing"

	"gitlab.com/bertrandbenj/politikojj/chain"
	"gitlab.com/bertrandbenj/politikojj/crypto"
	"gitlab.com/bertrandbenj/politikojj/node"
	storage "gitlab.com/bertrandbenj/politikojj/storage/mock"
	"gitlab.com/bertrandbenj/politikojj/utilities"
)

var smith *node.BlockSmith
var ch *chain.BlockChainService
var store *storage.IPFSStoreMock

func init() {
	key := crypto.LoadKey("./testset/ed25519.priv")
	//key := crypto.ReadPubAndPrivateArmorKeyFile("./testset/pub.asc", "./testset/keypair.asc", "test1")

	store = storage.NewFileSystemStore("/tmp/testmain/")
	ch = chain.NewChain(store, "TestNet")
	smith = node.NewBlockSmith(ch, key, 1)

	utilities.ConfigLogger("DEBUG")
}

func TestFlags(t *testing.T) {
	CliVote()
	CliClaim()
	CliPropose()
	CliDaemon()
	CliTX()

	pr := "./testset/ed25519.priv"
	flags.private = &pr

	pub := "./testset/pub.asc"
	flags.publicKey = &pub

	pass := "test1"
	flags.passphrase = &pass

	n := "name"
	flags.name = &n

	r := "role"
	flags.role = &r
	permission := "permission"
	flags.permission = &permission
	voteObject := "voteObject"
	flags.voteObject = &voteObject
	voteValue := "voteValue"
	flags.voteValue = &voteValue
	to := "to"
	flags.to = &to
	amount := 10
	flags.amount = &amount

	scanCredentials()
	keypair()

	InteractiveClaim()
	InteractiveTransaction()
	InteractiveVote()
	//InteractiveAmendment()

	os.Args[1] = "help"
	ExecuteSubCommand()

	// os.Args = []string{"daemon", "-p2p_port", "8020"}
	// RunAsDaemon()

	main()
}
